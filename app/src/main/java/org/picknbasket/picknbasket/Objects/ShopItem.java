package org.picknbasket.picknbasket.Objects;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.picknbasket.picknbasket.Abstracts.EObjectType;
import org.picknbasket.picknbasket.Abstracts.GridObject;
import org.picknbasket.picknbasket.Abstracts.IDBObject;
import org.picknbasket.picknbasket.Abstracts.INumberPickerListener;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.CustomContentValues;
import org.picknbasket.picknbasket.Helpers.NumberPicker;
import org.picknbasket.picknbasket.Helpers.ObjectCache;
import org.picknbasket.picknbasket.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Takunda on 1/20/2020.
 */

public class ShopItem extends GridObject implements Comparable<GridObject>, IDBObject {

    private int shopId;
    private String name;
    private String displayPrice;
    private String details;
    private double price;
    private String currency;
    private int quantity; // eg 200g will be 200 and 2kg will be 2
    private String measurement; // eg 200g will be 'g' and 2kg will be 'kg'
    private String webUrl;
    //references the ShopWebPage
    private int webId;
    private Long lastUpdate;
    private boolean onSpecial;

    // Used to tell if the item has been picked when it's in a basket
    private int basketItemId;
    private boolean picked;
    private int count = 1;
    private int basketId;

    // Only used when item is being put in db
    private int priceCount;

    public ShopItem(String name){
        this.name=name.replace("'","");
        this.id=Math.abs(name.hashCode());
    }

    public ShopItem(String name,int id){
        this.name=name;
        this.id=id;
    }

    @Override
    public int compareTo(@NonNull GridObject shopItem) {

        // Use website first to sort
        if(isPicked() && !((ShopItem)shopItem).isPicked()){
            return 1;
        }

        if (!isPicked() && ((ShopItem)shopItem).isPicked()){
            return -1;
        }

        return 0;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getTotalPrice(){
        return price * count;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public void setWebId(int webId) {
        this.webId = webId;
    }

    public int getWebId() {
        return webId;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getDisplayPrice() {
        return displayPrice;
    }

    public boolean isPicked() {
        return picked;
    }

    public int getPicked(){
        if (picked){
            return 1;
        }else{
            return 0;
        }
    }

    public void setPicked(boolean picked) {
        this.picked = picked;
    }

    public void setPicked(int picked){
        if (picked == 0){
            this.picked = false;
        }else{
            this.picked = true;
        }
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = Math.max(1,count);
    }

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public void setBasketItemId(int basketItemId){
        this.basketItemId=basketItemId;
    }

    public String getCurrency() {
        if (currency == null){
            return "";
        }
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setDisplayPrice(String displayPrice) {
        this.displayPrice=displayPrice.replace("'","");
    }

    public String getDetails() {
        return details;
    }

    /**
     * Will also check if the item is on special
     * @param details
     */
    public void setDetails(String details) {
        details=details.replace("'","");
        this.details = details;
    }

    /**
     * Checks if details has key words for a special
     */
    public void setOnSpecial() {

        System.out.println("on special price "+priceCount+" "+details);
        if (priceCount > 1){
            onSpecial = true;
            return;
        }

        details = this.details.toLowerCase();
        for (int i=0;i<AppVariables.saveKeywords.length;i++){
            if (details.contains(AppVariables.saveKeywords[i])){
                onSpecial = true;
                return;
            }
        }

        onSpecial = false;
    }

    public void setOnSpecial(int onSpecial){
        if (onSpecial == 1){
            this.onSpecial = true;
            return;
        }
        this.onSpecial = false;
    }

    public int getOnSpecial(){
        if (onSpecial){
            return 1;
        }
        return 0;
    }

    /**
     * gets the price from the display price
     */
    public void derivePrice(){
        if (this.displayPrice == null || this.displayPrice.isEmpty()){
            return;
        }
        String displayPrice = this.displayPrice.replaceAll("\\s","");
        Pattern pricePattern = Pattern.compile("\\d{1,5}[,\\.]?(\\d{1,5})?");
        Matcher m = pricePattern.matcher(displayPrice);
        while (m.find()) {
            String priceStr = m.group(0).replace(",","");
            double price = Double.parseDouble(priceStr);
            setDisplayPrice(this.displayPrice.replace(m.group(0),price+" "));
            if (this.price == 0){
                int cIndex = displayPrice.indexOf(m.group(0));
                if (cIndex > 0){
                    currency = displayPrice.charAt(cIndex-1)+"";
                }else{
                    currency = displayPrice.charAt(cIndex+m.group(0).length())+"";
                }
                this.price = price;
            }
            priceCount++;
        }
        System.out.println("derived price "+priceCount);
    }

    @Override
    public ContentValues getContentValues() {
        CustomContentValues data = new CustomContentValues();

        data.put("name",name);
        data.put("price",price);
        data.put("displayPrice",displayPrice);
        data.put("details",details);
        data.put("onSpecial",onSpecial);
        data.put("pictureUrl",pictureUrl);
        data.put("webUrl",webUrl);
        data.put("webId",webId);
        data.put("lastUpdate",lastUpdate);
        data.put("shopId",shopId);
        data.put("quantity",quantity);
        data.put("measurement",measurement);
        data.put("currency",currency);

        return data.getContentValues();
    }

    @Override
    public CharSequence getMainText() {
        return name;
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.SHOP_ITEM;
    }

    @Override
    public CharSequence getSecondaryText() {
        return displayPrice;
    }

    @Override
    public CharSequence getBrandText() {
        Shop shop = ObjectCache.getShop(shopId);
        if (shop != null){
            return shop.getName();
        }
        return "";
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Drawable getDefaultPicture(){
        Drawable drawable = (AppVariables.context.getDrawable(R.drawable.ic_grocery));
        return drawable;
    }

    @Override
    public void setSecondaryView(View view){
        if (selectItem != null) {
            selectItem.setVisibility(View.VISIBLE);
        }

        view.findViewById(R.id.rating_container).setVisibility(View.GONE);
        view.findViewById(R.id.brand_container).setVisibility(View.VISIBLE);

        Shop shop = ObjectCache.getShop(shopId);
        ImageView brandIcon = view.findViewById(R.id.brand_iv);
        TextView brandName = view.findViewById(R.id.brand_tv);

        if (shop != null){
            if (shop.getBmp() != null) {
                Drawable drawable = new BitmapDrawable(AppVariables.context.getResources(), shop.getBmp());
                brandIcon.setImageDrawable(drawable);
            }
            brandName.setText(shop.getName());
        }

        if (infoHighlightTV != null) {
            if (onSpecial) {
                infoHighlightTV.setVisibility(View.VISIBLE);
            } else {
                infoHighlightTV.setVisibility(View.GONE);
            }
        }

        if (adapter != null){
            if(adapter.showNumberPicker()) {
                NumberPicker numberPicker = view.findViewById(R.id.items_number_picker);
                if (numberPicker != null) {
                    numberPicker.setVisibility(View.VISIBLE);
                    numberPicker.setMin(1);
                    numberPicker.setValue(getCount());
                    numberPicker.setListener(new INumberPickerListener() {
                        @Override
                        public void onNumberPicked(int value) {
                            adapter.numberPicked(object, value);
                        }
                    });
                }
            }

            // NB! if this is called before the listener for the recycled view has been called
            // the id of the old view will be used leading to unexpected stuff.
            // To avoid we changed order in ViewHolder.prepareView
            if (selectItem != null && adapter.isSelected(object.getId())){
                System.out.println("shop item selected "+object.getId());
                selectItem.setChecked(true);
            }else{
                System.out.println("shop item de-selected "+object.getId());
                selectItem.setChecked(false);
            }
        }
    }

    @Override
    public void doPrimaryViewActivity(){

        primaryIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(listActivity, listItemClassActivity);
                intent.putExtra("shopItemId",id);
                intent.putExtra("shopId",shopId);
                listActivity.startActivity(intent);

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(pictureUrl));
                //listActivity.startActivity(i);
            }
        });

        primaryIV.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                adapter.longClick(object);
                return true;
            }
        });
    }


    @Override
    public void applyBackground() {
        if (view.findViewById(R.id.items_number_picker) == null){
            return;
        }

        if (isPicked()){
            gridViewContainer.setBackgroundResource(R.drawable.rectangle_green);
            view.findViewById(R.id.btn_more).setBackgroundResource(R.drawable.rectangle_green_button);
            view.findViewById(R.id.btn_less).setBackgroundResource(R.drawable.rectangle_green_button);
        }else{
            gridViewContainer.setBackgroundResource(R.drawable.rectangle_orange);
            view.findViewById(R.id.btn_more).setBackgroundResource(R.drawable.rectangle_orange_button);
            view.findViewById(R.id.btn_less).setBackgroundResource(R.drawable.rectangle_orange_button);
        }
    }
}
