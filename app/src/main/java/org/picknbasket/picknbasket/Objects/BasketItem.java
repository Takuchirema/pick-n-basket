package org.picknbasket.picknbasket.Objects;

public class BasketItem extends ShopItem{
    // Used to tell if the item has been picked when it's in a basket
    private boolean picked;
    private int quantity;

    public BasketItem(String name) {
        super(name);
    }

    public boolean isPicked() {
        return picked;
    }

    public void setPicked(boolean picked) {
        this.picked = picked;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    @Override
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
