package org.picknbasket.picknbasket.Objects;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import android.view.View;

import org.picknbasket.picknbasket.Abstracts.EObjectType;
import org.picknbasket.picknbasket.Abstracts.GridObject;
import org.picknbasket.picknbasket.Abstracts.IDBObject;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.CustomContentValues;
import org.picknbasket.picknbasket.R;

import java.util.HashMap;

/**
 * Created by Takunda on 1/10/2020.
 */

public class Shop extends GridObject implements Comparable<GridObject>,IDBObject {

    private String googleId;
    private String placeId;
    private String name;
    private String website;
    private String phoneNumber;
    private String latitude;
    private String longitude;
    private String latlng2;
    private int priceLevel;
    private String vicinity;
    private boolean detailsFetched;
    private EHasDetails hasDetails;
    private String businessHours;
    private HashMap<Integer,ShopItem> shopItems = new HashMap<>();

    public Shop(String name){
        this.name=name.replace("'","");
    }

    @Override
    public int compareTo(@NonNull GridObject shop) {

        // Use website first to sort
        if (hasWebsite() && ((Shop)shop).hasWebsite()){
            if (hasPicture() && !(shop).hasPicture()){
                return -1;
            }

            if (!hasPicture() && (shop).hasPicture()){
                return 1;
            }
        }

        if(hasWebsite() && !((Shop)shop).hasWebsite()){
            return -1;
        }

        if (!hasWebsite() && ((Shop)shop).hasWebsite()){
            return 1;
        }

        // then use location to sort
        if (AppVariables.latlng2 != null){
            if (latlng2 == AppVariables.latlng2 && ((Shop)shop).latlng2 != AppVariables.latlng2){
                return -1;
            }

            if (latlng2 != AppVariables.latlng2 && ((Shop)shop).latlng2 == AppVariables.latlng2){
                return 1;
            }
        }

        // then use ratings to sort
        if(this.userRatingsTotal > ((Shop)shop).userRatingsTotal){
            return -1;
        }

        if(this.userRatingsTotal < ((Shop)shop).userRatingsTotal){
            return 1;
        }
        return 0;
    }

    /**
     * This is to show if the shop has all contact details or not.
     * ALL means it has them all and they have been fetched
     * SOME means they have been fetched by some are missing
     * UNKNOWN means they have not been fetched yet
     * NONE means they have been fetched and are non existent
     * NA means not applicable because it's not google details
     */
    public enum EHasDetails{
        ALL,SOME,UNKNOWN,NONE
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getName() {
        return name;
    }

    public String getWebsite() {
        return website == null ? "" : website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getBusinessHours() {
        if (businessHours != null){
            businessHours = businessHours.replace(",", "\n");
        }
        return businessHours;
    }

    public void setBusinessHours(String businessHours) {
        this.businessHours = businessHours;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLatitude() {
        return latitude;
    }

    /**
     * Checks if the shop is close by in relation to current user's location
     * @return
     */
    public boolean isNearby(){
        float distance = getDistanceFrom();
        if (distance < AppVariables.nearbyRadius){
            return true;
        }
        return false;
    }

    public float getDistanceFrom(){
        Location currentLocation = AppVariables.getLocation();
        if (currentLocation == null){
            return 0;
        }

        Location shopLocation = new Location("shop location");
        shopLocation.setLatitude(Double.parseDouble(latitude));
        shopLocation.setLongitude(Double.parseDouble(longitude));

        return shopLocation.distanceTo(currentLocation);
    }

    public boolean isDetailsFetched() {
        return detailsFetched;
    }

    public void setDetailsFetched(int detailsFetched) {
        if (detailsFetched == 0){
            this.detailsFetched=false;
        }else{
            this.detailsFetched=true;
        }
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLatLng(String latitude, String longitude){
        this.latitude=latitude;
        this.longitude=longitude;

        String lat2 = latitude.substring(0,latitude.indexOf(".")+3);
        String lng2 = longitude.substring(0,longitude.indexOf(".")+3);
        this.latlng2=lat2+","+lng2;
    }

    public String getLatLng2(){
        return latlng2;
    }

    public int getPriceLevel() {
        return priceLevel;
    }

    public void setPriceLevel(int priceLevel) {
        this.priceLevel = priceLevel;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public boolean hasPhoneNumber(){
        if (phoneNumber != null && !phoneNumber.isEmpty()){
            return true;
        }
        return false;
    }

    public boolean hasWebsite(){
        if (website != null && !website.trim().isEmpty()){
            return true;
        }
        return false;
    }

    public EHasDetails hasDetails(){
        return hasDetails;
    }

    public void setHasDetails(){
        if (hasPicture() && hasPhoneNumber()){
            hasDetails = EHasDetails.ALL;
        }else if (hasPicture() || hasPhoneNumber()){
            hasDetails = EHasDetails.SOME;
        }else if (detailsFetched){
            hasDetails = EHasDetails.NONE;
        }else if (googleId == null || googleId.isEmpty()){
            hasDetails = EHasDetails.NONE;
        } else{
            hasDetails = EHasDetails.UNKNOWN;
        }
    }

    public boolean fetchPictureUrl(){
        if (!hasPicture() && hasWebsite()){
            return true;
        }
        return false;
    }

    public HashMap<Integer, ShopItem> getShopItems() {
        return shopItems;
    }

    public void setShopItems(HashMap<Integer, ShopItem> shopItems) {
        this.shopItems = shopItems;
    }

    @Override
    public ContentValues getContentValues() {
        CustomContentValues data = new CustomContentValues();

        data.put("name",name);
        data.put("website",website);
        data.put("phoneNumber",phoneNumber);
        data.put("pictureUrl",pictureUrl);
        data.put("latitude",latitude);
        data.put("longitude",longitude);
        data.put("latlng2",latlng2);
        data.put("priceLevel",priceLevel);
        data.put("rating",rating);
        data.put("userRatingsTotal",userRatingsTotal);
        data.put("vicinity",vicinity);
        data.put("businessHours",businessHours);
        if (detailsFetched)
            data.put("detailsFetched",1);
        else
            data.put("detailsFetched",0);

        return data.getContentValues();
    }

    @Override
    public CharSequence getMainText() {
        System.out.println("shop pic url: "+name+" "+pictureUrl);
        return name;
    }

    @Override
    public EObjectType getListObjectType() {
        return EObjectType.SHOP;
    }

    @Override
    public CharSequence getSecondaryText() {
        return vicinity;
    }

    @Override
    public void applyBackground() {
        if (hasPicture()){
            gridViewContainer.setBackgroundResource(R.drawable.rectangle_green);
        }else{
            gridViewContainer.setBackgroundResource(R.drawable.rectangle_orange);
        }
    }

    @Override
    public void doPrimaryViewActivity(){
        primaryIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(listActivity, listItemClassActivity);
                intent.putExtra("shopId", id);
                listActivity.startActivity(intent);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Drawable getDefaultPicture(){
        Drawable drawable = (AppVariables.context.getDrawable(R.drawable.ic_supermarket));
        return drawable;
    }
}
