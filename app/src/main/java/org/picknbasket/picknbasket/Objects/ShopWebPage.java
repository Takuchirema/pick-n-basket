package org.picknbasket.picknbasket.Objects;

import android.content.ContentValues;

import org.picknbasket.picknbasket.Abstracts.IDBObject;
import org.picknbasket.picknbasket.Helpers.CustomContentValues;

public class ShopWebPage implements IDBObject {

    private int id;
    private int shopId;
    private long lastScrapped;
    private String url;

    public ShopWebPage(String url){
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public long getLastScrapped() {
        return lastScrapped;
    }

    public void setLastScrapped(long lastScrapped) {
        this.lastScrapped = lastScrapped;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public ContentValues getContentValues() {
        CustomContentValues data = new CustomContentValues();

        data.put("url",url);
        data.put("shopId",shopId);
        data.put("lastScrapped",lastScrapped);

        return data.getContentValues();
    }
}
