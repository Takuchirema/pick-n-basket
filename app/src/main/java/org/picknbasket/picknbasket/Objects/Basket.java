package org.picknbasket.picknbasket.Objects;

import android.content.ContentValues;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.picknbasket.picknbasket.Abstracts.ListObject;
import org.picknbasket.picknbasket.Abstracts.EObjectType;
import org.picknbasket.picknbasket.Abstracts.IDBObject;
import org.picknbasket.picknbasket.Helpers.CustomContentValues;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Takunda on 2/17/2020.
 */

public class Basket extends ListObject implements IDBObject {

    private String name;
    private SyncLinkedHashMap<Integer, ShopItem> basketItems = new SyncLinkedHashMap<>();
    private float totalCost;
    private int totalItems;
    private float remainingCost;
    private int remainingItems;

    public Basket(String name){
        this.name=name;
    }

    public void calculate(){
        totalItems = basketItems.size();
        totalCost = 0;
        remainingCost = 0;
        remainingItems = 0;

        for (Map.Entry<Integer, ShopItem> entry: basketItems.entrySet()){
            ShopItem shopItem = entry.getValue();
            System.out.println("shop item id "+ entry.getKey());
            totalCost+=shopItem.getTotalPrice();
            if (!shopItem.isPicked()){
                remainingItems++;
                remainingCost += shopItem.getTotalPrice();
            }
        }
        // So it can be rounded to 2dp
        setTotalCost(totalCost);
    }

    private void countRecalculate(ShopItem shopItem, int oldCount){
        int countDiff = shopItem.getCount() - oldCount;
        double priceDiff = shopItem.getPrice() * countDiff;

        double total = priceDiff + getTotalCost();
        setTotalCost((float)total);
        if (!shopItem.isPicked()) {
            remainingCost += priceDiff;
        }
    }

    private void pickedRecalculate(ShopItem shopItem){
        if (shopItem.isPicked()){
            remainingCost -= shopItem.getTotalPrice();
            remainingItems--;
        }else {
            remainingCost += shopItem.getTotalPrice();
            remainingItems++;
        }
    }

    private void addedRecalculate(ShopItem shopItem, boolean add){
        if (add){
            totalItems++;
            remainingItems++;
            double total = totalCost + shopItem.getTotalPrice();
            setTotalCost((float)total);
            remainingCost += shopItem.getTotalPrice();
        }else{
            ShopItem basketItem = basketItems.get(shopItem.getId());
            totalItems--;
            double total = totalCost - shopItem.getTotalPrice();
            setTotalCost((float)total);
            if (basketItem == null || !basketItem.isPicked()){
                remainingItems--;
                remainingCost -= shopItem.getTotalPrice();
            }
        }
    }

    public ShopItem setPicked(ShopItem shopItem){
        ShopItem item = null;
        if (basketItems.containsKey(shopItem.getId())) {
            item = basketItems.get(shopItem.getId());
            item.setPicked(!item.isPicked());
            pickedRecalculate(item);
            System.out.println("set picked "+remainingItems);
        }
        return item;
    }

    public ShopItem setCount(ShopItem shopItem, int count){
        ShopItem item = null;
        if (basketItems.containsKey(shopItem.getId())) {
            item = basketItems.get(shopItem.getId());
            int oldCount = item.getCount();
            item.setCount(count);
            countRecalculate(item, oldCount);
            System.out.println("set count "+remainingItems);
        }
        return item;
    }

    /**
     * This method also recalculates the new values for basket
     * @param shopItem
     */
    public void addBasketItem(ShopItem shopItem){
        if (!basketItems.containsKey(shopItem.getId())) {
            System.out.println("basket item added "+basketItems.size()+" "+ shopItem.getName());
            basketItems.put(shopItem.getId(), shopItem);
            addedRecalculate(shopItem, true);
        }else{
            System.out.println("basket item contained "+basketItems.size()+" "+ shopItem.getName());
        }
    }

    /**
     * This method also recalculates the new values for basket
     * @param shopItem
     */
    public void removeBasketItem(ShopItem shopItem){
        if (basketItems.containsKey(shopItem.getId())) {
            basketItems.remove(shopItem.getId());
            addedRecalculate(shopItem, false);
        }
    }

    public String getCurrency(){
        if (basketItems.isEmpty()){
            return "";
        }
        ShopItem shopItem = basketItems.values().iterator().next();
        return shopItem.getCurrency();
    }

    /**
     * calculates the progress of items picked/purchased out of 100 as percentage
     * @return
     */
    public int getProgress(){
        int purchasedItems = getPurchasedItems();
        if (purchasedItems > 0){
            return (int)((purchasedItems / (float)totalItems)*100);
        }
        return 0;
    }

    public boolean isComplete(){
        if (totalItems == 0){
            return false;
        }
        if (totalItems == getPurchasedItems()){
            return true;
        }
        return false;
    }

    @Override
    public CharSequence getMainText() {
        return name;
    }

    @Override
    public EObjectType getListObjectType() {
        return null;
    }

    @Override
    public CharSequence getSecondaryText() {
        return null;
    }

    @Override
    public void setPrimaryView() {
        TextView amountTV = view.findViewById(R.id.amount_tv);
        TextView quantityTV = view.findViewById(R.id.quantity_tv);
        TextView progress = view.findViewById(R.id.progress);
        ImageView imageView = view.findViewById(R.id.primary_iv);
        ProgressBar progressBar = view.findViewById(R.id.progress_bar);

        amountTV.setText(getCurrency()+" "+getTotalCost()+"");
        quantityTV.setText(getTotalItems()+"");

        if (isComplete()){
            imageView.setImageResource(R.drawable.ic_done_all_white_24dp);
            imageView.setBackgroundResource(R.drawable.round_green_button);
            progress.setText("Done!");
        }else{
            progress.setText(getPurchasedItems()+" / "+getTotalItems());
        }
        progressBar.setProgress(getProgress());
    }

    @Override
    public ContentValues getContentValues() {
        CustomContentValues data = new CustomContentValues();

        data.put("name",name);

        return data.getContentValues();
    }

    @Override
    public void setPrimaryViewListener(){
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(listActivity, listItemClassActivity);
                intent.putExtra("basketId",id);
                listActivity.startActivity(intent);
            }
        });
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SyncLinkedHashMap<Integer, ShopItem> getBasketItems() {
        return basketItems;
    }

    public SyncLinkedHashMap<Integer, ShopItem> getPendingItems() {
        SyncLinkedHashMap<Integer, ShopItem> pendingItems = new SyncLinkedHashMap<>();
        for(ShopItem shopItem: basketItems.values()){
            if (!shopItem.isPicked()){
                pendingItems.put(shopItem.getId(),shopItem);
            }
        }
        return pendingItems;
    }


    public void setBasketItems(SyncLinkedHashMap<Integer, ShopItem> basketItems) {
        this.basketItems = basketItems;
        calculate();
    }

    public void resetBasketItems(){

    }

    public float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(float totalCost) {
        totalCost = (float)(Math.round(totalCost*100)/100D);
        this.totalCost = totalCost;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public float getRemainingCost() {
        float remainingCost =  (float)(Math.round(this.remainingCost*100)/100D);
        return remainingCost;
    }

    public float getSpentCost(){
        float spentCost = totalCost - remainingCost;;
        spentCost = (float)(Math.round(spentCost*100)/100D);
        return spentCost;
    }

    public int getPurchasedItems(){
        return totalItems - remainingItems;
    }

    public void setRemainingCost(float remainingCost) {
        this.remainingCost = remainingCost;
    }

    public int getRemainingItems() {
        return remainingItems;
    }

    public void setRemainingItems(int remainingItems) {
        this.remainingItems = remainingItems;
    }
}
