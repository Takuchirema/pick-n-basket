package org.picknbasket.picknbasket.Helpers;

import android.app.Activity;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import org.picknbasket.picknbasket.Abstracts.ListObject;
import org.picknbasket.picknbasket.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Takunda on 2/17/2020.
 */

public class ListAdapter extends ArrayAdapter<Integer> {
    private Activity listActivity;
    private Class listItemClassActivity;
    private ArrayList<Integer> ids = new ArrayList<>();
    private ArrayList<Integer> originalIds = new ArrayList<>();
    protected HashMap<Integer,? extends ListObject> mapObjects = new HashMap<>();
    protected ArrayList<ListObject> listObjects = new ArrayList<>();
    protected HashMap<Integer, ListObject> selectedItems = new HashMap<>();
    protected HashMap<Integer,View> cachedViews = new HashMap<>();
    private int layoutResource;
    private ViewGroup root;

    public ListAdapter(Activity listActivity, Class listItemClassActivity, ArrayList<Integer> ids, ViewGroup root, int layoutResource) {
        super(listActivity, layoutResource, ids);

        this.listActivity = listActivity;
        this.listItemClassActivity=listItemClassActivity;
        this.root=root;
        this.ids = ids;
        this.layoutResource=layoutResource;
        this.originalIds.addAll(ids);
    }

    @Override
    public int getCount(){
        return ids.size();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        //System.out.println("adapter get view pos: "+position+" s: "+ids.size());
        int objectId = ids.get(position);

        ListObject listObject = getBasketListObject(objectId);
        listObject.setListActivity(listActivity);
        listObject.setListItemClassActivity(listItemClassActivity);
        listObject.setPosition(position);

        view = cachedViews.get(objectId);
        if (view != null){
            prepareView(view,listObject);
            return view;
        }

        if (listObject == null){
            return view;
        }

        LayoutInflater inflater = listActivity.getLayoutInflater();
        View rowView= inflater.inflate(layoutResource, root, false);

        prepareView(rowView,listObject);

        cachedViews.put(objectId,rowView);
        return rowView;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private View prepareView(View view, ListObject listObject){
        listObject.setView(view);
        listObject.setAdapter(this);
        listObject.populateData();
        listObject.setListeners();
        return view;
    }

    private ListObject getBasketListObject(int objectId){
        ListObject listObject;
        if (!mapObjects.isEmpty()) {
            listObject = mapObjects.get(objectId);
        }else{
            listObject = listObjects.get(objectId);
        }

        return listObject;
    }

    public void selected(ListObject gridObject, boolean selected){
        if (selected){
            selectedItems.put(gridObject.getId(),gridObject);
        }else{
            selectedItems.remove(gridObject.getId());
        }
    }

    /**
     * marks all the list items as selected on the checkbox
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void selectAll(boolean select){
        selectedItems.clear();
        int count = 0;
        for (int id:ids){
            View view = getView(count,null,null);
            CheckBox selectItem = view.findViewById(R.id.select_item);
            if (selectItem != null){
                selectItem.setChecked(select);
                if (select) {
                    selectedItems.put(id, getBasketListObject(id));
                }
            }
            count++;
        }
        notifyDataSetChanged();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ids.clear();
        if (charText == null || charText.length() == 0) {
            ids.addAll(originalIds);
        }
        else
        {
            for (int id: originalIds) {
                ListObject object = getBasketListObject(id);
                String searchString = object.getSearchString().toLowerCase();
                if (searchString.contains(charText)) {
                    ids.add(id);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void setIds(ArrayList<Integer> ids) {
        originalIds.clear();
        cachedViews.clear();

        this.ids = ids;
        originalIds.addAll(ids);
    }

    public void setBasketListObjects(ArrayList<ListObject> listObjects) {
        this.listObjects = listObjects;
    }

    public void setMapObjects(HashMap<Integer,? extends ListObject> mapObjects) {
        this.mapObjects = mapObjects;
    }

    public HashMap<Integer, ListObject> getSelectedItems() {
        return selectedItems;
    }
}
