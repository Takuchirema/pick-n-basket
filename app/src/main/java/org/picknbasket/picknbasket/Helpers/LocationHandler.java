package org.picknbasket.picknbasket.Helpers;

import android.annotation.SuppressLint;
import android.location.Location;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.Task;

/**
 * Created by Takunda on 03/06/2018.
 */

public class LocationHandler {
    private static FusedLocationProviderClient fusedLocationClient;

    @SuppressLint("MissingPermission")
    public static Task<Location> getLastLocationTask() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(AppVariables.context);
        return fusedLocationClient.getLastLocation();
    }
}
