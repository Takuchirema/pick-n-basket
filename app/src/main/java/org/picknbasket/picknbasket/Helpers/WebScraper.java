package org.picknbasket.picknbasket.Helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslCertificate;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.RequiresApi;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.BaseInputConnection;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.GeolocationPermissions;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import static java.lang.Thread.sleep;

@SuppressLint("SetJavaScriptEnabled,unused,Deprecation")
public class WebScraper {

    private Context context;
    private WebView web;
    private boolean runInBackground;
    private String userAgent;
    public String URL;
    // This is html for the previous page that was loaded
    public String html;
    public String htmlUrl;
    public String currentUrl;

    WifiManager wifiManager;

    // This is only used internally
    private PageLoadedListener pageLoadedListener;
    // Used externally
    private PageLoadedListener onPageLoadedListener;
    private WaitForHtml HtmlListener;
    private List<Task> tasks = new ArrayList<>();

    private MyWebViewClient webViewClient = new MyWebViewClient();

    public static int MAX = -1;

    public WebScraper(final Context context, WebView webView) {
        this.context = context;
        if (webView != null){
            web=webView;
        }else {
            web = new WebView(context);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WebView.enableSlowWholeDocumentDraw();
        }
        WebSettings settings = web.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setDomStorageEnabled(true);
        settings.setGeolocationEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        JSInterface jInterface = new JSInterface(context);
        web.addJavascriptInterface(jInterface, "HtmlViewer");
        web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        web.setScrollbarFadingEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            web.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            web.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        userAgent = web.getSettings().getUserAgentString();
        web.setWebChromeClient(new WebChromeClient(){
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                // callback.invoke(String origin, boolean allow, boolean remember);
                callback.invoke(origin, true, false);
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                currentUrl = view.getUrl();
                if (webViewClient.getUrl() == null || !webViewClient.getUrl().equals(view.getUrl())){
                    htmlUrl = view.getUrl();
                    //Toast.makeText(context,"Something wrong with website! "+ htmlUrl,Toast.LENGTH_LONG).show();
                }
                super.onReceivedTitle(view, title);
            }
        });
        web.setWebViewClient(webViewClient);
    }

    private class MyWebViewClient extends WebViewClient {
        private String url;

        public String getUrl(){
            return url;
        }

        public void onPageFinished(WebView view, String url) {
            URL = url;
            Log.i("Webscraper",url);
            if (pageLoadedListener != null){
                Log.i("Webscraper","page load listener");
                pageLoadedListener.loaded(url);
                pageLoadedListener = null;
            }

            if (onPageLoadedListener != null){
                onPageLoadedListener.loaded(url);
            }

            if (runInBackground) {
                setWebViewMeasurements();
            }
        }

        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            System.out.println("web error "+error.toString());
        }

        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            System.out.println("web http error "+errorResponse.toString());
        }

        @Override
        public boolean shouldOverrideKeyEvent (WebView view, KeyEvent event) {
            //Toast.makeText(context,"Key Event",Toast.LENGTH_LONG).show();
            getHtml();
            return false;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                System.out.println("should override url 1" + request.getUrl());
            }
            return false;
        }

        @Override
        public boolean shouldOverrideUrlLoading (WebView view, String url) {
            this.url = url;
            //System.out.println("should override url 2" + url);
            getHtml();
            return false;
        }
    }

    public void setWebViewMeasurements(){
        web.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        web.layout(0, 0, web.getMeasuredWidth(), web.getMeasuredHeight());
        //noinspection deprecation
        web.setDrawingCacheEnabled(true);
    }

    public View getView() {
        return web;
    }

    public void setOnPageLoadedListener(PageLoadedListener pageLoadedListener){
        this.onPageLoadedListener=pageLoadedListener;
    }

    private class JSInterface {
        private Context ctx;
        JSInterface(Context ctx) {
            this.ctx = ctx;
        }
        @JavascriptInterface
        public void showHTML(String pageHtml) {
            // Check for empty document
            if (!pageHtml.equals("<head></head><body></body>")) {
                htmlUrl = URL;
                html = pageHtml;
            }
            if (HtmlListener != null){
                HtmlListener.gotHtml(html);
            }
        }
    }

    //Web tasks
    public void execute(final WebTaskListener webTaskListener){
        final Handler handler = new Handler();
        final int[] count = {0};
        final Dictionary<String, String> result = new Hashtable<>();
        Log.i("Webscraper", "Starting execution");
        TaskListener taskListener = new TaskListener() {
            @Override
            public void done() {
                Log.i("Webscraper", "Task Nr.: " + count[0]);
                if (count[0] < tasks.size()-1) {
                    count[0]++;
                    handler.post(tasks.get(count[0]));
                }else{
                    webTaskListener.finished(result);
                }
            }
        };

        for (Task task: tasks) task.set(taskListener, result);
        handler.post(tasks.get(0));
    }
    protected void addTask(final String javascript, final boolean storeResult, final String key){
        tasks.add(new Task() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void run() {
                web.evaluateJavascript(javascript, new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String s) {
                        if (storeResult) result.put(key, s.substring(1, s.length() - 1));
                        taskListener.done();
                    }
                });
            }
        });
    }

    protected void addTask(String javascript){
        addTask(javascript, false, null);
    }
    public void waitForElement(Element element, int timeout){
        final String js = "javascript:document.body.contains(" + element.getElement() + ")";
        for (int i = 0; i < timeout/300; i++){
            tasks.add(new Task() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void run() {
                    web.evaluateJavascript(js, new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String s) {
                            if (s.equals("false")) {
                                waitTime(300);
                            }
                            taskListener.done();
                        }
                    });
                }
            });
        }
    }

    public void log(final String text){
        tasks.add(new Task() {
            @Override
            public void run() {
                Log.i("Webscraper",text);
                taskListener.done();
            }
        });
    }
    public void waitForAttribute(Element element, String attribute, final String value, int timeout){
        final String js = "javascript:" + element.getElement() + "." + attribute + ";";
        for (int i = 0; i < timeout/300; i++){
            tasks.add(new Task() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void run() {
                    web.evaluateJavascript(js, new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String s) {
                            if (!s.equals(value)) {
                                waitTime(300);
                            }
                            taskListener.done();
                        }
                    });
                }
            });
        }
    }
    public void typeText(final int... keyevents){
        tasks.add(new Task() {
            @Override
            public void run() {
                BaseInputConnection inputConnection = new BaseInputConnection(getView(),true);
                for (int i: keyevents){
                    inputConnection.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,i));
                    inputConnection.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_UP,i));
                }
                waitTime(keyevents.length * 40);
                taskListener.done();
            }
        });
    }
    public void typeText(String text){
        int[] keys = new int[text.length()];
        for (int i = 0; i < text.length(); i++){
            keys[i] = KEYCODES(text.toLowerCase().charAt(i));
        }
        typeText(keys);
    }

    public void waitTime(final int millis){
        tasks.add(new Task() {
            @Override
            public void run() {
                waitTime(millis);
                taskListener.done();
            }
        });
    }
    public void waitForPage(){
        tasks.add(new Task() {
            @Override
            public void run() {
                pageLoadedListener = new PageLoadedListener() {
                    @Override
                    public void loaded(String url) {
                        taskListener.done();
                    }
                };
            }
        });
    }
    public void loadURL(final String Url){
        tasks.add(new Task() {
            @Override
            public void run() {
                System.out.println("web load "+Url);
                web.loadUrl(Url);
                pageLoadedListener = new PageLoadedListener() {
                    @Override
                    public void loaded(String url) {
                        System.out.println("web scraper done "+url);
                        taskListener.done();
                    }
                };
            }
        });
    }
    public void reload(){
        tasks.add(new Task() {
            @Override
            public void run() {
                web.reload();
                pageLoadedListener = new PageLoadedListener() {
                    @Override
                    public void loaded(String url) {
                        taskListener.done();
                    }
                };
            }
        });
    }
    public void reload(PageLoadedListener listener) {
        web.reload();
        pageLoadedListener = listener;
    }

    public void getHtml(WaitForHtml htmlListener) {
        //web.evaluateJavascript("(function(){return document.getElementsByTagName('html')[0].innerHTML})();", htmlListener::gotHtml);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            web.evaluateJavascript("if (document.getElementsByTagName('html')[0] !== 'undefined'){javascript:window.HtmlViewer.showHTML(document.getElementsByTagName('html')[0].innerHTML);}", null);
        }else{
            web.loadUrl("javascript:window.HtmlViewer.showHTML(document.getElementsByTagName('html')[0].innerHTML);", null);
        }
        this.HtmlListener = htmlListener;
    }

    public void getHtml(){
        web.loadUrl("javascript:window.HtmlViewer.showHTML(document.getElementsByTagName('html')[0].innerHTML);", null);
    }

    public void loadURL(String URL, PageLoadedListener listener) {
        this.URL = URL;
        web.loadUrl(URL);
        pageLoadedListener = listener;
    }

    //Get webview attributes
    public String getWebsiteTitle(){
        return web.getTitle();
    }
    public String getURL() {
        return web.getUrl();
    }
    public Bitmap getFavicon(){
        return web.getFavicon();
    }
    public SslCertificate getSslCertificate(){
        return web.getCertificate();
    }

    //Setup
    public void setUserAgentToDesktop(boolean desktop){
        if (desktop){
            String osString = userAgent.substring(userAgent.indexOf("("), userAgent.indexOf(")") + 1);
            web.getSettings().setUserAgentString(userAgent.replace(osString,"(X11; Linux x86_64)"));
        }else{
            web.getSettings().setUserAgentString(userAgent);
        }
    }
    public void setLoadImages(boolean enabled) {
        web.getSettings().setBlockNetworkImage(!enabled);
        web.getSettings().setLoadsImagesAutomatically(enabled);
    }

    //Screenshots
    public Bitmap takeScreenshot() { //Pay attention with big webpages
        return takeScreenshot(MAX, MAX);
    }
    public Bitmap takeScreenshot(int width, int height) {
        try {
            if (width < 0 || height < 0) {
                web.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            }
            if (width < 0) {
                width = web.getMeasuredWidth();
            }
            if (height < 0) {
                height = web.getMeasuredHeight();
            }
            web.layout(0, 0, width, height);
            //noinspection deprecation
            web.setDrawingCacheEnabled(true);
            try {
                sleep(30);
            } catch (InterruptedException ignored) {}
            //noinspection deprecation
            return Bitmap.createBitmap(web.getDrawingCache());
        } catch (NullPointerException ignored) {
            return null;
        }
    }
    public int getMaxHeight() {
        web.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        return web.getMeasuredHeight();
    }
    public int getMaxWidth() {
        web.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        return web.getMeasuredWidth();
    }

    //Browser data
    public void clearHistory() {
        web.clearHistory();
    }
    public void clearCache() {
        web.clearCache(true);
    }
    public void clearCookies(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        }else{
            CookieSyncManager cookieSync = CookieSyncManager.createInstance(context);
            cookieSync.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSync.stopSync();
            cookieSync.sync();
        }
    }
    public void clearAll(){
        clearHistory();
        clearCache();
        clearCookies();
    }

    //FindWebViewElement
    public Element findElementsByClassName(String classname, int id){
        return new Element(this, Element.CLASS, classname, id);
    }
    public Element findElementByClassName(String classname){
        return findElementsByName(classname, 0);
    }
    public Element findElementById(String id){
        return new Element(this, Element.ID, id, 0);
    }
    public Element findElementsByName(String name, int id){
        return new Element(this, Element.NAME, name,id);
    }
    public Element findElementByName(String name){
        return findElementsByName(name,0);
    }
    public Element findElementByXpath(String xpath){
        return new Element(this, Element.XPATH, xpath,0);
    }
    public Element findElementByCustomJavascript(String javascript){
        return new Element(this, Element.CUSTOM, javascript, 0);
    }
    public Element findElementsByAttribute(String attribute, String value, int count){
        return new Element(this, Element.ATTRIBUTE, attribute + "=" + value,count);
    }
    public Element findElementByAttribute(String attribute, String value){
        return findElementsByAttribute(attribute, value, 0);
    }
    public Element findElementsByValue(String value, int id){
        return new Element(this,Element.VALUE, value, id);
    }
    public Element findElementByInnerHtml(String innerHtml, String type){
        return new Element(this, Element.HTML, "//" + type + "[contains(text(), '" + innerHtml +"')]", 0);
    }
    public Element findElementByValue(String value){
        return findElementsByValue(value,0);
    }
    public Element findElementsByTitle(String title, int id){
        return new Element(this, Element.TITLE,title, id);
    }
    public Element findElementByTitle(String title){
        return findElementsByTitle(title,0);
    }
    public Element findElementsByTagName(String tagName, int id){
        return new Element(this,Element.TAG,tagName, id);
    }
    public Element findElementByTagName(String tagName){
        return findElementsByTagName(tagName,0);
    }
    public Element findElementsByType(String type, int id){
        return new Element(this, Element.TYPE, type, id);
    }
    public Element findElementByType(String type){
        return findElementsByType(type,0);
    }

    public interface PageLoadedListener{
        void loaded(String url);
    }
    public interface WaitForHtml{
        void gotHtml(String html);
    }
    public interface WebTaskListener{
        void finished(Dictionary<String, String> result);
    }
    private interface TaskListener{
        void done();
    }

    private abstract  class Task implements Runnable{
        TaskListener taskListener;
        Dictionary<String,String> result;
        public void set(TaskListener taskListener, Dictionary<String, String> result){
            this.taskListener = taskListener;
            this.result = result;
        }
        public void waitTime(int time){
            try {
                sleep(time);
            } catch (InterruptedException ignored) {}
        }
    }

    private static int KEYCODES(char ch){
        switch (ch) {
            case 'a': return KeyEvent.KEYCODE_A;
            case 'b': return KeyEvent.KEYCODE_B;
            case 'c': return KeyEvent.KEYCODE_C;
            case 'd': return KeyEvent.KEYCODE_D;
            case 'e': return KeyEvent.KEYCODE_E;
            case 'f': return KeyEvent.KEYCODE_F;
            case 'g': return KeyEvent.KEYCODE_G;
            case 'h': return KeyEvent.KEYCODE_H;
            case 'i': return KeyEvent.KEYCODE_I;
            case 'j': return KeyEvent.KEYCODE_J;
            case 'k': return KeyEvent.KEYCODE_K;
            case 'l': return KeyEvent.KEYCODE_L;
            case 'm': return KeyEvent.KEYCODE_M;
            case 'n': return KeyEvent.KEYCODE_N;
            case 'o': return KeyEvent.KEYCODE_O;
            case 'p': return KeyEvent.KEYCODE_P;
            case 'q': return KeyEvent.KEYCODE_Q;
            case 'r': return KeyEvent.KEYCODE_R;
            case 's': return KeyEvent.KEYCODE_S;
            case 't': return KeyEvent.KEYCODE_T;
            case 'u': return KeyEvent.KEYCODE_U;
            case 'v': return KeyEvent.KEYCODE_V;
            case 'w': return KeyEvent.KEYCODE_W;
            case 'x': return KeyEvent.KEYCODE_X;
            case 'y': return KeyEvent.KEYCODE_Y;
            case 'z': return KeyEvent.KEYCODE_Z;
            case '0': return KeyEvent.KEYCODE_0;
            case '1': return KeyEvent.KEYCODE_1;
            case '2': return KeyEvent.KEYCODE_2;
            case '3': return KeyEvent.KEYCODE_3;
            case '4': return KeyEvent.KEYCODE_4;
            case '5': return KeyEvent.KEYCODE_5;
            case '6': return KeyEvent.KEYCODE_6;
            case '7': return KeyEvent.KEYCODE_7;
            case '8': return KeyEvent.KEYCODE_8;
            case '9': return KeyEvent.KEYCODE_9;
            case '@': return KeyEvent.KEYCODE_AT;
            case '-': return KeyEvent.KEYCODE_MINUS;
            case '/': return KeyEvent.KEYCODE_SLASH;
            case '.': return KeyEvent.KEYCODE_PERIOD;
            case ',': return KeyEvent.KEYCODE_COMMA;
            case ';': return KeyEvent.KEYCODE_SEMICOLON;
            case ' ': return KeyEvent.KEYCODE_SPACE;
            default: return 0;
        }
    }

    //Network options
    public boolean isWifiEnabled(){
        if(wifiManager == null){
            wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        }
        return wifiManager.isWifiEnabled();
    }
    public void setWifiEnabled(boolean enabled){
        if(wifiManager == null){
            wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        }
        wifiManager.setWifiEnabled(enabled);
    }
    public boolean isWifiConnected(){
        if(wifiManager == null){
            wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        }
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return wifiInfo.getNetworkId() != -1;
    }

    public boolean isRunInBackground() {
        return runInBackground;
    }

    public void setRunInBackground(boolean runInBackground) {
        this.runInBackground = runInBackground;
    }
}