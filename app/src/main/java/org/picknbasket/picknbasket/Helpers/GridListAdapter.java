package org.picknbasket.picknbasket.Helpers;

import android.app.Activity;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import org.picknbasket.picknbasket.Abstracts.GridObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.picknbasket.picknbasket.Abstracts.IGridActivity;
import org.picknbasket.picknbasket.R;

/**
 * Created by Takunda on 21/06/2018.
 */

public class GridListAdapter extends ArrayAdapter<Integer> {

    private Activity listActivity;
    private IGridActivity gridActivity;
    private Class listItemClassActivity;
    private ArrayList<Integer> ids;
    private ArrayList<Integer> originalIds = new ArrayList<>();
    protected HashMap<Integer,? extends GridObject> mapObjects = new HashMap<>();
    protected ArrayList<GridObject> listObjects = new ArrayList<>();
    protected HashMap<Integer, GridObject> selectedItems = new HashMap<>();
    protected HashMap<Integer,View> cachedViews = new HashMap<>();
    private int layoutResource;
    private ViewGroup root;
    private boolean showNumberPicker;

    public GridListAdapter(Activity listActivity, Class listItemClassActivity, ArrayList<Integer> ids, ViewGroup root, int layoutResource) {
        super(listActivity, layoutResource, ids);

        this.listActivity = listActivity;
        this.listItemClassActivity=listItemClassActivity;
        this.root=root;
        this.ids = ids;
        this.layoutResource=layoutResource;
        this.originalIds.addAll(ids);
    }

    @Override
    public int getCount(){
        return ids.size();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        //System.out.println("adapter get view pos: "+position+" s: "+ids.size());
        int objectId = ids.get(position);

        GridObject listObject = getGridObject(objectId);
        listObject.setListActivity(listActivity);
        listObject.setListItemClassActivity(listItemClassActivity);
        listObject.setPosition(position);

        view = cachedViews.get(objectId);
        if (view != null){
            prepareView(view,listObject);
            return view;
        }

        if (listObject == null){
            return view;
        }

        LayoutInflater inflater = listActivity.getLayoutInflater();
        View rowView= inflater.inflate(layoutResource, root, false);

        prepareView(rowView,listObject);

        cachedViews.put(objectId,rowView);
        return rowView;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private View prepareView(View view, GridObject listObject){
        listObject.setView(view);
        //listObject.setAdapter(this);
        listObject.populateData();
        listObject.setListeners();
        return view;
    }

    private GridObject getGridObject(int objectId){
        GridObject listObject;
        if (!mapObjects.isEmpty()) {
            listObject = mapObjects.get(objectId);
        }else{
            listObject = listObjects.get(objectId);
        }

        return listObject;
    }

    public void selected(GridObject gridObject, boolean selected){
        if (selected){
            selectedItems.put(gridObject.getId(),gridObject);
        }else{
            selectedItems.remove(gridObject.getId());
        }

        if (listActivity instanceof IGridActivity){
            ((IGridActivity) listActivity).gridItemSelected(gridObject, selected);
        }else if (gridActivity != null){
            gridActivity.gridItemSelected(gridObject, selected);
        }
    }

    public void longClick(GridObject gridObject){
        if (listActivity instanceof IGridActivity){
            ((IGridActivity) listActivity).longClick(gridObject);
        }else if (gridActivity != null){
            gridActivity.longClick(gridObject);
        }
    }

    public void numberPicked(GridObject gridObject, int value){
        if (listActivity instanceof IGridActivity){
            ((IGridActivity) listActivity).numberPicked(gridObject, value);
        }else if (gridActivity != null){
            gridActivity.numberPicked(gridObject, value);
        }
    }

    public boolean isSelected(int id){
        if (selectedItems.containsKey(id)){
            return true;
        }
        return false;
    }

    /**
     * marks all the list items as selected on the checkbox
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void selectAll(boolean select){
        selectedItems.clear();
        int count = 0;
        for (int id:ids){
            View view = getView(count,null,null);
            CheckBox selectItem = view.findViewById(R.id.select_item);
            if (selectItem != null){
                selectItem.setChecked(select);
                if (select) {
                    selectedItems.put(id, getGridObject(id));
                }
            }
            count++;
        }
        notifyDataSetChanged();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ids.clear();
        if (charText == null || charText.length() == 0) {
            ids.addAll(originalIds);
        }
        else
        {
            for (int id: originalIds) {
                GridObject object = getGridObject(id);
                String searchString = object.getSearchString().toLowerCase();
                if (searchString.contains(charText)) {
                    ids.add(id);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void setIds(ArrayList<Integer> ids) {
        originalIds.clear();
        cachedViews.clear();

        this.ids = ids;
        originalIds.addAll(ids);
    }

    public void setGridObjects(ArrayList<GridObject> listObjects) {
        this.listObjects = listObjects;
    }

    public void setMapObjects(HashMap<Integer,? extends GridObject> mapObjects) {
        this.mapObjects = mapObjects;
    }

    public HashMap<Integer, GridObject> getSelectedItems() {
        return selectedItems;
    }

    public void setGridActivity(IGridActivity gridActivity) {
        this.gridActivity = gridActivity;
    }

    public void setShowNumberPicker(boolean showNumberPicker) {
        this.showNumberPicker = showNumberPicker;
    }

    public boolean showNumberPicker(){
        return showNumberPicker;
    }

    /**
     * Creates a deep copy to avoid unforeseen reference issues
     * @param selectedItems
     */
    public void setSelectedItems(HashMap<Integer, GridObject> selectedItems) {
        HashMap<Integer, GridObject> copy = new HashMap<>();
        for(Map.Entry<Integer, GridObject> entry : selectedItems.entrySet()){
            copy.put(entry.getKey(), entry.getValue());
        }
        this.selectedItems = copy;
    }
}
