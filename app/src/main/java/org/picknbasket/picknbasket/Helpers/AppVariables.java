package org.picknbasket.picknbasket.Helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.format.DateUtils;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.picknbasket.picknbasket.Abstracts.IActivity;
import org.picknbasket.picknbasket.Abstracts.ILocationListener;
import org.picknbasket.picknbasket.Abstracts.IMainActivity;
import org.picknbasket.picknbasket.Managers.NotificationsManager;
import org.picknbasket.picknbasket.Managers.ShopsManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by Takunda on 23/05/2018.
 */

public class AppVariables {
    public static String appLink =
            "Get all your essential services, transport, restaurants and more! " +
            "Install Now! https://play.google.com/store/apps/details?id=org.picknbasket.picknbasket.user";

    public static final String ADMOB_AD_UNIT_ID_DEBUG = "ca-app-pub-3940256099942544/2247696110";
    public static final String ADMOB_AD_UNIT_ID ="ca-app-pub-7323543099897376/7820588343";
    public static final String SHOW_TUTORIAL = "SHOW_TUTORIAL";
    public static String token;
    public static ArrayList<Integer> supportedVersions = new ArrayList<>();
    public static String versionName;
    public static String versionSuffix;
    public static String enterpriseId;
    public static int versionCode;
    public static String countryCode;
    public static int webScrapingDepth=1;
    public static int backgroundWebScrapingDepth=2;
    public static int shopScrapingNumber=2;
    private static Location location;
    public static String latlng2;
    public static int nearbyRadius = 1000; // in meters
    public static int pageRefreshInterval = 7; // in days
    // Error Log name
    public static String errorLogName = "Error_Log.txt";
    public static String[] saveKeywords = new String[]{"save","special","discount"};

    // For logging out
    public static IMainActivity mainActivity;
    public static Context context;

    public static String timeFormat = "yyyy-MM-dd_HH:mm";
    public static String fullTimeFormat = "yyyy-MM-dd_HH:mm:ss";

    private static LocationManager locationManager;
    private static NotificationsManager notificationsManager;
    private static HashMap<Integer, ILocationListener> locationListeners = new HashMap<>();

    public static String getDeviceId(Activity context) {
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    public static String setVersionName(Activity context) throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        String version = pInfo.versionName;
        versionName=version;
        return version;
    }

    public static int setVersionCode(Activity context) throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        int version = pInfo.versionCode;
        versionCode = version;
        return version;
    }

    public static String setVersionNameSuffix(Activity context) throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        String version = pInfo.versionName;
        String suffix = version.substring(version.indexOf("-")+1);
        versionSuffix = suffix;
        return suffix;
    }

    public static String setEnterpriseId(Activity context) throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        String version = pInfo.versionName;
        String id = version.substring(version.lastIndexOf(".")+1);
        enterpriseId = id;
        return id;
    }

    public static String getCountryCode(){
        if (countryCode != null && !countryCode.trim().isEmpty()){
            return countryCode;
        }

        TelephonyManager tm;
        if (mainActivity == null){
            tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        }else{
            tm = (TelephonyManager)mainActivity.getContext().getSystemService(Context.TELEPHONY_SERVICE);
        }

        String countryCodeValue = tm.getNetworkCountryIso();
        if (countryCodeValue == null || countryCodeValue.isEmpty()){
            return null;
        }
        countryCode = countryCodeValue;
        return countryCodeValue;
    }

    public static String getVersionNameSuffix(){
        return versionSuffix;
    }

    public static String getEnterpriseId(){
        System.out.println("** enterprise "+enterpriseId+" **");
        return enterpriseId;
    }

    public static Location getLocation(){
        return location;
    }

    public static void setLocation(Location location){
        AppVariables.location = location;
        setLatLng2(location);
    }

    public static void setLatLng2(Location location){
        String lat = Double.toString(location.getLatitude());
        String lng = Double.toString(location.getLongitude());

        String lat2 = lat.substring(0,lat.indexOf(".")+3);
        String lng2 = lng.substring(0,lng.indexOf(".")+3);
        String latlng2=lat2+","+lng2;

        AppVariables.latlng2 = latlng2;
    }

    @SuppressLint("SimpleDateFormat")
    public static String formatTime(String time){

        if (time == null){
            return null;
        }

        String formatted="";
        // SimpleDateFormat can be used to control the date/time display format:
        //   E (day of week): 3E or fewer (in text xxx), >3E (in full text)
        //   M (month): M (in number), MM (in number with leading zero)
        //              3M: (in text xxx), >3M: (in full text full)
        //   h (hour): h, hh (with leading zero)
        //   m (minute)
        //   s (second)
        //   a (AM/PM)
        //   H (hour in 0 to 23)
        //   z (time zone)

        SimpleDateFormat ft = new SimpleDateFormat(timeFormat);

        Date date;
        try {
            date = ft.parse(time);
            long now = System.currentTimeMillis();
            formatted = (DateUtils.getRelativeTimeSpanString(date.getTime(), now, DateUtils.MINUTE_IN_MILLIS)).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("formatted date "+formatted);
        return formatted;
    }

    public static void setNotificationManager(){
        // Get current location and send it out

        notificationsManager = new NotificationsManager();
        registerLocationListener(0,notificationsManager);
    }

    /**
     * The id is the layout resource file id e.g. R.layout.activity_shops
     * @param id
     * @param listener
     */
    public static void registerLocationListener(int id, ILocationListener listener){
        locationListeners.put(id,listener);
    }

    @SuppressLint("MissingPermission")
    public static void setLocationManager(){
        if (locationManager == null) {
            getCurrentLocation();
            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    TimeUnit.MINUTES.toMillis(0), // min time
                    1000, // min distance
                    locationListenerGPS);
        }
    }

    private static void getCurrentLocation(){
        Task<Location> locationTask = LocationHandler.getLastLocationTask();
        locationTask.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    AppVariables.setLocation(location);
                }
            }
        });
    }

    private static LocationListener locationListenerGPS=new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {
            AppVariables.setLocation(location);
            //Toast.makeText(context,"Location Changed "+locationListeners.size(),Toast.LENGTH_LONG).show();
            for(ILocationListener listener: locationListeners.values()){
                listener.onLocationChanged(location);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };
}