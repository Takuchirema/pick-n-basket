package org.picknbasket.picknbasket.Helpers;

import android.content.ContentValues;

/**
 * Created by Takunda on 1/20/2020.
 */

public class CustomContentValues{
    private ContentValues contentValues = new ContentValues();


    public void put(String key, String value) {
        if (value == null){
            contentValues.put(key,"");
            return;
        }
        contentValues.put(key,value);
    }

    public void putAll(ContentValues other) {
        if (other == null){
            return;
        }
        contentValues.putAll(other);
    }

    public void put(String key, Byte value) {
        if (value == null){
            return;
        }
        contentValues.put(key,value);
    }

    public void put(String key, Short value) {
        if (value == null){
            return;
        }
        contentValues.put(key,value);
    }

    public void put(String key, Integer value) {
        contentValues.put(key,value);
    }

    public void put(String key, Long value) {
        if (value == null){
            return;
        }
        contentValues.put(key,value);
    }

    public void put(String key, Float value) {
        if (value == null){
            return;
        }
        contentValues.put(key,value);
    }

    public void put(String key, Double value) {
        if (value == null){
            return;
        }
        contentValues.put(key,value);
    }

    public void put(String key, Boolean value) {
        if (value == null){
            return;
        }
        contentValues.put(key,value);
    }

    public void put(String key, byte[] value) {
        if (value == null){
            return;
        }
        contentValues.put(key,value);
    }

    public ContentValues getContentValues(){
        return contentValues;
    }
}
