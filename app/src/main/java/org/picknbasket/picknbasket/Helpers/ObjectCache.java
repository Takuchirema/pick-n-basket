package org.picknbasket.picknbasket.Helpers;

import org.picknbasket.picknbasket.Objects.Basket;
import org.picknbasket.picknbasket.Objects.Shop;
import org.picknbasket.picknbasket.Objects.ShopItem;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Takunda on 2/10/2020.
 */

public class ObjectCache {
    private static SyncLinkedHashMap<Integer,Shop> shops = new SyncLinkedHashMap<>();
    private static SyncLinkedHashMap<Integer,Basket> baskets = new SyncLinkedHashMap<>();

    public static Shop updateShop(Shop newShop, boolean sort){
        Shop oldShop = shops.get(newShop.getId());

        if (oldShop != null){
            oldShop.setPictureUrl(newShop.getPictureUrl());
            oldShop.setWebsite(newShop.getWebsite());
            oldShop.setPhoneNumber(newShop.getPhoneNumber());
            oldShop.setBusinessHours(newShop.getBusinessHours());
        }else{
            shops.put(newShop.getId(), newShop);
        }

        if (sort){
            shops.sort();
        }
        return oldShop;
    }

    public static void refreshShops(SyncLinkedHashMap<Integer,Shop> newShops){
        if (shops.isEmpty()){
            //we want these to be by reference so whatever changes happen also happen in cache
            shops = newShops;
            shops.sort();
            return;
        }

        for (Map.Entry<Integer,Shop> e:newShops.entrySet()){
            Shop newShop = e.getValue();
            updateShop(newShop, false);
        }

        // remove all shops not in new shops
        for (Map.Entry<Integer,Shop> e:shops.entrySet()){
            Shop shop = e.getValue();
            if (!newShops.containsKey(shop.getId())){
                shops.remove(shop.getId());
            }
        }
        shops.sort();
    }

    public static Basket updateBasket(Basket newBasket){
        Basket oldBasket = baskets.get(newBasket.getId());

        if (oldBasket != null){
            oldBasket.setBasketItems(newBasket.getBasketItems());
        }else{
            baskets.put(newBasket.getId(), newBasket);
        }

        return oldBasket;
    }
    
    public static void refreshBaskets(SyncLinkedHashMap<Integer,Basket> newBaskets){

        if (baskets.isEmpty()){
            //we want these to be by reference so whatever changes happen also happen in cache
            baskets = newBaskets;
            //copyBaskets(baskets,newBaskets);
            return;
        }

        for (Map.Entry<Integer,Basket> e:newBaskets.entrySet()){
            Basket newBasket = e.getValue();
            updateBasket(newBasket);
        }
        baskets.clear();
        baskets.putAll(newBaskets);
    }

    public static void refreshShopItems(HashMap<Integer, ShopItem> newShopItems){

        for (Map.Entry<Integer,ShopItem> cp: newShopItems.entrySet()){
            ShopItem newShopItem = cp.getValue();
            int shopId = newShopItem.getShopId();

            Shop shop = shops.get(shopId);
            if (shop == null){
                continue;
            }
            HashMap<Integer, ShopItem> shopItems = shop.getShopItems();

            ShopItem oldItem = shopItems.get(newShopItem.getId());
            if (oldItem == null){
                shopItems.put(newShopItem.getId(),newShopItem);
                continue;
            }

            newShopItem.setBmp(oldItem.getBmp());
            shopItems.put(newShopItem.getId(),newShopItem);
        }
    }

    public static void copyShops(Map<Integer, Shop> copy,Map<Integer, Shop> original){
        for(Map.Entry<Integer, Shop> entry : original.entrySet()){
            copy.put(entry.getKey(), entry.getValue());
        }
    }

    public static SyncLinkedHashMap<Integer,Shop> getShops(){
        return shops;
    }

    public static Shop getShop(int shopId){
        return shops.get(shopId);
    }

    public static ShopItem getShopItem(int shopId, int shopItemId){
        Shop shop = shops.get(shopId);
        if (shop == null){
            return null;
        }

        return shop.getShopItems().get(shopItemId);
    }

    public static SyncLinkedHashMap<Integer,Basket> getBaskets(){
        return baskets;
    }

    public static Basket getBasket(int basketId){
        return baskets.get(basketId);
    }
}
