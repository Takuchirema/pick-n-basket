package org.picknbasket.picknbasket.Helpers;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.picknbasket.picknbasket.Abstracts.INumberPickerListener;
import org.picknbasket.picknbasket.R;

public class NumberPicker extends LinearLayout {
    private TextView et_number;
    private int min, max;
    private INumberPickerListener listener;

    public NumberPicker(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        inflate(context, R.layout.number_picker, this);

        et_number = findViewById(R.id.et_number);

        final Button btn_less = findViewById(R.id.btn_less);
        btn_less.setOnClickListener(new AddHandler(-1));

        final Button btn_more = findViewById(R.id.btn_more);
        btn_more.setOnClickListener(new AddHandler(1));
    }

    public void setListener(INumberPickerListener listener) {
        this.listener = listener;
    }

    /***
     * HANDLERS
     **/
    private class AddHandler implements OnClickListener {
        final int diff;

        public AddHandler(int diff) {
            this.diff = diff;
        }

        @Override
        public void onClick(View view) {
            int newValue = getValue() + diff;
            if (newValue < min) {
                newValue = min;
            }
            else if (max > 0 && newValue > max) {
                newValue = max;
            }
            System.out.println("On click "+diff+" "+newValue);
            et_number.setText(String.valueOf(newValue));
            listener.onNumberPicked(newValue);
        }
    }

    /***
     * GETTERS & SETTERS
     */

    public int getValue() {
        if (et_number != null) {
            try {
                final String value = et_number.getText().toString();
                return Integer.parseInt(value);
            } catch (NumberFormatException ex) {
                Log.e("NumberPicker", ex.toString());
            }
        }
        return 0;
    }

    public void setValue(final int value) {
        if (et_number != null) {
            et_number.setText(String.valueOf(value));
        }
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
}
