package org.picknbasket.picknbasket.Helpers;

import android.os.Build;
import android.view.View;

import org.picknbasket.picknbasket.Abstracts.GridObject;
import org.picknbasket.picknbasket.R;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

// stores and recycles views as they are scrolled off screen
public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    protected View view;
    protected GridRecyclerAdapter adapter;

    ViewHolder(View itemView,GridRecyclerAdapter adapter) {
        super(itemView);
        this.view = itemView;
        this.adapter=adapter;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public View prepareView(GridObject listObject){
        view.findViewById(R.id.ad_container).setVisibility(View.GONE);
        view.findViewById(R.id.grid_view_container).setVisibility(View.VISIBLE);

        listObject.setView(view);
        listObject.setAdapter(adapter);
        listObject.setUpView();
        listObject.setListeners();
        listObject.populateData();
        return view;
    }

    public void prepareAdView(){
    }

    @Override
    public void onClick(View view) {
    }

    public View getView(){
        return view;
    }
}

