package org.picknbasket.picknbasket.Helpers;

import org.picknbasket.picknbasket.Objects.Shop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Semaphore;

/**
 * Created by Takunda on 2018/01/12.
 */

public class SyncLinkedHashMap<K,V> extends LinkedHashMap<K,V> implements Iterable<V> {

    private Semaphore lock = new Semaphore(1);
    private Date lastAccessTime;

    public void sort(){
        ArrayList<Entry<K,V>> itemList = new ArrayList<>();
        for(Map.Entry<K,V> e:entrySet()){
            itemList.add(e);
        }
        Collections.sort(itemList, new Comparator<Entry<K,V>>() {
            public int compare(Entry<K,V> a, Entry<K,V> b){
                if (!(a.getValue() instanceof Comparable)){
                    return 0;
                }
                return ((Comparable)a.getValue()).compareTo(b.getValue());
            }
        });
        clear();
        for (Entry<K,V> item:itemList){
            put(item.getKey(),item.getValue());
        }
    }

    @Override
    public synchronized V put(K key, V value){
        /*if (lock.isHeldByCurrentThread()) {
            try {
                //System.out.println("put lock wait");
                lock.wait();
                put(key,value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }*/
        return super.put(key,value);
    }

    @Override
    public synchronized Set<Entry<K, V>> entrySet(){
        return super.entrySet();
    }

    @Override
    public synchronized V get(Object var1){
        return super.get(var1);
    }

    @Override
    public V remove(Object key) {
        ////System.out.println("remove lock wait");
        /*try {
            lock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        return super.remove(key);
    }

    @Override
    public Iterator<V> iterator() {
        /*try {
            lock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        final Iterator<V> iterator = super.values().iterator();

        Iterator<V> it = new Iterator<V>() {

            @Override
            public boolean hasNext() {
                /*if (!iterator.hasNext()) {
                    //System.out.println("has next lock unlocking");
                    lock.release();
                }*/
                return iterator.hasNext();
            }

            @Override
            public V next() {
                return iterator.next();
            }

            @Override
            public void remove() {
                iterator.remove();
            }
        };
        return it;
    }

}
