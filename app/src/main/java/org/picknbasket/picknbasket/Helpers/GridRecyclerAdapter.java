package org.picknbasket.picknbasket.Helpers;

import android.app.Activity;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import org.picknbasket.picknbasket.Abstracts.GridObject;
import org.picknbasket.picknbasket.Abstracts.IGridActivity;
import org.picknbasket.picknbasket.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class GridRecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {

    private Activity listActivity;
    private IGridActivity gridActivity;
    private Class listItemClassActivity;
    private ArrayList<Integer> ids;
    private ArrayList<Integer> originalIds = new ArrayList<>();
    protected HashMap<Integer,? extends GridObject> mapObjects = new HashMap<>();
    protected ArrayList<GridObject> listObjects = new ArrayList<>();
    protected SyncLinkedHashMap<Integer, GridObject> selectedItems = new SyncLinkedHashMap<>();
    //protected HashMap<Integer,ViewHolder> cachedViews = new HashMap<>();
    private int layoutResource;
    private ViewGroup root;
    private boolean showNumberPicker;
    private GridRecyclerAdapter adapter;
    private int adsPosition;

    public GridRecyclerAdapter(Activity listActivity, Class listItemClassActivity, ArrayList<Integer> ids, ViewGroup root, int layoutResource) {
        adapter = this;
        this.listActivity = listActivity;
        this.listItemClassActivity=listItemClassActivity;
        this.root=root;
        this.ids = ids;
        this.layoutResource=layoutResource;
        this.originalIds.addAll(ids);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = listActivity.getLayoutInflater();
        View rowView= inflater.inflate(layoutResource, root, false);
        return new ViewHolderAds(rowView,this);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int objectId = ids.get(position);

        if (insertAd(objectId,position)){
            holder.prepareAdView();
            return;
        }

        GridObject listObject = getGridObject(objectId);
        listObject.setListActivity(listActivity);
        listObject.setListItemClassActivity(listItemClassActivity);
        listObject.setPosition(position);
        holder.prepareView(listObject);
    }

    /**
     * Just checks if the position is meant to add an advert or not
     * @param objectId
     * @param position
     * @return
     */
    public boolean insertAd(int objectId, int position){
        if (adsPosition == 0){
            return false;
        }
        if (objectId == -1){
            return true;
        }
        int rem = (position + 1) % adsPosition;
        if (rem == 0){
            return true;
        }
        return false;
    }

    public void setAdverts(GridLayoutManager layoutManager, final int adsPosition){
        this.adsPosition = adsPosition;
        // -1 id means that the position is for an advert
        for (int i=0;i<ids.size();i++){
            if (insertAd(0,i)){
                ids.add(i,-1);
            }
        }
        // put in id's into original ids for filtering during search
        originalIds.clear();
        originalIds.addAll(ids);
        layoutManager.setSpanSizeLookup( new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int spanSize = 1;

                int rem = (position + 1) % adsPosition;
                if (rem == 0){
                    spanSize = 2;
                }

                //System.out.println("pos: "+position+" rem: "+rem+" span: "+spanSize);
                return spanSize;
            }
        });
    }

    @Override
    public int getItemCount() {
        return ids.size();
    }

    private GridObject getGridObject(int objectId){
        GridObject listObject;
        if (!mapObjects.isEmpty()) {
            listObject = mapObjects.get(objectId);
        }else{
            listObject = listObjects.get(objectId);
        }

        return listObject;
    }

    public void selected(GridObject gridObject, boolean selected){
        if (selected){
            //System.out.println("selected "+gridObject.getId());
            selectedItems.put(gridObject.getId(),gridObject);
        }else{
            selectedItems.remove(gridObject.getId());
        }

        if (listActivity instanceof IGridActivity){
            ((IGridActivity) listActivity).gridItemSelected(gridObject, selected);
        }else if (gridActivity != null){
            gridActivity.gridItemSelected(gridObject, selected);
        }
    }

    public void longClick(GridObject gridObject){
        if (listActivity instanceof IGridActivity){
            ((IGridActivity) listActivity).longClick(gridObject);
        }else if (gridActivity != null){
            gridActivity.longClick(gridObject);
        }
    }

    public void numberPicked(GridObject gridObject, int value){
        if (listActivity instanceof IGridActivity){
            ((IGridActivity) listActivity).numberPicked(gridObject, value);
        }else if (gridActivity != null){
            gridActivity.numberPicked(gridObject, value);
        }
    }

    public boolean isSelected(int id){
        if (selectedItems.containsKey(id)){
            return true;
        }
        return false;
    }

    /**
     * marks all the list items as selected on the checkbox
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void selectAll(boolean select){
        selectedItems.clear();
        int count = 0;
        for (int id: ids) {
            if (id < 0){
                continue;
            }
            ViewHolder holder = onCreateViewHolder(null,0);
            onBindViewHolder(holder,count);
            View view = holder.getView();

            CheckBox selectItem = view.findViewById(R.id.select_item);
            if (selectItem != null){
                selectItem.setChecked(select);
                if (select) {
                    selectedItems.put(id, getGridObject(id));
                }
            }
            count++;
        }
        notifyDataSetChanged();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase();
        ids.clear();
        if (charText == null || charText.length() == 0) {
            ids.addAll(originalIds);
        }
        else
        {
            for (int id: originalIds) {
                if (id < 0){
                    continue;
                }
                GridObject object = getGridObject(id);
                String searchString = object.getSearchString().toLowerCase();
                if (searchString.contains(charText)) {
                    ids.add(id);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void setIds(ArrayList<Integer> ids) {
        originalIds.clear();

        this.ids = ids;
        originalIds.addAll(ids);
    }

    public void setGridObjects(ArrayList<GridObject> listObjects) {
        this.listObjects = listObjects;
    }

    public void setMapObjects(HashMap<Integer,? extends GridObject> mapObjects) {
        this.mapObjects = mapObjects;
    }

    public SyncLinkedHashMap<Integer, GridObject> getSelectedItems() {
        return selectedItems;
    }

    public void setGridActivity(IGridActivity gridActivity) {
        this.gridActivity = gridActivity;
    }

    public void setShowNumberPicker(boolean showNumberPicker) {
        this.showNumberPicker = showNumberPicker;
    }

    public boolean showNumberPicker(){
        return showNumberPicker;
    }

    /**
     * Creates a deep copy to avoid unforeseen reference issues
     * @param selectedItems
     */
    public void setSelectedItems(SyncLinkedHashMap<Integer, GridObject> selectedItems) {
        SyncLinkedHashMap<Integer, GridObject> copy = new SyncLinkedHashMap<>();
        for(Map.Entry<Integer, GridObject> entry : selectedItems.entrySet()){
            copy.put(entry.getKey(), entry.getValue());
        }
        this.selectedItems = copy;
    }
}
