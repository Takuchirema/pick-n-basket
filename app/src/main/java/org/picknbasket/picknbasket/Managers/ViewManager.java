package org.picknbasket.picknbasket.Managers;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import org.picknbasket.picknbasket.Abstracts.DataSet;
import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Abstracts.EHttpMethod;
import org.picknbasket.picknbasket.Abstracts.GridObject;
import org.picknbasket.picknbasket.Abstracts.IGetDataSet;
import org.picknbasket.picknbasket.Abstracts.IGetPicture;
import org.picknbasket.picknbasket.Data.PictureDataSet;

/**
 * Created by Takunda on 26/06/2018.
 *
 * This class is for handling what happens to a view after receiving database response.
 */

public class ViewManager implements IGetDataSet, IGetPicture {

    private EActionType actionType;
    private View view;
    private ArrayAdapter<Integer> adapter;

    private String objectId;
    private Object object;

    private String url;
    private EHttpMethod httpMethod = EHttpMethod.POST;
    private ViewManager context;
    private Activity viewActivity;

    private String comment;

    private boolean setImageBackground = false;

    public ViewManager(View view, EActionType actionType, Activity viewActivity){
        context=this;
        this.actionType=actionType;
        this.view=view;
        this.viewActivity=viewActivity;
    }

    public void getPicture(){
        PictureDataSet pictureDataSet = new PictureDataSet(url,this);
        pictureDataSet.getDataSet();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        if (dataSet.success){
            switch (actionType){
            }
        }else{
            if (viewActivity != null) {
                Toast.makeText(viewActivity, dataSet.message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void postGetPicture(Bitmap bmp) {
        if (bmp == null){
            return;
        }

        if (object instanceof GridObject){
            System.out.println("post get pic shop");
            ((GridObject)object).setBmp(bmp);
        }

        setPicture(bmp);

        if (adapter != null){
            adapter.notifyDataSetChanged();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setPicture(Bitmap bmp){
        if (view != null) {
            Drawable drawable = new BitmapDrawable(viewActivity.getResources(), bmp);
            if (setImageBackground) {
                view.setBackground(drawable);
                view.invalidate();
            }else{
                ((ImageView)view).setImageDrawable(drawable);
                view.invalidate();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setPicture(Drawable drawable){
        if (drawable == null){
            return;
        }
        if (view != null) {
            if (setImageBackground) {
                view.setBackground(drawable);
                view.invalidate();
            }else{
                ((ImageView)view).setImageDrawable(drawable);
                view.invalidate();
            }
        }
    }

    public void setAdapter(ArrayAdapter<Integer> adapter) {
        this.adapter = adapter;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public EHttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(EHttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    public void setImageBackground(boolean setImageBackground) {
        this.setImageBackground = setImageBackground;
    }

    public Activity getViewActivity() {
        return viewActivity;
    }

    public void setViewActivity(Activity viewActivity) {
        this.viewActivity = viewActivity;
    }
}
