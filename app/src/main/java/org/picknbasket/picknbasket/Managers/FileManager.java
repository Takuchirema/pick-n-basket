package org.picknbasket.picknbasket.Managers;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.picknbasket.picknbasket.Abstracts.IGetServerFile;
import org.picknbasket.picknbasket.Abstracts.ISendServerFile;
import org.picknbasket.picknbasket.Helpers.AppVariables;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Takunda on 7/28/2018.
 */

public class FileManager implements ISendServerFile, IGetServerFile {

    private Context context;
    private IGetServerFile returnServerFile;

    public FileManager(Context context){
        this.context=context;
    }

    public FileManager(){
    }

    public void checkErrorLogs(){
        if (fileExists(AppVariables.errorLogName)){
            sendErrorLogs();
        }
    }

    public void sendLogs(String fileName, String url){
    }

    public void sendErrorLogs(){
    }

    /* File name includes the extension*/
    public  void writeToFile(String fileName, String body)
    {
        FileOutputStream fos;

        try {
            File file = getFile(fileName);

            fos = new FileOutputStream(file,true);

            fos.write((body+"\n").getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Checking the main storage only
    public File getFile(String fileName){
        final File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+fileName);

        try{
            if (file.exists()){
                return file;
            }else{
                return createNewFile(fileName);
            }
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    // Checking the main storage only
    public boolean fileExists(String fileName){
        final File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+fileName);

        if (file.exists()){
            return true;
        }
        return false;
    }

    /* Will delete existing file*/
    public File createNewFile(String fileName) throws IOException {
        final File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+fileName);

        if (file.exists()){
            file.delete();
        }

        file.createNewFile();

        return file;
    }

    public void getServerFile(IGetServerFile returnServerFile, String url, String extension){
    }

    @Override
    public void postGetServerFile(File file) {
        if (this.returnServerFile != null){
            this.returnServerFile.postGetServerFile(file);
        }
    }

    @Override
    public void postSendServerFile(JSONObject data) throws JSONException {

        if (data == null){
            return;
        }

        int success = data.getInt("success");

        if (success == 1){
            String fileName = data.getString("fileName");
            deleteFile(fileName);
        }
    }

    public void deleteFile(String fileName){
        final File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" );

        if (!dir.exists())
        {
            if(!dir.mkdirs()){
                Log.e("ALERT","could not create the directories");
            }
        }

        final File myFile = new File(dir, fileName);
        myFile.delete();
    }
}
