package org.picknbasket.picknbasket.Managers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;

import org.picknbasket.picknbasket.Abstracts.DataSet;
import org.picknbasket.picknbasket.Abstracts.EObjectType;
import org.picknbasket.picknbasket.Abstracts.IGetDataSet;
import org.picknbasket.picknbasket.Abstracts.ILocationListener;
import org.picknbasket.picknbasket.Data.BasketsDataSet;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.ObjectCache;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Objects.Basket;
import org.picknbasket.picknbasket.Objects.Shop;
import org.picknbasket.picknbasket.Objects.ShopItem;
import org.picknbasket.picknbasket.R;
import org.picknbasket.picknbasket.Views.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationsManager implements ILocationListener, IGetDataSet {

    private HashMap<EObjectType,ArrayList<Integer>> notificationIds = new HashMap<>();

    @Override
    public void onLocationChanged(Location location) {
        createNotifications();
    }

    public void createNotifications(){
        SyncLinkedHashMap<Integer, Basket> baskets = getPendingBaskets();

        if (baskets.isEmpty()) {
            return;
        }

        createNotifications(baskets);
    }

    public void createNotifications(SyncLinkedHashMap<Integer, Basket> baskets){
        for(Basket basket: baskets.values()){
            SyncLinkedHashMap<Integer, ShopItem> pendingItems = basket.getPendingItems();
            createNotification(basket, pendingItems);
        }
    }

    /**
     * The pending items all belong to one basket.
     * Every notification is per basket.
     * @param pendingItems
     */
    public void createNotification(Basket basket, SyncLinkedHashMap<Integer, ShopItem> pendingItems){
        if (pendingItems.isEmpty()){
            return;
        }

        HashMap<Integer,String> shopMessages = new HashMap<>();
        NotificationCompat.InboxStyle notificationCompat = new NotificationCompat.InboxStyle();
        String title = "'"+basket.getName()+"' from these nearby shops:";

        for(ShopItem shopItem: pendingItems.values()){
            System.out.println("create notification "+shopItem.getName());
            Shop shop = ObjectCache.getShop(shopItem.getShopId());
            if (!shopMessages.containsKey(shop.getId()) && shop != null && shop.isNearby()){
                int distance = (int)shop.getDistanceFrom();
                String message = distance+"m away: Pick '"+shopItem.getName()+"' from '"+shop.getName()+"'";
                shopMessages.put(shop.getId(),message);
                notificationCompat.addLine(message);
            }
        }

        if (shopMessages.isEmpty()){
            return;
        }

        Intent intent = new Intent(AppVariables.context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("notification",true);

        PendingIntent pendingIntent = PendingIntent.getActivity(AppVariables.context, 0, intent, 0);
        notify(basket.getId(), EObjectType.BASKET, title, null, notificationCompat, pendingIntent);
    }

    public void notify(Integer id, EObjectType type, String title, String message,
                       NotificationCompat.InboxStyle inbox, PendingIntent intent){
        Context context = AppVariables.context;
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);

        if (notificationIds.containsKey(type) && notificationIds.get(type).contains(id)){
            return;
        }

        if (!notificationIds.containsKey(type)){
            ArrayList<Integer> ids = new ArrayList<>();
            ids.add(id);
            notificationIds.put(type,ids);
        }else{
            notificationIds.get(type).add(id);
        }

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(AppVariables.context, EObjectType.BASKET.toString())
                .setSmallIcon(R.drawable.logo_orange)
                .setContentIntent(intent)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setContentTitle(title)
                .setVibrate(new long[] { 1000, 1000})
                .setSound(alarmSound)
                .setLights(Color.BLUE, 3000, 3000);

        if (inbox != null){
            notificationBuilder.setStyle(inbox);
        }

        if (message != null){
            notificationBuilder.setContentText(message);
        }

        notificationManager.notify(id, notificationBuilder.build());
    }

    public SyncLinkedHashMap<Integer, Basket> getPendingBaskets(){
        SyncLinkedHashMap<Integer, Basket> baskets = ObjectCache.getBaskets();

        if (baskets.isEmpty()){
            BasketsDataSet dataSet = new BasketsDataSet(this);
            dataSet.getDataSet();
        }

        return baskets;
    }

    public SyncLinkedHashMap<Integer, ShopItem> getPendingItems(SyncLinkedHashMap<Integer, Basket> baskets){
        SyncLinkedHashMap<Integer, ShopItem> pendingItems = new SyncLinkedHashMap<>();
        for(Basket basket:baskets.values()){
             pendingItems.putAll(basket.getPendingItems());
        }
        return pendingItems;
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        if (dataSet.success == true) {
            if (dataSet instanceof BasketsDataSet) {
                BasketsDataSet basketsDataSet = (BasketsDataSet)dataSet;
                SyncLinkedHashMap<Integer, Basket> baskets = basketsDataSet.getBaskets();
                createNotifications(baskets);
            }
        }
    }
}
