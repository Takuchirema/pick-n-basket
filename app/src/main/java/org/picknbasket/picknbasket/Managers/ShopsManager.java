package org.picknbasket.picknbasket.Managers;

import android.app.Activity;
import android.location.Location;
import androidx.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.picknbasket.picknbasket.Abstracts.DataSet;
import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Abstracts.IActivity;
import org.picknbasket.picknbasket.Abstracts.IDataAdapterCall;
import org.picknbasket.picknbasket.Abstracts.IGetDataSet;
import org.picknbasket.picknbasket.Abstracts.IManager;
import org.picknbasket.picknbasket.Abstracts.IWebViewActivity;
import org.picknbasket.picknbasket.Data.SQLite;
import org.picknbasket.picknbasket.Data.ShopDetailsDataSet;
import org.picknbasket.picknbasket.Data.ShopItemsDataAdapter;
import org.picknbasket.picknbasket.Data.ShopItemsDataSet;
import org.picknbasket.picknbasket.Data.ShopsDataSet;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.LocationHandler;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Objects.Shop;
import org.picknbasket.picknbasket.Objects.ShopItem;
import org.picknbasket.picknbasket.R;

/**
 * Created by Takunda on 1/10/2020.
 */

public class ShopsManager implements IManager, IGetDataSet, IDataAdapterCall {
    private IActivity activity;
    private boolean populateView = true;
    private String message = "New items added please refresh screen";

    @Override
    public void startManager() { }

    public void getShops(Location location, IActivity activity){
        this.activity=activity;
        LatLng latLng = null;
        ShopsDataSet shopsDataSet = new ShopsDataSet(this);
        if (location != null) {
            latLng = new LatLng(location.getLatitude(), location.getLongitude());
        }
        shopsDataSet.getDataSet(latLng);
    }

    public void getShop(int shopId, IActivity activity){
        this.activity=activity;
        ShopDetailsDataSet shopDetailsDataSet = new ShopDetailsDataSet(this);
        shopDetailsDataSet.getDataSet(shopId);
    }

    public void storeShop(Shop shop){
        SQLite.storeShop(shop,shop.getLatLng2());
    }

    public void getShopItems(int shopId, IWebViewActivity activity){
        this.activity=activity;
        ShopItemsDataSet shopItemsDataSet = new ShopItemsDataSet(this);
        shopItemsDataSet.getDataSet(shopId,activity);
    }

    public void getShopItem(int shopId, int shopItemId, IActivity activity){
        this.activity = activity;
        ShopItemsDataSet shopItemsDataSet = new ShopItemsDataSet(this);
        shopItemsDataSet.setShopItemId(shopItemId);
        shopItemsDataSet.setShopId(shopId);
        shopItemsDataSet.getDataSet();
    }

    public void getShopItems(int shopId, String url, IWebViewActivity activity, String html){
        this.activity=activity;
        populateView=false;
        ShopItemsDataSet shopItemsDataSet = new ShopItemsDataSet(this);
        shopItemsDataSet.setUrl(url);
        shopItemsDataSet.setHtml(html);
        shopItemsDataSet.getDataSet(shopId, activity);
    }

    /**
     * This will go and delete all shop items and the pages scrapped from
     */
    public void resetShopItems(IActivity activity){
        this.activity=activity;
        ShopItemsDataAdapter dataAdapter = new ShopItemsDataAdapter(this);
        dataAdapter.resetShopItems();
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        if (dataSet.success == true){
            if (dataSet instanceof ShopsDataSet) {
                //Toast.makeText(AppVariables.context, "post get shops ", Toast.LENGTH_LONG).show();
                ShopsDataSet shopsDataSet = (ShopsDataSet)dataSet;
                SyncLinkedHashMap<Integer, Shop> shops = shopsDataSet.getShops();
                populateView(shops);
            }else if (dataSet instanceof ShopDetailsDataSet){
                Shop shop = ((ShopDetailsDataSet)dataSet).getShop();
                populateView(shop);
            }else if (dataSet instanceof ShopItemsDataSet){
                SyncLinkedHashMap<Integer, ShopItem> shopItems = ((ShopItemsDataSet) dataSet).getShopItems();
                populateView(shopItems);
            }
        }
    }

    private void populateView(Object data){
        if (activity == null){
            return;
        }
        if (populateView) {
            //Toast.makeText(AppVariables.context, "populate view ", Toast.LENGTH_LONG).show();
            activity.populateMainView(data);
        }else{
            activity.postAction(EActionType.POST_REQUEST,data);
            populateView=true;
        }
    }

    public void setActivity(IActivity activity) {
        this.activity = activity;
    }

    @Override
    public void postCreateData(Object o) {

    }

    @Override
    public void postUpdateData(Object o) {

    }

    @Override
    public void postDeleteData(boolean success) {
        if (activity != null){
            activity.postAction(EActionType.POST_DELETE, success);
        }
    }
}
