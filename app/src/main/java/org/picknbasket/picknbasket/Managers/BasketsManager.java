package org.picknbasket.picknbasket.Managers;

import org.picknbasket.picknbasket.Abstracts.DataSet;
import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Abstracts.IActivity;
import org.picknbasket.picknbasket.Abstracts.IDataAdapterCall;
import org.picknbasket.picknbasket.Abstracts.IGetDataSet;
import org.picknbasket.picknbasket.Data.BasketsDataAdapter;
import org.picknbasket.picknbasket.Data.BasketsDataSet;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Objects.Basket;

import java.io.ByteArrayInputStream;

/**
 * Created by Takunda on 2/18/2020.
 */

public class BasketsManager implements IGetDataSet, IDataAdapterCall<Basket> {

    private IActivity activity;

    public void getBaskets(IActivity activity){
        this.activity=activity;
        BasketsDataSet dataSet = new BasketsDataSet(this);
        dataSet.getDataSet();
    }

    public void getBasket(IActivity activity, int basketId){
        this.activity=activity;
        BasketsDataSet dataSet = new BasketsDataSet(this);
        dataSet.setBasketId(basketId);
        dataSet.getDataSet();
    }

    public void createBasket(IActivity activity, Basket basket){
        this.activity=activity;
        BasketsDataAdapter adapter = new BasketsDataAdapter(this);
        adapter.createData(basket);
    }

    public void saveBasket(IActivity activity, Basket basket){
        this.activity=activity;
        BasketsDataAdapter adapter = new BasketsDataAdapter(this);
        adapter.updateData(basket);
    }

    public void deleteBasket(IActivity activity, Basket basket){
        this.activity=activity;
        BasketsDataAdapter adapter = new BasketsDataAdapter(this);
        adapter.deleteData(basket);
    }

    public void resetBasket(IActivity activity, Basket basket){
        this.activity=activity;
        BasketsDataAdapter adapter = new BasketsDataAdapter(this);
        adapter.resetBasket(basket);
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        if (dataSet.success == true) {
            if (dataSet instanceof BasketsDataSet) {
                BasketsDataSet basketsDataSet = (BasketsDataSet)dataSet;
                SyncLinkedHashMap<Integer, Basket> baskets = basketsDataSet.getBaskets();
                activity.populateMainView(baskets);
            }
        }
    }

    @Override
    public void postCreateData(Basket basket) {
        if (basket != null){
            activity.postAction(EActionType.POST_ADD, basket);
        }
    }

    @Override
    public void postUpdateData(Basket basket) {
        if (activity != null){
            activity.postAction(EActionType.POST_UPDATE, basket);
        }
    }

    @Override
    public void postDeleteData(boolean success) {
        if (activity != null){
            activity.postAction(EActionType.POST_DELETE, null);
        }
    }
}
