package org.picknbasket.picknbasket.Data;

import com.google.android.gms.maps.model.LatLng;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.picknbasket.picknbasket.Abstracts.DataSet;
import org.picknbasket.picknbasket.Abstracts.EHttpMethod;
import org.picknbasket.picknbasket.Abstracts.IGetDataSet;
import org.picknbasket.picknbasket.Abstracts.IGetServerData;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.ObjectCache;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Objects.Shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Takunda on 1/12/2020.
 */

public class ShopsDataSet extends DataSet{
    private String url;
    private SyncLinkedHashMap<Integer,Shop> shops = new SyncLinkedHashMap<>();
    private String nextPageToken;
    private LatLng latlng;
    private boolean fetchedFromDB;

    public ShopsDataSet(IGetDataSet getDataset) {
        super(getDataset);
    }

    public void getDataSet(LatLng latLng){
        //check if shops are already available for this location in DB
        SyncLinkedHashMap<Integer,Shop> dbShops = getDBShops(latLng);
        if (dbShops != null && !dbShops.isEmpty()){
            System.out.println("db shops "+dbShops.size());
            fetchedFromDB=true;
            shops = dbShops;
            success=true;
            postGetDataSet(this);
            return;
        }

        this.latlng=latLng;

        if (latLng == null && AppVariables.getLocation() != null){
            this.latlng = new LatLng(AppVariables.getLocation().getLatitude(),AppVariables.getLocation().getLongitude());
        }

        if (this.latlng == null){
            success=false;
            postGetDataSet(this);
            return;
        }

        String groceryStoreUrl = getUrl(this.latlng,"grocery_or_supermarket&");
        String clothingStoreUrl = getUrl(this.latlng, "clothing_store");
        getData(groceryStoreUrl);
        getData(clothingStoreUrl);
    }

    public void getData(String url){
        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setHttpMethod(EHttpMethod.GET);
        getServerData.execute();
    }

    /**
     * If there are no shops for that latlng we try get shops from Google
     * If they are shops for the location, we get all shops and they will be sorted on UI
     *      according to those nearest the user.
     * @param latLng
     * @return
     */
    public SyncLinkedHashMap<Integer,Shop> getDBShops(LatLng latLng){
        SyncLinkedHashMap<Integer,Shop> shops = new SyncLinkedHashMap<>();

        if (latLng == null){
            HashMap<Integer,Shop> cachedShops = ObjectCache.getShops();
            if (cachedShops != null && !cachedShops.isEmpty()){
                //System.out.println("got cached shops ");
                shops.putAll(cachedShops);
                return shops;
            }
            shops = SQLite.getShops(null);
            return shops;
        }

        ArrayList<Integer> shopIds = SQLite.getShopIds(latLng.latitude,latLng.longitude);

        if (shopIds.isEmpty()){
            return null;
        }

        shops = SQLite.getShops(null);

        if (shops.isEmpty()){
            return null;
        }

        return shops;
    }

    //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.951316,18.474093&type=grocery_or_supermarket&radius=2000&key=AIzaSyA9_61L9dnFJyA2akkDLcP14hlEtdeh-D0
    private String getUrl(LatLng latLng, String type){
        if (url != null){
            return url;
        }

        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";

        if (nextPageToken != null){
            url = url +
                    "pagetoken=" + nextPageToken+"&";
        }else{
            url = url +
                    "location="+latLng.latitude+","+latLng.longitude+"&" +
                    "radius=2000&"+
                    "type="+type+"&";
        }
        url = url + "key=AIzaSyA9_61L9dnFJyA2akkDLcP14hlEtdeh-D0";

        return url;
    }

    public void setUrl(String url){
        this.url=url;
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {
        ArrayList<Shop> newShops = new ArrayList<>();

        if (json.has("next_page_token")){
            nextPageToken = json.getString("next_page_token");
        }

        CustomJSONArray results = json.getJSONArray("results");

        for (int i = 0; i < results.length(); i++) {
            CustomJSONObject place = results.getJSONObject(i);
            String lat = place.getJSONObject("geometry").getJSONObject("location").getString("lat");
            String lng = place.getJSONObject("geometry").getJSONObject("location").getString("lng");

            String googleId = place.getString("id");
            String placeId = place.getString("place_id");
            String name = place.getString("name");
            int priceLevel = place.getInt("price_level");
            double rating = place.getDouble("rating");
            int userRatingsTotal = place.getInt("user_ratings_total");
            String vicinity = place.getString("vicinity");

            Shop shop = new Shop(name);
            shop.setGoogleId(googleId);
            shop.setPlaceId(placeId);
            shop.setLatLng(lat,lng);
            shop.setPriceLevel(priceLevel);
            shop.setUserRatingsTotal(userRatingsTotal);
            shop.setRating(rating);
            shop.setVicinity(vicinity);

            newShops.add(shop);
        }

        SQLite.storeShops(newShops, latlng);
        //Save shops into DB
        this.shops = SQLite.getShops(null);
        if (nextPageToken != null){
            getDataSet(latlng);
        }
    }

    public SyncLinkedHashMap<Integer, Shop> getShops() {
        return shops;
    }

    public boolean isFetchedFromDB() {
        return fetchedFromDB;
    }
}
