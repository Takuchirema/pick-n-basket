package org.picknbasket.picknbasket.Data;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.picknbasket.picknbasket.Abstracts.IGetServerPicture;
import org.picknbasket.picknbasket.Helpers.AppVariables;

import java.io.InputStream;

/**
 * Created by Takunda on 24/05/2018.
 */

public class GetServerPicture extends AsyncTask<String, String, Integer> {

    int success= 0;
    private String pictureUrl;
    private int timeout = -1;
    private IGetServerPicture getServerPicture;
    private InputStream inputStream;
    private Bitmap bmp;

    public GetServerPicture(IGetServerPicture getServerPicture){
        this.getServerPicture=getServerPicture;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Integer doInBackground(String... args) {
        System.out.println("url - '"+pictureUrl+"'");
        try {
            bmp = Picasso.get()
                    .load(pictureUrl)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .get();
            if (bmp != null){
                System.out.println("picture retrieved cache");
                return 1;
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }

        try {
            bmp = Picasso.get()
                    .load(pictureUrl)
                    .get();
            if (bmp != null) {
                System.out.println("picture retrieved non cache");
                return 1;
            }
        }catch (Exception e){
            //e.printStackTrace();
        }

        return 0;
    }

    /**
     * After completing background task Dismiss the progress dialog
     * **/
    protected void onPostExecute(Integer success) {
        if (success == 1){
            getServerPicture.postGetServerPicture(bmp);
        }else{
            getServerPicture.postGetServerPicture(null);
        }
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}