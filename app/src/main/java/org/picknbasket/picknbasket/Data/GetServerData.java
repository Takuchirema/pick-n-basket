package org.picknbasket.picknbasket.Data;

import android.os.AsyncTask;

import org.picknbasket.picknbasket.Abstracts.EHttpMethod;
import org.picknbasket.picknbasket.Abstracts.IGetServerData;
import org.picknbasket.picknbasket.Helpers.AppVariables;

import org.apache.http.NameValuePair;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Takunda on 24/05/2018.
 */

public class GetServerData extends AsyncTask<String, String, CustomJSONObject> {

    private List<NameValuePair> parameters = new ArrayList<>();
    JSONParser jsonParser = new JSONParser();
    private String url;
    private EHttpMethod httpMethod=EHttpMethod.GET;
    private int timeout = -1;
    private IGetServerData getServerData;

    public GetServerData(IGetServerData getServerData){
        //System.out.println("get server data thread: "+Thread.currentThread().getId());
        this.getServerData=getServerData;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected CustomJSONObject doInBackground(String... args) {
        try {
            //System.out.println("background server data thread: "+Thread.currentThread().getId());

            if (timeout > 0){
                jsonParser.setTimeoutConnection(timeout);
            }

            CustomJSONObject json;
            // get response data from url command.
            json = jsonParser.makeHttpRequest(url, httpMethod.toString(), parameters);

            return json;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * After completing background task Dismiss the progress dialog
     * **/
    protected void onPostExecute(final CustomJSONObject data) {
        // Avoid running on main thread. Preparation of data will be heavy sometimes.
        new Thread(new Runnable() {
            public void run() {
                try {
                    getServerData.postGetServerData(data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public boolean deprecatedVersion(){

        if (AppVariables.versionCode == 0 || AppVariables.supportedVersions.isEmpty()) {
            return false;
        }

        int maxVersion = Collections.max(AppVariables.supportedVersions);
        //Exit application if the version code is not supported
        if (AppVariables.versionCode < maxVersion && !AppVariables.supportedVersions.contains(AppVariables.versionCode)){
            if(AppVariables.mainActivity != null){
                AppVariables.mainActivity.deprecatedVersion();
                return true;
            }
        }

        return false;
    }

    public List<NameValuePair> getParameters() {
        return parameters;
    }

    public void setParameters(List<NameValuePair> parameters) {
        this.parameters = parameters;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public EHttpMethod getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(EHttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }
}
