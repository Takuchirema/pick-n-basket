package org.picknbasket.picknbasket.Data;

import org.picknbasket.picknbasket.Abstracts.DataSet;
import org.picknbasket.picknbasket.Abstracts.IGetDataSet;
import org.picknbasket.picknbasket.Helpers.ObjectCache;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Objects.Basket;

/**
 * Created by Takunda on 2/18/2020.
 */

public class BasketsDataSet extends DataSet{

    private SyncLinkedHashMap<Integer,Basket> baskets;
    private Basket basket;
    private int basketId;

    public BasketsDataSet(IGetDataSet getDataset) {
        super(getDataset);
    }

    public void getDataSet(){
        baskets = getDBBaskets();
        if (baskets != null){
            System.out.println("db baskets "+baskets.size());
            success=true;
            postGetDataSet(this);

        }else{
            message="No baskets found";
            success=false;
            postGetDataSet(this);
        }
        return;
    }

    private SyncLinkedHashMap<Integer,Basket> getDBBaskets(){
        SyncLinkedHashMap<Integer,Basket> baskets = new SyncLinkedHashMap<>();

        if (ObjectCache.getBasket(basketId) != null){
            basket = ObjectCache.getBasket(basketId);
            baskets.put(basketId, basket);
            return baskets;
        }

        if (basketId != 0){
            basket = SQLite.getBasket(basketId);
            baskets.put(basketId, basket);
            return baskets;
        }

        baskets = SQLite.getBaskets();
        return baskets;
    }

    public SyncLinkedHashMap<Integer, Basket> getBaskets() {
        return baskets;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }
}
