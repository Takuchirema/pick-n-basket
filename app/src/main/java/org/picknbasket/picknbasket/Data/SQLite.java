package org.picknbasket.picknbasket.Data;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.DatabaseHelper;
import org.picknbasket.picknbasket.Helpers.ObjectCache;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Objects.Basket;
import org.picknbasket.picknbasket.Objects.Shop;
import org.picknbasket.picknbasket.Objects.ShopItem;
import org.picknbasket.picknbasket.Objects.ShopWebPage;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Takunda Chirema  on 2017-10-28.
 */

public class SQLite {

    public static SQLiteDatabase basketDB;

    public static SQLiteDatabase openOrCreateDB(){
        basketDB = DatabaseHelper.getInstance(AppVariables.context).getReadableDatabase();
        return basketDB;
    }

    //region data tables
    public static void createShopsTable(){
        openOrCreateDB().execSQL("CREATE TABLE IF NOT EXISTS Shops(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "googleId VARCHAR," +
                "placeId VARCHAR," +
                "name VARCHAR," +
                "website VARCHAR," +
                "phoneNumber VARCHAR," +
                "pictureUrl VARCHAR," +
                "latitude VARCHAR," +
                "longitude VARCHAR," +
                "latlng2 VARCHAR," +
                "vicinity VARCHAR," +
                "priceLevel INT," +
                "userRatingsTotal INT," +
                "rating REAL," +
                "detailsFetched INT," +
                "businessHours VARCHAR"+
                ");");
    }

    public static void createShopItemsTable(){
        openOrCreateDB().execSQL("CREATE TABLE IF NOT EXISTS ShopItems(" +
                "id INTEGER PRIMARY KEY," +
                "shopId INT," +
                "name VARCHAR," +
                "price REAL," +
                "currency VARCHAR," +
                "displayPrice VARCHAR," +
                "details VARCHAR," +
                "onSpecial INT," +
                "quantity INT," +
                "measurement VARCHAR," +
                "pictureUrl VARCHAR," +
                "webUrl VARCHAR," +
                "webId INT," +
                "lastUpdate INTEGER" +
                ");");
    }

    public static void createShopLocationsTable(){
        openOrCreateDB().execSQL("CREATE TABLE IF NOT EXISTS ShopLocations(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "latlng2 VARCHAR," +
                "shopId INT" +
                ");");
    }

    public static void createShopWebPagesTable(){
        openOrCreateDB().execSQL("CREATE TABLE IF NOT EXISTS ShopWebPages(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "url VARCHAR," +
                "shopId INT," +
                "lastScrapped INTEGER" +
                ");");
    }

    public static void createBasketsTable(){
        openOrCreateDB().execSQL("CREATE TABLE IF NOT EXISTS Baskets(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name VARCHAR," +
                "created INT" +
                ");");
    }

    public static void createBasketItemsTable(){
        openOrCreateDB().execSQL("CREATE TABLE IF NOT EXISTS BasketItems(" +
                "itemId INT," +
                "basketId INT," +
                "count INT," +
                "picked INT,"+
                "PRIMARY KEY (itemId, basketId)"+
                ");");
    }
    //endregion

    //region data storage
    public static SyncLinkedHashMap<Integer,Shop> storeShops(ArrayList<Shop> shops, LatLng latlng){
        SyncLinkedHashMap<Integer,Shop> newShops = new SyncLinkedHashMap<>();

        String latlng2 = getLatLng2(latlng.latitude,latlng.longitude);

        createShopsTable();

        for (Shop shop:shops){
            Shop newShop = storeShop(shop,latlng2);
            //System.out.println("shops sql id: "+newShop.getId()+" name: "+newShop.getName());
            newShops.put(newShop.getId(),newShop);
        }

        return newShops;
    }

    public static SyncLinkedHashMap<Integer,ShopItem> storeShopItems(SyncLinkedHashMap<Integer,ShopItem> shopItems){
        SyncLinkedHashMap<Integer,ShopItem> newShopItems = new SyncLinkedHashMap<>();

        createShopItemsTable();

        for (ShopItem shopItem:shopItems){
            ShopItem newShopItem = storeShopItem(shopItem);
            //System.out.println("shops sql id: "+newShop.getId()+" name: "+newShop.getName());
            newShopItems.put(newShopItem.getId(),newShopItem);
        }

        return newShopItems;
    }

    public static Shop storeShop(final Shop shop, String latlng2){
        String idField=null;
        String idValue=null;
        createShopsTable();

        if (recordExists("Shops",new HashMap<String, String>(){{ put("googleId",shop.getGoogleId());}}) ) {
            idField = "googleId";
            idValue=shop.getGoogleId();
        }else if (recordExists("Shops",new HashMap<String, String>(){{ put("id",Integer.toString(shop.getId()));}}) ) {
            idField = "id";
            idValue=shop.getId()+"";
        }

        if (idField != null) {
            System.out.println("update shop "+shop.getName()+" "+shop.getPictureUrl());
            basketDB.update("Shops", shop.getContentValues(), idField+" = '" + idValue + "'", null);

            Cursor resultSet = basketDB.rawQuery("Select * from Shops where "+idField+"='" + idValue + "'", null);
            resultSet.moveToFirst();

            Shop newShop = getShop(resultSet);
            resultSet.close();

            ObjectCache.updateShop(newShop, true);
            return newShop;
        }

        //System.out.println("store shop: dr "+shop.getDebitAccountId()+" cr "+shop.getCreditAccountId());
        String insertStatement = "INSERT INTO Shops " +
                "(googleId,placeId,name,website,pictureUrl,latitude,longitude,latlng2,priceLevel,rating,userRatingsTotal,vicinity) "+
                " VALUES(" +
                "'"+shop.getGoogleId()+"'," +
                "'"+shop.getPlaceId()+"'," +
                "'"+shop.getName()+"'," +
                "'"+shop.getWebsite()+"',"+
                "'"+shop.getPictureUrl()+"'," +
                "'"+shop.getLatitude()+"'," +
                "'"+shop.getLongitude()+"'," +
                "'"+shop.getLatLng2()+"'," +
                "'"+shop.getPriceLevel()+"'," +
                "'"+shop.getRating()+"',"+
                "'"+shop.getUserRatingsTotal()+"'," +
                "'"+shop.getVicinity()+"'";

        basketDB.execSQL(insertStatement+ ");");

        Cursor resultSet = basketDB.rawQuery("Select * from Shops",null);
        resultSet.moveToLast();

        Shop newShop =  getShop(resultSet);
        resultSet.close();

        if (latlng2 != null) {
            //Store the shop location
            storeShopLocations(latlng2, newShop.getId());
        }

        return newShop;
    }

    public static void storeShopLocations(final String latlng2, final int shopId){
        createShopLocationsTable();

        System.out.println("Store loc 1 "+latlng2+" "+shopId);
        if (recordExists("ShopLocations",new HashMap<String, String>(){{ put("latlng2",latlng2);put("shopId",Integer.toString(shopId));}})) {
            return;
        }

        System.out.println("Store loc 2 "+latlng2+" "+shopId);
        //System.out.println("store shop: dr "+shop.getDebitAccountId()+" cr "+shop.getCreditAccountId());
        String insertStatement = "INSERT INTO ShopLocations " +
                "(latlng2,shopId) "+
                " VALUES(" +
                "'"+latlng2+"'," +
                "'"+shopId+"'";

        basketDB.execSQL(insertStatement+ ");");
    }

    public static ShopWebPage storeShopWebPages(final String url, final int shopId){
        createShopWebPagesTable();

        System.out.println("store scraping 1, shop id: "+shopId+" url: "+url);
        if (recordExists("ShopWebPages",new HashMap<String, String>(){{ put("url",url);put("shopId",Integer.toString(shopId));}})) {
            deleteWebPage(url,shopId);
        }

        Date date = new Date();
        long time = date.getTime();

        System.out.println("store scraping 2, shop id: "+shopId+" url: "+url);
        String insertStatement = "INSERT INTO ShopWebPages " +
                "(url,shopId,lastScrapped) "+
                " VALUES(" +
                "'"+url+"'," +
                "'"+shopId+"', "+
                ""+time+"";

        basketDB.execSQL(insertStatement+ ");");

        Cursor resultSet = basketDB.rawQuery("Select * from ShopWebPages",null);
        resultSet.moveToLast();

        ShopWebPage webPage =  getWebPage(resultSet);
        resultSet.close();

        return webPage;
    }

    public static ShopItem storeShopItem(final ShopItem shopItem){
        createShopItemsTable();

        if (recordExists("ShopItems",new HashMap<String, String>(){{ put("id",Integer.toString(shopItem.getId()));}}) ) {

            basketDB.update("ShopItems", shopItem.getContentValues()," id = '" + shopItem.getId() + "'", null);

            Cursor resultSet = basketDB.rawQuery("Select * from ShopItems where id ='" + shopItem.getId() + "'", null);
            resultSet.moveToFirst();

            ShopItem newShopItem = getShopItem(resultSet);
            resultSet.close();

            return newShopItem;
        }

        //System.out.println("store shopItem: dr "+shopItem.getDebitAccountId()+" cr "+shopItem.getCreditAccountId());
        String insertStatement = "INSERT INTO ShopItems " +
                "(id,shopId,name,price,currency,displayPrice,details,pictureUrl,webUrl,webId,onSpecial,lastUpdate) "+
                " VALUES(" +
                "'"+shopItem.getId()+"'," +
                "'"+shopItem.getShopId()+"'," +
                "'"+shopItem.getName()+"'," +
                "'"+shopItem.getPrice()+"'," +
                "'"+shopItem.getCurrency()+"'," +
                "'"+shopItem.getDisplayPrice()+"',"+
                "'"+shopItem.getDetails()+"',"+
                "'"+shopItem.getPictureUrl()+"'," +
                "'"+shopItem.getWebUrl()+"'," +
                "'"+shopItem.getWebId()+"'," +
                "'"+shopItem.getOnSpecial()+"'," +
                "'"+shopItem.getLastUpdate()+"'";

        basketDB.execSQL(insertStatement+ ");");

        Cursor resultSet = basketDB.rawQuery("Select * from ShopItems",null);
        resultSet.moveToLast();

        ShopItem newShopItem =  getShopItem(resultSet);
        resultSet.close();

        return newShopItem;
    }

    public static Basket storeBasket(final Basket basket){
        createBasketsTable();

        if (recordExists("Baskets",new HashMap<String, String>(){{ put("id",Integer.toString(basket.getId()));}}) ) {

            basketDB.update("Baskets", basket.getContentValues()," id = '" + basket.getId() + "'", null);

            Cursor resultSet = basketDB.rawQuery("Select * from Baskets where id ='" + basket.getId() + "'", null);
            resultSet.moveToFirst();

            Basket newBasket = getBasket(resultSet);
            resultSet.close();

            ObjectCache.updateBasket(newBasket);
            return newBasket;
        }

        //System.out.println("store basket: dr "+basket.getDebitAccountId()+" cr "+basket.getCreditAccountId());
        String insertStatement = "INSERT INTO Baskets " +
                "(name) "+
                " VALUES(" +
                "'"+basket.getName()+"'";

        basketDB.execSQL(insertStatement+ ");");

        Cursor resultSet = basketDB.rawQuery("Select * from Baskets",null);
        resultSet.moveToLast();

        Basket newBasket =  getBasket(resultSet);
        resultSet.close();

        return newBasket;
    }

    /**
     * This will first clear out all previous items of the basket and insert these ones.
     * It's easier than checking each one if it's there or not already etc.
     * @param basketId
     * @param basketItems
     */
    public static void storeBasketItems(int basketId, HashMap<Integer,ShopItem> basketItems){
        if (basketItems == null){
            return;
        }

        createBasketItemsTable();
        deleteBasketItems(basketId);

        if (basketItems.isEmpty()){
            return;
        }

        String insertValues = "";
        for (Map.Entry<Integer,ShopItem> entry:basketItems.entrySet()){
            ShopItem shopItem = entry.getValue();
            insertValues = insertValues+",('"+basketId+"','"+shopItem.getId()+"',"+shopItem.getPicked()+","+shopItem.getCount()+")";
        }
        insertValues = insertValues.substring(1);

        System.out.println("insert shop item values: "+insertValues);
        String insertStatement = "INSERT INTO BasketItems " +
                "(basketId,itemId,picked,count)"+
                " VALUES "+insertValues;

        basketDB.execSQL(insertStatement+ ";");
    }
    //endregion

    //region get data
    public static SyncLinkedHashMap<Integer,Shop> getShops(ArrayList<Integer> shopIds){
        String queryString = "Select * from Shops";

        if (shopIds != null && !shopIds.isEmpty()){
            String strList = shopIds.toString();
            strList = strList.substring(1, strList.length() - 1);
            queryString = queryString + " where id in ("+strList+")";
        }

        createShopsTable();

        SyncLinkedHashMap<Integer,Shop> shops = new SyncLinkedHashMap<>();
        Cursor resultSet = basketDB.rawQuery(queryString,null);

        while (resultSet.moveToNext()) {
            Shop shop = getShop(resultSet);
            shops.put(shop.getId(),shop);
        }
        resultSet.close();

        ObjectCache.refreshShops(shops);
        return ObjectCache.getShops();
    }

    public static Shop getShop(int shopId){
        createShopsTable();

        Shop shop=null;
        String queryString = "Select * from Shops where id='"+shopId+"'";
        Cursor resultSet = basketDB.rawQuery(queryString,null);

        while (resultSet.moveToNext()) {
            shop = getShop(resultSet);
        }

        resultSet.close();
        return shop;
    }

    public static ArrayList<Integer> getShopIds(double latitude, double longitude){
        ArrayList<Integer> shopIds = new ArrayList<>();

        String latlng2 = getLatLng2(latitude,longitude);

        createShopLocationsTable();
        Cursor resultSet = basketDB.rawQuery(queryNearbyLatLng2(latitude,longitude),null);

        while (resultSet.moveToNext()) {
            //System.out.println("sql-lite "+resultSet.getString(resultSet.getColumnIndex("latlng2")));
            int shopId = resultSet.getInt(resultSet.getColumnIndex("shopId"));
            shopIds.add(shopId);
        }
        resultSet.close();

        System.out.println("sql-lite shop id's "+shopIds.size()+" loc "+latlng2);
        return shopIds;
    }

    private static String queryNearbyLatLng2(double latitude, double longitude){
        String LatLng2 = getLatLng2(latitude,longitude);
        String query = "Select * from ShopLocations where latlng2 = '"+LatLng2+"'";

        String lat2 = getLatLng2(latitude);
        String lng2 = getLatLng2(longitude);

        String lat2P1 = getLatLng2(Double.parseDouble(getLatLng2(latitude))+0.01);
        String lng2P1 = getLatLng2(Double.parseDouble(getLatLng2(longitude))+0.01);

        String lat2M1 = getLatLng2(Double.parseDouble(getLatLng2(latitude))-0.01);
        String lng2M1 = getLatLng2(Double.parseDouble(getLatLng2(longitude))-0.01);

        query = query + " or latlng2 = '"+lat2+","+lng2P1+"'";
        query = query + " or latlng2 = '"+lat2+","+lng2M1+"'";

        query = query + " or latlng2 = '"+lat2P1+","+lng2+"'";
        query = query + " or latlng2 = '"+lat2P1+","+lng2P1+"'";
        query = query + " or latlng2 = '"+lat2P1+","+lng2M1+"'";

        query = query + " or latlng2 = '"+lat2M1+","+lng2+"'";
        query = query + " or latlng2 = '"+lat2M1+","+lng2P1+"'";
        query = query + " or latlng2 = '"+lat2M1+","+lng2M1+"'";

        System.out.println("shop id's query "+query);
        return query;
    }

    public static ShopWebPage getWebPage(Cursor resultSet){
        int id = resultSet.getInt(resultSet.getColumnIndex("id"));
        long lastScrapped = resultSet.getLong(resultSet.getColumnIndex("lastScrapped"));
        int shopId = resultSet.getInt(resultSet.getColumnIndex("shopId"));
        String url = resultSet.getString(resultSet.getColumnIndex("url"));

        ShopWebPage webPage = new ShopWebPage(url);
        webPage.setId(id);
        webPage.setLastScrapped(lastScrapped);
        webPage.setShopId(shopId);

        return webPage;
    }

    public static ShopWebPage getWebPage(int shopId, String url){
        createShopWebPagesTable();

        ShopWebPage webPage = null;
        Cursor resultSet = basketDB.rawQuery("Select * from ShopWebPages where shopId = '"+shopId+"' and url='"+url+"'",null);
        while (resultSet.moveToNext()) {
            webPage = getWebPage(resultSet);
        }

        return webPage;
    }

    public static ArrayList<String> getShopWebPages(int shopId){
        ArrayList<String> webPages = new ArrayList<>();

        Cursor resultSet = basketDB.rawQuery("Select * from ShopWebPages where shopId = '"+shopId+"'",null);

        while (resultSet.moveToNext()) {
            //System.out.println("sql-lite "+resultSet.getString(resultSet.getColumnIndex("latlng2")));
            String url = resultSet.getString(resultSet.getColumnIndex("url"));
            webPages.add(url);
        }

        return webPages;
    }

    public static boolean hasScraped(final int shopId, final String url){
        createShopWebPagesTable();

        if (recordExists("ShopWebPages",new HashMap<String, String>(){{ put("url",url);put("shopId",Integer.toString(shopId));}})) {
            ShopWebPage webPage = getWebPage(shopId,url);
            long time= System.currentTimeMillis();
            long diff = time - webPage.getLastScrapped();

            long daysDiff = diff / (1000 * 60 * 60 * 24);

            System.out.println("scrapped days "+daysDiff+" "+url);
            if (daysDiff > AppVariables.pageRefreshInterval){
                deleteWebPage(url,shopId);
                return false;
            }
            return true;
        }
        return false;
    }

    public static SyncLinkedHashMap<Integer,ShopItem> getShopItems(ArrayList<Integer> shopIds){
        createShopItemsTable();

        String queryString = "Select * from ShopItems";

        if (shopIds != null && !shopIds.isEmpty()){
            String strList = "";
            for(int shopId:shopIds){
                strList = strList + "'"+shopId+"',";
            }
            strList = strList.substring(0,strList.length() - 1);
            queryString = queryString + " where shopId in ("+strList+")";
        }

        SyncLinkedHashMap<Integer,ShopItem> shopItems = new SyncLinkedHashMap<>();
        Cursor resultSet = basketDB.rawQuery(queryString,null);

        while (resultSet.moveToNext()) {
            ShopItem shopItem = getShopItem(resultSet);
            shopItems.put(shopItem.getId(),shopItem);
        }
        resultSet.close();

        ObjectCache.refreshShopItems(shopItems);
        return shopItems;
    }

    public static ShopItem getShopItem(int shopItemId){
        createShopItemsTable();

        String queryString = "Select * from ShopItems where id = '"+shopItemId+"'";

        ShopItem shopItem = null;
        Cursor resultSet = basketDB.rawQuery(queryString,null);

        while (resultSet.moveToNext()) {
            shopItem = getShopItem(resultSet);
        }
        resultSet.close();

        return shopItem;
    }

    public static ShopItem getBasketItem(Cursor resultSet){
        ShopItem basketItem = getShopItem(resultSet);

        int basketId = resultSet.getInt(resultSet.getColumnIndex("basketId"));
        int count = resultSet.getInt(resultSet.getColumnIndex("count"));
        int picked = resultSet.getInt(resultSet.getColumnIndex("picked"));

        basketItem.setCount(count);
        basketItem.setPicked(picked);
        basketItem.setBasketId(basketId);

        return basketItem;
    }
    public static ShopItem getShopItem(Cursor resultSet){
        //System.out.println("column index: "+resultSet.getColumnIndex("Date")+" count: "+resultSet.getCount());
        int id = resultSet.getInt(resultSet.getColumnIndex("id"));
        String name = resultSet.getString(resultSet.getColumnIndex("name"));
        String pictureUrl = resultSet.getString(resultSet.getColumnIndex("pictureUrl"));
        int shopId = resultSet.getInt(resultSet.getColumnIndex("shopId"));
        String webUrl = resultSet.getString(resultSet.getColumnIndex("webUrl"));
        double price = resultSet.getDouble(resultSet.getColumnIndex("price"));
        String displayPrice = resultSet.getString(resultSet.getColumnIndex("displayPrice"));
        Long lastUpdate = resultSet.getLong(resultSet.getColumnIndex("lastUpdate"));
        int quantity = resultSet.getInt(resultSet.getColumnIndex("quantity"));
        int onSpecial = resultSet.getInt(resultSet.getColumnIndex("onSpecial"));
        String measurement = resultSet.getString(resultSet.getColumnIndex("measurement"));
        String currency = resultSet.getString(resultSet.getColumnIndex("currency"));
        String details = resultSet.getString(resultSet.getColumnIndex("details"));

        System.out.println("on special price "+onSpecial+ " "+name);
        ShopItem shopItem = new ShopItem(name,id);
        shopItem.setPictureUrl(pictureUrl);
        shopItem.setWebUrl(webUrl);
        shopItem.setPrice(price);
        shopItem.setOnSpecial(onSpecial);
        shopItem.setDisplayPrice(displayPrice);
        shopItem.setLastUpdate(lastUpdate);
        shopItem.setShopId(shopId);
        shopItem.setQuantity(quantity);
        shopItem.setMeasurement(measurement);
        shopItem.setCurrency(currency);
        shopItem.setDetails(details);

        return shopItem;
    }

    public static Shop getShop(Cursor resultSet){
        ////System.out.println("column index: "+resultSet.getColumnIndex("Date")+" count: "+resultSet.getCount());
        int id = resultSet.getInt(resultSet.getColumnIndex("id"));
        String googleId = resultSet.getString(resultSet.getColumnIndex("googleId"));
        String placeId = resultSet.getString(resultSet.getColumnIndex("placeId"));
        String name = resultSet.getString(resultSet.getColumnIndex("name"));
        String website = resultSet.getString(resultSet.getColumnIndex("website"));
        String phoneNumber = resultSet.getString(resultSet.getColumnIndex("phoneNumber"));
        String pictureUrl = resultSet.getString(resultSet.getColumnIndex("pictureUrl"));
        String latitude = resultSet.getString(resultSet.getColumnIndex("latitude"));
        String longitude = resultSet.getString(resultSet.getColumnIndex("longitude"));
        String vicinity = resultSet.getString(resultSet.getColumnIndex("vicinity"));
        int priceLevel = resultSet.getInt(resultSet.getColumnIndex("priceLevel"));
        int userRatingsTotal = resultSet.getInt(resultSet.getColumnIndex("userRatingsTotal"));
        float rating = resultSet.getFloat(resultSet.getColumnIndex("rating"));
        int detailsFetched = resultSet.getInt(resultSet.getColumnIndex("detailsFetched"));
        String businessHours = resultSet.getString(resultSet.getColumnIndex("businessHours"));

        Shop shop = new Shop(name);
        shop.setId(id);
        shop.setGoogleId(googleId);
        shop.setPlaceId(placeId);
        shop.setWebsite(website);
        shop.setPhoneNumber(phoneNumber);
        shop.setPictureUrl(pictureUrl);
        shop.setLatLng(latitude,longitude);
        shop.setVicinity(vicinity);
        shop.setPriceLevel(priceLevel);
        shop.setUserRatingsTotal(userRatingsTotal);
        shop.setRating(rating);
        shop.setDetailsFetched(detailsFetched);
        shop.setBusinessHours(businessHours);
        shop.setHasDetails();

        System.out.println("shop picture url: "+name+" - "+pictureUrl+" "+shop.hasWebsite()+" "+shop.getWebsite());
        return shop;
    }

    
    public static Basket getBasket(int basketId){
        createBasketsTable();

        String queryString = "Select * from Baskets where id = '"+basketId+"'";

        Basket basket = null;
        Cursor resultSet = basketDB.rawQuery(queryString,null);

        while (resultSet.moveToNext()) {
            basket = getBasket(resultSet);
        }
        resultSet.close();

        return basket;
    }
    
    public static Basket getBasket(Cursor resultSet){
        //System.out.println("column index: "+resultSet.getColumnIndex("Date")+" count: "+resultSet.getCount());
        int id = resultSet.getInt(resultSet.getColumnIndex("id"));
        String name = resultSet.getString(resultSet.getColumnIndex("name"));

        //Log.i("get shop items","name: "+name+" price: "+displayPrice);
        Basket basket = new Basket(name);
        basket.setId(id);

        //get the basket items
        basket.setBasketItems(getBasketItems(id));
        basket.calculate();
        return basket;
    }

    public static SyncLinkedHashMap<Integer,Basket> getBaskets(){
        createBasketsTable();

        String queryString = "Select * from Baskets";

        SyncLinkedHashMap<Integer,Basket> baskets = new SyncLinkedHashMap<>();
        Cursor resultSet = basketDB.rawQuery(queryString,null);

        while (resultSet.moveToNext()) {
            Basket basket = getBasket(resultSet);
            baskets.put(basket.getId(),basket);
        }
        resultSet.close();

        ObjectCache.refreshBaskets(baskets);
        return baskets;
    }

    public static SyncLinkedHashMap<Integer, ShopItem> getBasketItems(int basketId){
        createBasketItemsTable();
        createShopItemsTable();

        String queryString = "Select * from ShopItems "+
                "inner join BasketItems on BasketItems.itemId = ShopItems.id "+
                "where BasketItems.basketId = '"+basketId+"'";

        SyncLinkedHashMap<Integer,ShopItem> shopItems = new SyncLinkedHashMap<>();
        Cursor resultSet = basketDB.rawQuery(queryString,null);

        //System.out.println(" get basket items "+resultSet.)
        while (resultSet.moveToNext()) {
            ShopItem shopItem = getBasketItem(resultSet);
            shopItems.put(shopItem.getId(),shopItem);
        }
        resultSet.close();

        return shopItems;
    }

    //endregion

    //region delete data
    public static boolean deleteShop(int shopId)
    {
        return basketDB.delete("Shops", "id = '"+shopId+"'", null) > 0;
    }

    public static boolean deleteBasket(Basket basket)
    {
        //delete basket items first
        basketDB.delete("BasketItems", "basketId = '"+basket.getId()+"'", null);
        return basketDB.delete("Baskets", "id = '"+basket.getId()+"'", null) > 0;
    }

    public static void deleteWebPage(String url, int shopId)
    {
        basketDB.delete("ShopWebPages", "shopId = '"+shopId+"' and url = '"+url+"'", null);
    }

    public static boolean deleteBasketItems(int basketId)
    {
        //delete basket items first
        return basketDB.delete("BasketItems", "basketId = '"+basketId+"'", null) > 0;
    }

    public static void deleteAllWebPages(){
        createShopWebPagesTable();
        basketDB.delete("ShopWebPages",null, null);
    }

    public static void deleteAllShopItems(){
        createShopItemsTable();
        basketDB.delete("ShopItems",null, null);
    }

    public static void deleteAllBasketItems(){
        createBasketItemsTable();
        basketDB.delete("BasketItems",null, null);
    }

    public static void deleteDB(){
        AppVariables.context.deleteDatabase(DatabaseHelper.DATABASE_NAME);
    }
    //endregion

    public static SyncLinkedHashMap<Integer,Shop> sortShops(SyncLinkedHashMap<Integer,Shop> shops){
        List<Shop> shopsList = new ArrayList<>(shops.values());
        Collections.sort(shopsList, new Comparator<Shop>() {
            public int compare(Shop a, Shop b){
                return a.compareTo(b);
            }
        });
        shops = new SyncLinkedHashMap<>();
        for (Shop account:shopsList){
            shops.put(account.getId(),account);
        }
        return shops;
    }

    public static boolean recordExists(String TableName, HashMap<String,String> fieldValues) {
        String queryString = "";
        Iterator iterator = fieldValues.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry)iterator.next();
            queryString = mapElement.getKey() + " = '"+mapElement.getValue()+"'";
            if (iterator.hasNext()) {
                queryString = queryString + " and ";
            }
        }

        String Query = "Select * from " + TableName + " where " + queryString;
        Cursor cursor = basketDB.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            return false;
        }

        cursor.close();
        return true;
    }

    public static String getLatLng2(double latitude, double longitude){
        String LatLng2=getLatLng2(latitude)+","+getLatLng2(longitude);
        return LatLng2;
    }

    /**
     * Gets the latlng2 representation of only one of the two
     * @param LatLng
     * @return
     */
    public static String getLatLng2(double LatLng){
        String LatLngStr = Double.toString(LatLng);
        int dotIndex = LatLngStr.indexOf(".");
        if (dotIndex < 0){
            return LatLngStr;
        }
        int decimals = LatLngStr.length() - (dotIndex + 1);
        String lat2 = LatLngStr.substring(0,LatLngStr.indexOf(".")+Math.min(3,decimals));
        return lat2;
    }
}
