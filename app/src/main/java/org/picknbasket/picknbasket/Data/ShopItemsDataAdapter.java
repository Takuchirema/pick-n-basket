package org.picknbasket.picknbasket.Data;

import org.picknbasket.picknbasket.Abstracts.DataAdapter;
import org.picknbasket.picknbasket.Abstracts.IDataAdapterCall;
import org.picknbasket.picknbasket.Objects.ShopItem;

public class ShopItemsDataAdapter extends DataAdapter<ShopItem> {

    public ShopItemsDataAdapter(IDataAdapterCall dataAdapterCall) {
        super(dataAdapterCall);
    }

    @Override
    public void createData(ShopItem shopItem) {

    }

    @Override
    public void updateData(ShopItem shopItem) {

    }

    @Override
    public void deleteData(ShopItem shopItem) {

    }

    public void resetShopItems(){
        //SQLite.deleteDB();
        SQLite.deleteAllWebPages();
        SQLite.deleteAllBasketItems();
        SQLite.deleteAllShopItems();
        postDeleteData(true);
    }

    @Override
    public void postCreateData(ShopItem shopItem) {

    }

    @Override
    public void postUpdateData(ShopItem shopItem) {

    }

    @Override
    public void postDeleteData(boolean success) {

    }
}
