package org.picknbasket.picknbasket.Data;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.RequiresApi;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.picknbasket.picknbasket.Abstracts.DataSet;
import org.picknbasket.picknbasket.Abstracts.IActivity;
import org.picknbasket.picknbasket.Abstracts.IGetDataSet;
import org.picknbasket.picknbasket.Abstracts.IGetWebData;
import org.picknbasket.picknbasket.Abstracts.IWebViewActivity;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Helpers.WebScraper;
import org.picknbasket.picknbasket.Objects.Shop;
import org.picknbasket.picknbasket.Objects.ShopItem;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Dictionary;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.picknbasket.picknbasket.Helpers.AppVariables.context;

/**
 * Created by Takunda on 1/20/2020.
 */

public class GetWebData extends AsyncTask<String, String, CustomJSONObject> implements IGetDataSet{

    private String logoUrl;
    private boolean getLogo;
    private String url;
    private String baseUrl;
    // These are urls to scrap. Will be useful only if depth has been set.
    private ArrayList<String> urls = new ArrayList<>();
    private ArrayList<String> nextLevelUrls = new ArrayList<>();
    // These are urls that have been scraped. Will be saved
    private ArrayList<String> scrapedUrls = new ArrayList<>();
    private ArrayList<Integer> scrapedShopIds = new ArrayList<>();
    private IGetWebData getWebData;
    private SyncLinkedHashMap<Integer,ShopItem> shopItems = new SyncLinkedHashMap<>();
    // This is the depth of webscraping we'll go
    private int depth=1;
    private boolean getDocumentData = false;
    private WebView webView;
    private IWebViewActivity activity;
    private int shopId;
    private Shop shop;
    private ArrayList<Shop> shops = new ArrayList<>();

    // HTML to be looked into might be coming from another activity
    private String html;
    public GetWebData(IGetWebData getWebData){
        this.getWebData=getWebData;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected CustomJSONObject doInBackground(String... args) {
        if (shops.isEmpty()) {
            scrapForData();
        }else{
            scrapForShopsData();
        }
        return null;
    }

    public void scrapForData(){
        if (depth == 0){
            scrapForShopsData();
            return;
        }

        if (html != null && url != null && shopId != 0){
            Document document = Jsoup.parse(html);
            analyseDocument(document,0,url);
            return;
        }

        int i=0;
        if (!urls.isEmpty()) {
            String url = urls.get(0);
            if (scrapedUrls.contains(url)){
                scrapNextUrl(0,0);
            }

            if (shop != null) {
                String url1 = url.replaceFirst("^(http[s]?://www\\.|http[s]?://|www\\.)", "");
                String url2 = shop.getWebsite().replaceFirst("^(http[s]?://www\\.|http[s]?://|www\\.)", "");
                if (!url1.startsWith(url2)) {
                    scrapNextUrl(0, 0);
                }
            }

            if (getLogo) {
                try {
                    JSOUPWebData(url, i);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                WebServiceData(url, i);
            }
        }else{
            scrapForShopsData();
        }
    }

    /**
     * The method will get the website for the shop and scrap the shop
     */
    public void scrapForShopsData(){
        if (shops.isEmpty()) {
            return;
        }

        if (scrapedShopIds.size() == AppVariables.shopScrapingNumber){
            return;
        }

        // If there is a current shopId we assume it has been scrapped
        if (shopId != 0) {
            scrapedShopIds.add(shopId);
            shops.remove(0);
        }

        urls.clear();
        shop = shops.get(0);

        shopId = shop.getId();
        if (shop.hasWebsite()) {
            url = shop.getWebsite();
            urls.add(url);
            System.out.println("web scraping get shop det: "+shop.getName()+" - "+depth+" - "+scrapedUrls.size());
            scrapForData();
        }else if (shop.hasDetails() == Shop.EHasDetails.UNKNOWN){
            System.out.println("web scraping get shop det: "+shop.getName());
            // This will call scrap data when details are returned
            ShopDetailsDataSet shopDetailsDataSet = new ShopDetailsDataSet(this);
            shopDetailsDataSet.getDataSet(shopId);
        }
    }

    @Override
    public void postGetDataSet(DataSet dataSet) {
        if (dataSet instanceof ShopDetailsDataSet){
            ShopDetailsDataSet shopDetailsDataSet = (ShopDetailsDataSet)dataSet;
            shop = shopDetailsDataSet.getShop();

            if (!shop.hasWebsite()){
                return;
            }
            System.out.println("web scraping post shop dat: "+shop.getName()+" "+shop.getWebsite());
            shopId=shop.getId();
            url = shop.getWebsite();
            urls.add(url);
            scrapForData();
        }
    }
    /**
     * After completing background task Dismiss the progress dialog
     * **/
    protected void onPostExecute(final CustomJSONObject data) {
        // Avoid running on main thread. Preparation of data will be heavy sometimes.
        new Thread(new Runnable() {
            public void run() {
                getWebData.postGetWebData(shopItems,logoUrl,scrapedUrls,shopId);
            }
        }).start();
    }

    public void JSOUPWebData(String url, int index) throws IOException {
        System.out.println("web get data url: "+url);
        if (url == null){
            return;
        }
        //Connect to website
        Connection.Response response = Jsoup.connect(url).execute();
        Document document = response.parse();
        analyseDocument(document,index,url);
    }

    public void WebServiceData(final String url, final int index){
        System.out.println("web scrapping url:"+url+" i:"+index);
         ((Activity)AppVariables.mainActivity).runOnUiThread(new Runnable() {
             @Override
             public void run() {
                 showProgress(true);
                 WebView webView = null;
                 if (urls.size() == 1 && activity != null) {
                     webView = activity.getWebView();
                     webView.loadUrl("javascript:document.open();document.close();");
                 }
                 //"https://www.pnp.co.za/b2c_pnp/start.jsp"
                 getWebData(webView,index,url);
             }
         });

    }

    public void getWebData(WebView webView, final int index, final String url){
        if (this.webView == null){
            if (webView != null){
                this.webView = webView;
            }else {
                this.webView = new WebView(context);
            }
            prepareWebView(index, url);
        }
        System.out.println("web scraper loading url data "+url);
        this.webView.loadUrl(url);
    }

    private void prepareWebView(final int index, final String url){
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(false);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setDomStorageEnabled(true);
        settings.setGeolocationEnabled(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(true);
        webView.addJavascriptInterface(
                new Object() {
                    @JavascriptInterface
                    public void showHTML(String html) {
                        Document document = Jsoup.parse(html);
                        analyseDocument(document,index,url);
                        // Need to call it again because first time it had no results
                        onPostExecute(null);
                        showProgress(false);
                    }
                },
                "HtmlViewer"
        );
        webView.setWebViewClient(new WebViewClient()
        {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onPageFinished(WebView view, String url)
            {
                System.out.println("web scraper got data "+url);
                webView.evaluateJavascript("javascript:window.HtmlViewer.showHTML(document.documentElement.innerHTML);", null);
            }
        });
    }

    public void oldGetWebData(WebView webView, final int index){
        WebScraper webScraper = new WebScraper(context, webView);
        webScraper.setRunInBackground(false);
        webScraper.setUserAgentToDesktop(true);
        webScraper.loadURL(url);

        final WebScraper finalWebScraper = webScraper;
        finalWebScraper.execute(new WebScraper.WebTaskListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void finished(Dictionary<String, String> result) {
                System.out.println("web scraper got data "+result.toString()+" "+url);

                finalWebScraper.getHtml(new WebScraper.WaitForHtml() {
                    @Override
                    public void gotHtml(String html) {
                        Document document = Jsoup.parse(html);
                        analyseDocument(document,index,url);
                        // Need to call it again because first time it had no results
                        onPostExecute(null);
                        showProgress(false);
                    }
                });
            }
        });
    }

    public void showProgress(final boolean show){
        if (activity == null){
            return;
        }
        activity.showProgress(show);
    }

    /**
     * First method which gets webpage, gets the logo (favicon) and gets document data
     * @param document
     * @param index
     */
    public void analyseDocument(Document document,int index, String url){
        Elements links = document.select("a[href]");
        if (!links.isEmpty()) {
            //First link is the website itself usually
            links.remove(0);
        }
        if (index == 0){
            getLogo(document);
        }
        for (Element link:links){
            String l = link.attr("abs:href");
            if (!l.isEmpty()) {
                nextLevelUrls.add(link.attr("abs:href"));
            }
        }
        System.out.println("web data urls \n"+nextLevelUrls.toString());
        getDocumentData(document,url, index);
    }

    public String getLogo(Document document){
        Element element = document.head().select("link[href~=.*\\.(ico|png)]").first();
        if (element != null) {
            //System.out.println("get logo 1");
            logoUrl = element.attr("abs:href");
            if (logoUrl == null || logoUrl.isEmpty()){
                logoUrl = getBaseUrl(element.attr("href"));
            }
        }
        //System.out.println("get logo 2: "+logoUrl);
        return logoUrl;
    }

    /**
     * Will populate the shopItems with priced items on this particular url
     * @param doc
     */
    public void getDocumentData(Document doc,String url,int index){
        if (!getDocumentData){
            return;
        }

        //System.out.println(doc.html());
        Elements elements = doc.getElementsByAttributeValueContaining("class","price");
        System.out.println("web data products size: "+elements.size());
        for (Element priceElement: elements){
            Element element = getParentPriceElement(priceElement);
            Element parentElement = element.parent();

            System.out.println("parent details: "+parentElement.text());
            String price = element.text();
            price = price.replaceAll("\\s","").trim();
            if (price.isEmpty()){
                price = parentElement.text();
            }

            System.out.println("web data products price: "+price);
            String[] details = getProductDetails(element,null,null, null);
            if (details[0] == null || details[1]==null){
                continue;
            }

            ShopItem shopItem = new ShopItem(details[0]);
            if (shopItems.containsKey(shopItem.getId())){
                System.out.println("shop item already got "+shopItem.getName()+" - "+shopItem.getId()+" - "+details[0]);
                continue;
            }
            shopItem.setShopId(shopId);
            shopItem.setPictureUrl(details[1]);
            shopItem.setDisplayPrice(price);
            shopItem.derivePrice();
            shopItem.setWebUrl(url);
            shopItem.setDetails(details[2]);
            shopItem.setOnSpecial();
            shopItems.put(shopItem.getId(),shopItem);
        }

        scrapNextUrl(elements.size(),index);
    }

    /**
     * Gets the price that encapsulates both the dollars and the cents.
     * @param priceElement
     * @return
     */
    public Element getParentPriceElement(Element priceElement){
        Element parentElement = priceElement.parent();
        if (parentElement == null){
            return priceElement;
        }

        if (!parentElement.hasAttr("class")){
            return priceElement;
        }

        if (!parentElement.attr("class").toLowerCase().contains("price")){
            return priceElement;
        }

        return getParentPriceElement(parentElement);
    }

    public Element getChildPriceElement(Element priceElement){
        Elements childElements = priceElement.children();
        for(Element childElement : childElements){
            if (childElement.hasAttr("class") && childElement.attr("class").toLowerCase().contains("price")){
                Element element = getChildPriceElement(childElement);

                String price = element.ownText().trim();
                Pattern pricePattern = Pattern.compile("\\d{1,5}[,\\.]?(\\d{1,5})?");
                Matcher m = pricePattern.matcher(price);
                if (m.find()){
                    return element;
                }
            }
        }
        return priceElement;
    }

    /**
     * Removes already scraped url and adds also it to scraped urls for storage later in DataSet class
     * Checks if urls are empty and loads the next level urls to be scraped if so
     * If there were no priced elements on the first page we do not continue scraping
     * @param pricedElements
     */
    public void scrapNextUrl(int pricedElements,int index){
        System.out.println("web scrapping scrap next, url size:"+urls.size()+" depth: "+depth);
        urls.remove(0);
        depth--;
        scrapedUrls.add(url);

        // If there were no actual priced products on first page usually website has no useful information
        if (pricedElements == 0 && index == 0){
            // If there are shops we are scraping, other shops with actual prices must be scrapped in their place
            if (!shops.isEmpty()) {
                shopId = 0;
                shops.remove(0);
                scrapForShopsData();
            }else{
                scrapForData();
            }
            return;
        }

        if (urls.isEmpty()){
            urls.addAll(nextLevelUrls);
        }
        scrapForData();
    }

    /**
     * This method takes a child element and recursively climbs up the parent
     * hierarchy looking for the product name and product image.
     * @param element
     * @param name
     * @param image
     * @return returns a comma separated string of name and image url
     */
    public String[] getProductDetails(Element element, String name, String image, String detail){
        String[] details = new String[]{name,image,detail};
        if (name != null && image != null && !name.isEmpty() && !image.isEmpty()){
            return details;
        }

        Pattern namePattern = Pattern.compile(".*name.*|.*title.*|.*description.*");
        Element parent = element.parent();

        if (parent == null){
            return details;
        }

        System.out.println("parent class "+parent.attr("class"));
        if (name == null) {
            Elements nameElements = parent.getElementsByAttributeValueMatching("class", namePattern);
            if (nameElements.isEmpty()) {
                nameElements = parent.select("[title]");
            }

            if (!nameElements.isEmpty()) {
                name = nameElements.get(0).text().trim();
                detail = parent.parent().text();
                if (name.isEmpty()){
                    name = nameElements.get(0).attr("title");
                }
                System.out.println("web data products name/title: " + name+" - "+parent.parent().text());
            }
        }

        //We can change the image as long as we haven't got the title yet
        //Seems to work better for some websites with small images for picking different color items etc.
        Elements imageElements = parent.select("img[class~=.*image.*|.*img.*|.*product.*]");
        if (imageElements.isEmpty()){
            imageElements = parent.select("img");
        }
        if (!imageElements.isEmpty()) {
            Element imageElement =imageElements.first();
            image = getImageUrl(imageElement);
        }

        details = getProductDetails(parent,name,image,detail);
        return details;
    }

    /**
     * For some img src the absUrl does not work using JSOUP elements.
     * So checking that and using base url of main website to get the pictures.
     * @param imageElement
     * @return
     */
    private String getImageUrl(Element imageElement){
        String imgUrl = imageElement.attr("src").trim();
        imgUrl = getBaseUrl(imgUrl);
        System.out.println("web data products image: " + imgUrl);
        System.out.println("img "+imageElement.outerHtml());
        return imgUrl;
    }

    /**
     * Will return a url with the base if it already is not a base one
     * @param url
     * @return
     */
    public String getBaseUrl(String url){
        if (url.isEmpty()){
            return url;
        }
        if (url.startsWith("http") || url.startsWith("https")){
            return url;
        }
        if (url.startsWith("//")){
            return "http:"+url;
        }
        if (baseUrl == null || url == null){
            return url;
        }
        if (baseUrl.charAt(baseUrl.length()-1) != '/'){
            baseUrl += "/";
        }
        if (url.charAt(0) == '/'){
            url = url.substring(1);
        }
        url = baseUrl + url;
        return url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String urlString){
        this.url = urlString;
        this.baseUrl = urlString;
        urls.add(urlString);

        try {
            URL url = new URL(urlString);
            baseUrl = url.getProtocol() + "://" + url.getHost();
        } catch (MalformedURLException e) { }
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public void getDocumentData(boolean getDocumentData){
        this.getDocumentData=getDocumentData;
    }

    public IActivity getActivity() {
        return activity;
    }

    public void setActivity(IWebViewActivity activity) {
        this.activity = activity;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public void setShop(Shop shop){
        this.shop=shop;
    }

    public void setGetLogo(boolean getLogo){
        this.getLogo=getLogo;
    }

    public void setShops(SyncLinkedHashMap<Integer, Shop> shops){
        this.shops = new ArrayList<>(shops.values());
        Collections.sort(this.shops, new Comparator<Shop>() {
            public int compare(Shop a, Shop b){
                return a.compareTo(b);
            }
        });
    }
}
