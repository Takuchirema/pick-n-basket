package org.picknbasket.picknbasket.Data;

import androidx.appcompat.app.AppCompatActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.picknbasket.picknbasket.Abstracts.DataSet;
import org.picknbasket.picknbasket.Abstracts.IActivity;
import org.picknbasket.picknbasket.Abstracts.IGetDataSet;
import org.picknbasket.picknbasket.Abstracts.IWebViewActivity;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.ObjectCache;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Objects.Shop;
import org.picknbasket.picknbasket.Objects.ShopItem;
import org.picknbasket.picknbasket.Objects.ShopWebPage;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Takunda on 1/20/2020.
 */

public class ShopItemsDataSet extends DataSet {
    private String url;
    private SyncLinkedHashMap<Integer,ShopItem> shopItems = new SyncLinkedHashMap<>();
    private int shopId;
    private int shopItemId;
    private Shop shop;
    private IActivity activity;
    private String html;

    public ShopItemsDataSet(IGetDataSet getDataset) {
        super(getDataset);
    }

    public void getDataSet(int shopId, IWebViewActivity activity){
        this.shopId=shopId;
        this.activity=activity;
        shop = SQLite.getShop(shopId);

        System.out.println("web get data 1: "+shopId);
        //check if shops are already available for this location in DB
        boolean scrapWebData = scrapWebData();
        if (!scrapWebData) {
            System.out.println("shop items db data");
            if (shop != null && !shop.hasPicture()){
                getShopLogo();
            }
            if (shopId == 0) {
                shopItems = SQLite.getShopItems(null);
            } else {
                shopItems = SQLite.getShopItems(new ArrayList<>(Arrays.asList(shopId)));
            }
            success = true;
            postGetDataSet(this);
            return;
        }
        getWebData(activity);
    }

    public void getDataSet(){
        if (shopItemId == 0){
            success = false;
            postGetDataSet(this);
            return;
        }

        ShopItem shopItem = ObjectCache.getShopItem(shopId, shopItemId);
        if (shopItem != null){
            shopItems.put(shopItemId, shopItem);
            success = true;
            postGetDataSet(this);
            return;
        }

        shopItem = SQLite.getShopItem(shopItemId);
        shopItems.put(shopItemId, shopItem);
        success = true;
        postGetDataSet(this);
    }

    public void getWebData(IWebViewActivity activity){
        if (url == null){
            url = shop.getWebsite();
        }

        if (url == null || url.isEmpty()){
            success = false;
            postGetDataSet(this);
            return;
        }

        System.out.println("web get data 2: "+shopId);
        GetWebData getWebData = new GetWebData(this);
        getWebData.setActivity(activity);
        getWebData.setUrl(url);
        getWebData.setHtml(html);
        getWebData.getDocumentData(true);
        getWebData.setDepth(AppVariables.webScrapingDepth);
        getWebData.setShopId(shopId);
        getWebData.setShop(shop);
        getWebData.execute();
    }

    /**
     * This method gets the best two stores with price details to scrap them
     * @param shops
     */
    public void getDataSet(SyncLinkedHashMap<Integer, Shop> shops, IWebViewActivity activity){
        System.out.println("shop items ds - get ds");
        GetWebData getWebData = new GetWebData(this);
        //getWebData.setActivity(activity);
        getWebData.getDocumentData(true);
        getWebData.setDepth(AppVariables.backgroundWebScrapingDepth);
        getWebData.setShops(shops);
        getWebData.execute();
    }

    /**
     * Checks whether the given url has already been scrapped or not
     * @return
     */
    private boolean scrapWebData(){
        String webUrl = url;
        if (shopId == 0){
            return false;
        }

        if (!shop.hasWebsite()){
            return false;
        }

        if (url == null){
            //webUrl = shop.getWebsite();
            return false;
        }

        boolean scraped = SQLite.hasScraped(shopId,webUrl);

        if (scraped){
            System.out.println("url: "+webUrl+" has been scraped");
            return false;
        }

        System.out.println("url: "+webUrl+" has NOT been scraped");
        return true;
    }

    public void setUrl(String url){
        this.url=url;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public void setShopItemId(int shopItemId) {
        this.shopItemId = shopItemId;
    }

    public void setShopId(int shopId){
        this.shopId = shopId;
    }

    @Override
    public void prepareDataSet(Object data){
        if (data != null){
            shopItems=(SyncLinkedHashMap)data;
            setWebPageIds();
            System.out.println("shop items prep data "+shopItems.size());
            SQLite.storeShopItems(shopItems);
        }
    }

    @Override
    public void postGetWebData(Object data,String logoUrl,ArrayList<String> scrapedUrls,int shopId){
        if (!shop.hasWebsite()) {
            if (logoUrl != null && !logoUrl.isEmpty()) {
                System.out.println("web logo url " + logoUrl);
                shop.setPictureUrl(logoUrl);
                SQLite.storeShop(shop, null);
            }
        }
        super.postGetWebData(data,logoUrl,scrapedUrls,shopId);
    }

    private void setWebPageIds(){
        for(ShopItem shopItem:shopItems.values()){
            if(webpages.containsKey(shopItem.getWebUrl())){
                ShopWebPage webPage = webpages.get(shopItem.getWebUrl());
                shopItem.setWebId(webPage.getId());
            }
        }
    }

    private void getShopLogo(){
        System.out.println("1 get shop logo: "+shop.getName());
        if (html == null){
            return;
        }
        System.out.println("2 get shop logo: "+shop.getName());
        Document document = Jsoup.parse(html);
        GetWebData getWebData = new GetWebData(this);
        getWebData.setUrl(url);
        String logoUrl = getWebData.getLogo(document);
        shop.setPictureUrl(logoUrl);
        SQLite.storeShop(shop,null);
    }

    public SyncLinkedHashMap<Integer, ShopItem> getShopItems() {
        return shopItems;
    }
}
