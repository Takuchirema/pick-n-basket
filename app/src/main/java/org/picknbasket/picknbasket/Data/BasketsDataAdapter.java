package org.picknbasket.picknbasket.Data;

import org.picknbasket.picknbasket.Abstracts.DataAdapter;
import org.picknbasket.picknbasket.Abstracts.DataSet;
import org.picknbasket.picknbasket.Abstracts.IDataAdapterCall;
import org.picknbasket.picknbasket.Abstracts.IGetDataSet;
import org.picknbasket.picknbasket.Objects.Basket;
import org.picknbasket.picknbasket.Objects.ShopItem;

public class BasketsDataAdapter extends DataAdapter<Basket> {

    public BasketsDataAdapter(IDataAdapterCall dataAdapterCall){
        super(dataAdapterCall);
    }

    @Override
    public void createData(Basket basket) {
        Basket newBasket = SQLite.storeBasket(basket);
        postCreateData(newBasket);
    }

    @Override
    public void updateData(Basket basket) {
        if (basket == null){
            return;
        }

        SQLite.storeBasketItems(basket.getId(),basket.getBasketItems());
        Basket newBasket = SQLite.storeBasket(basket);
        postUpdateData(newBasket);
    }

    @Override
    public void deleteData(Basket basket) {
        boolean success = SQLite.deleteBasket(basket);
        postDeleteData(success);
    }

    public void resetBasket(Basket basket){
        for (ShopItem shopItem: basket.getBasketItems()){
            shopItem.setPicked(false);
            shopItem.setCount(1);
        }
        updateData(basket);
    }

    @Override
    public void postCreateData(Basket basket) {
        dataAdapterCall.postCreateData(basket);
    }

    @Override
    public void postUpdateData(Basket basket) {
        dataAdapterCall.postUpdateData(basket);
    }

    @Override
    public void postDeleteData(boolean success) {
        dataAdapterCall.postDeleteData(success);
    }
}
