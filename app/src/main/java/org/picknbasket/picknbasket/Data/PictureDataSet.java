package org.picknbasket.picknbasket.Data;

import android.graphics.Bitmap;

import org.picknbasket.picknbasket.Abstracts.IGetPicture;
import org.picknbasket.picknbasket.Abstracts.IGetServerPicture;

/**
 * Created by Takunda on 22/06/2018.
 */

public class PictureDataSet implements IGetServerPicture {

    private int success = 0;
    private String message;
    private String pictureUrl;
    private IGetPicture getPicture;

    public PictureDataSet(String pictureUrl, IGetPicture getPicture){
        this.pictureUrl=pictureUrl;
        this.getPicture=getPicture;
    }

    public void getDataSet(){
        GetServerPicture getServerPicture = new GetServerPicture(this);
        getServerPicture.setPictureUrl(pictureUrl);
        getServerPicture.execute();
    }

    @Override
    public void postGetServerPicture(Bitmap bmp) {
        if (bmp != null){
            success = 1;
            message= "Successfully retrieved picture";
            getPicture.postGetPicture(bmp);
        }else {
            success = 0;
            message = "Could not retrieve the picture";
            getPicture.postGetPicture(null);
        }
    }

}
