package org.picknbasket.picknbasket.Data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Takunda on 11/07/2018.
 */

public class CustomJSONObject extends org.json.JSONObject {

    public CustomJSONObject(JSONObject json) throws JSONException {
        super(json.toString());
    }

    @Override
    /**
     * Returns the value mapped by {@code name} if it exists, coercing it if
     * necessary, or throws if no such mapping exists.
     *
     * @throws JSONException if no such mapping exists.
     */
    public String getString(String name) throws JSONException {
        if (!super.has(name)){
            return null;
        }

        String value = super.getString(name);
        if (value.equalsIgnoreCase("null")){
            return null;
        }
        if (value.isEmpty()){
            return null;
        }
        return value;
    }

    /**
     * Returns the value mapped by {@code name} if it exists and is an int or
     * can be coerced to an int, or returns 0 otherwise.
     *
     * returns 0 if the mapping doesn't exist or cannot be coerced
     *     to an int.
     */
    public int getInt(String name) throws JSONException {
        if (!super.has(name)){
            return 0;
        }

        int value = 0;
        try {
            value = super.getInt(name);
        }catch (Exception ex){}

        return value;
    }

    public double getDouble(String name) throws JSONException {
        if (!super.has(name)){
            return 0;
        }

        double value = 0;
        try {
            value = super.getDouble(name);
        }catch (Exception ex){}

        return value;
    }

    /**
     * Returns the value mapped by {@code name} if it exists and is a {@code
     * JSONObject}, or throws otherwise.
     *
     * @throws JSONException if the mapping doesn't exist or is not a {@code
     *     JSONObject}.
     */
    public CustomJSONObject getJSONObject(String name) throws JSONException {
        if (!super.has(name)){
            return null;
        }
        JSONObject object = super.getJSONObject(name);
        CustomJSONObject customJSONObject = new CustomJSONObject(object);
        return customJSONObject;
    }

    /**
     * Returns the value mapped by {@code name} if it exists and is a {@code
     * JSONArray}, or throws otherwise.
     *
     * @throws JSONException if the mapping doesn't exist or is not a {@code
     *     JSONArray}.
     */
    public CustomJSONArray getJSONArray(String name) throws JSONException {
        JSONArray jsonArray = super.getJSONArray(name);
        CustomJSONArray customJSONArray = new CustomJSONArray(jsonArray);
        return customJSONArray;
    }
}
