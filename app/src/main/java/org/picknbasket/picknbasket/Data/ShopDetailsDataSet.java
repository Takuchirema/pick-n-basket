package org.picknbasket.picknbasket.Data;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.jsoup.select.Elements;
import org.picknbasket.picknbasket.Abstracts.DataSet;
import org.picknbasket.picknbasket.Abstracts.EHttpMethod;
import org.picknbasket.picknbasket.Abstracts.IGetDataSet;
import org.picknbasket.picknbasket.Abstracts.IWebViewActivity;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Objects.Shop;

import java.util.ArrayList;

/**
 * Created by Takunda on 1/16/2020.
 */

public class ShopDetailsDataSet extends DataSet{
    private String url;
    private Shop shop;
    private int shopId;

    public ShopDetailsDataSet(IGetDataSet getDataset) {
        super(getDataset);
    }

    public void getDataSet(int shopId){
        //check if shops are already available for this location in DB
        shop = SQLite.getShop(shopId);
        if (shop == null) {
            success = false;
            postGetDataSet(this);
            return;
        }

        System.out.println("shop det "+shop.getName()+" "+shop.hasDetails().toString()+" "+shop.getPhoneNumber()+" "+shop.getPictureUrl());
        if (shop.hasDetails() != Shop.EHasDetails.UNKNOWN && !shop.fetchPictureUrl()){
            success = true;
            postGetDataSet(this);
            return;
        }

        this.shopId=shopId;
        String url = getUrl(shop.getPlaceId());

        GetServerData getServerData = new GetServerData(this);
        getServerData.setUrl(url);
        getServerData.setHttpMethod(EHttpMethod.GET);
        getServerData.execute();
    }

    //https://maps.googleapis.com/maps/api/place/details/json?place_id=ChIJmQK4edxCzB0RrLnDvmP1SnU&fields=opening_hours,website,formatted_phone_number&key=AIzaSyA9_61L9dnFJyA2akkDLcP14hlEtdeh-D0
    private String getUrl(String placeId){
        if (url != null){
            return url;
        }

        String url = "https://maps.googleapis.com/maps/api/place/details/json?"+
                    "place_id="+placeId+"&" +
                    "fields=opening_hours,website,formatted_phone_number&"+
                    "type=grocery_or_supermarket&"+
                    "key=AIzaSyA9_61L9dnFJyA2akkDLcP14hlEtdeh-D0";

        return url;
    }

    public void setUrl(String url){
        this.url=url;
    }

    @Override
    public void prepareDataSet(CustomJSONObject json) throws JSONException {
        ArrayList<Shop> newShops = new ArrayList<>();

        CustomJSONObject shopDetails = json.getJSONObject("result");

        String phoneNumber = shopDetails.getString("formatted_phone_number");
        String website = shopDetails.getString("website");
        String businessHours = "";
        getShopLogo(website);

        CustomJSONObject openingHours = shopDetails.getJSONObject("opening_hours");
        if (openingHours != null) {
            CustomJSONArray jsonArray = openingHours.getJSONArray("weekday_text");
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    businessHours = jsonArray.getString(i) + "," + businessHours;
                }
            }
        }

        shop.setPhoneNumber(phoneNumber);
        shop.setWebsite(website);
        shop.setBusinessHours(businessHours);

        shop = SQLite.storeShop(shop,null);
    }

    @Override
    public void postGetWebData(Object data,String logoUrl,ArrayList<String> scrapedUrls,int shopId){
        if (logoUrl != null && !logoUrl.isEmpty()){
            System.out.println("web logo url "+logoUrl);
            shop.setPictureUrl(logoUrl);
            SQLite.storeShop(shop,null);
        }
    }

    private void getShopLogo(String website){
        GetWebData getWebData = new GetWebData(this);
        getWebData.setUrl(website);
        getWebData.getDocumentData(false);
        getWebData.setGetLogo(true);
        getWebData.execute();
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
