package org.picknbasket.picknbasket.Data;

import android.content.Context;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;

import org.picknbasket.picknbasket.Abstracts.EObjectType;
import org.picknbasket.picknbasket.BuildConfig;
import org.picknbasket.picknbasket.Helpers.AppVariables;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.SSLContext;

public class JSONParser {

    public static String jsonDirections;
    public static String jsonAddress;
    private HttpURLConnection connection;
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    int timeoutConnection = 5000;
    int readTimeout = 5000;
    private String versionNumber = "";

    public static final String TAG_SUCCESS = "success";
    public static final String TAG_MESSAGE = "message";

    // constructor
    public JSONParser() {

    }

    public String getDirections()
    {
        return jsonDirections;
    }

    public String getAddress()
    {
        return jsonAddress;
    }


    public CustomJSONObject getJSONFromUrl(final String url) throws JSONException, IOException {

        // Making HTTP request
        try {
            // Construct the client and the HTTP request.
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            // Execute the POST request and store the response locally.
            HttpResponse httpResponse = httpClient.execute(httpPost);
            // Extract data from the response.
            HttpEntity httpEntity = httpResponse.getEntity();
            // Open an inputStream with the data content.
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Return the JSON Object.
        return getDataFromStream();
    }


    // function get json from url
    public CustomJSONObject makeHttpRequest(String urlStr, String method, List<NameValuePair> params) throws JSONException, IOException {
        urlStr = urlStr.replace(" ","%20");

        // Making HTTP request
        try {
            // check for request method
            if(method.equals("POST") || method.equals("PATCH")){
                System.out.println("URL - "+urlStr);
                URL url = new URL(urlStr);
                connection = (HttpURLConnection)url.openConnection();

                if (AppVariables.token != null) {
                    setHeader(connection);
                }

                if (method.equals("POST")) {
                    System.out.println("Post");
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);

                }else if (method.equals("PATCH")){
                    System.out.println("Patch");
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("PATCH");
                    connection.setDoInput(true);
                }

                connection.setConnectTimeout(timeoutConnection);
                connection.setReadTimeout(readTimeout);

                if (params != null) {
                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(getQuery(params));
                    writer.flush();
                    writer.close();
                    os.close();
                }

                connection.connect();
                //System.out.println("check4: parser");
                is = connection.getInputStream();
                //System.out.println("check6: parser");

            }else if(method.equals("GET") || method.equals("DELETE")){
                System.out.println("url - "+urlStr);
                URL url = new URL(urlStr);
                connection = (HttpURLConnection)url.openConnection();

                if (AppVariables.token != null) {
                    setHeader(connection);
                }

                if (method.equals("GET")) {
                    System.out.println("Get");
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                }else if (method.equals("DELETE")){
                    System.out.println("Delete");
                    connection.setRequestProperty("User-Agent", "");
                    connection.setRequestMethod("DELETE");
                    connection.setDoInput(true);
                }

                connection.setConnectTimeout(timeoutConnection);
                connection.setReadTimeout(readTimeout);

                connection.connect();
                //System.out.println("check4: parser");
                is = connection.getInputStream();
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println(" !!!!!!!! Unsupported encoding");
            e.printStackTrace();
            return null;
        } catch (ClientProtocolException e) {
            System.out.println(" !!!!!!!! Client Protocol");
            e.printStackTrace();
            return null;
        } catch(SocketTimeoutException e){
            System.out.println(" !!!!!!!! Time out exception");
            e.printStackTrace();
            return null;
        }catch (IOException e) {
            System.out.println(" !!!!!!!! IOException "+e.getMessage());
            e.printStackTrace();
            return null;
        }

        // return JSON String
        return getDataFromStream();
    }

    private CustomJSONObject getDataFromStream() throws JSONException, IOException {

        System.out.println("connection encoding "+connection.getContentEncoding());
        if ("gzip".equals(connection.getContentEncoding())) {
            is = new GZIPInputStream(is);
        }

        long starTime = System.currentTimeMillis();
        json = IOUtils.toString(is);
        //json = removeNotices(json);
        long elapsedTime = System.currentTimeMillis() - starTime;
        System.out.println("connection time "+elapsedTime+" "+json.length());
        is.close();
        //FileManager fileManager = new FileManager(AppVariables.context);
        //fileManager.writeToFile("jsondata.txt",json);
        //System.out.println("trying to print json: "+json +"  *******");

        jObj = new JSONObject(json);
        System.out.println("check11: parser");

        return new CustomJSONObject(jObj);
    }

    public void setHeader(HttpURLConnection connection){}

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            if (pair.getValue() == null)
                result.append(URLEncoder.encode("", "UTF-8"));
            else
                result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public String removeNotices(String json)
    {
        int indexStart = json.indexOf("<br />");
        int indexEnd = json.lastIndexOf("<br />");

        //System.out.println("edited json: "+json+ " start "+indexStart+" end "+indexEnd);
        return json;
    }

    public static void initializeSSLContext(Context context){
        try{
            SSLContext.getInstance("TLSv1.2");
        }catch (NoSuchAlgorithmException ex){
            ex.printStackTrace();
        }

        try {
            ProviderInstaller.installIfNeeded(context.getApplicationContext());
        }catch (GooglePlayServicesRepairableException e){
            e.printStackTrace();
        }catch (GooglePlayServicesNotAvailableException e){
            e.printStackTrace();
        }
    }

    public int getTimeoutConnection() {
        return timeoutConnection;
    }

    public void setTimeoutConnection(int timeoutConnection) {
        this.timeoutConnection = timeoutConnection;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }
}