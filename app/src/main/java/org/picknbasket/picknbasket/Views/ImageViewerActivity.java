package org.picknbasket.picknbasket.Views;

import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.ImageView;

import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Helpers.TouchImageView;
import org.picknbasket.picknbasket.Managers.ViewManager;
import org.picknbasket.picknbasket.R;

public class ImageViewerActivity extends AppCompatActivity {

    private String objectId;
    private String companyId;
    private String pictureUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        String title = getIntent().getExtras().getString("title");
        actionBar.setTitle(title);

        pictureUrl = getIntent().getExtras().getString("pictureUrl");

        setImage();
    }

    private void setImage(){
        ImageView imageView = (TouchImageView)findViewById(R.id.image);
        ViewManager viewManager = new ViewManager(imageView, EActionType.DOWNLOAD_PICTURE,this);
        viewManager.getPicture();

        viewManager.setUrl(pictureUrl);
        viewManager.setObject(this);
        viewManager.getPicture();
    }
}
