package org.picknbasket.picknbasket.Views;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Abstracts.IActivity;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Managers.ShopsManager;
import org.picknbasket.picknbasket.Objects.ShopItem;
import org.picknbasket.picknbasket.R;

public class ShopItemActivity extends AppCompatActivity implements IActivity {

    private int shopId;
    private int shopItemId;
    private View mainView;
    private TextView titleTV;
    private ImageView imageIV;
    private ImageView brandIV;
    private TextView brandTV;
    private TextView secondaryTV;
    private TextView infoTV;

    private ShopsManager shopsManager;
    private Context context;
    private ShopItem shopItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_item);

        context = this;
        
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("shopItemId")) {
            shopItemId = extras.getInt("shopItemId");
        }
        if (extras != null && extras.containsKey("shopId")) {
            shopId = extras.getInt("shopId");
        }

        UISetup();
        AppSetup();
    }

    public void UISetup(){
        mainView = findViewById(R.id.grid_view_container);
        titleTV = findViewById(R.id.primary_tv);
        imageIV = findViewById(R.id.primary_iv);
        brandIV = findViewById(R.id.brand_iv);
        brandTV = findViewById(R.id.brand_tv);
        secondaryTV = findViewById(R.id.secondary_tv);
        infoTV = findViewById(R.id.info_tv);
    }

    public void AppSetup(){
        shopsManager = new ShopsManager();
        shopsManager.getShopItem(shopId, shopItemId, this);
    }
    
    @Override
    public void populateMainView(Object object) {
        //Toast.makeText(context, "populate main view shop item", Toast.LENGTH_LONG).show();
        if (object instanceof SyncLinkedHashMap){
            SyncLinkedHashMap<Integer, ShopItem> shopItems = (SyncLinkedHashMap)object;
            for (ShopItem shopItem:shopItems.values()) {
                this.shopItem = shopItem;

                titleTV.setText(shopItem.getName()+"");
                brandTV.setText(shopItem.getBrandText()+"");
                secondaryTV.setText(shopItem.getDisplayPrice());
                infoTV.setText(shopItem.getDetails());

                shopItem.setListActivity(this);
                shopItem.setView(mainView);
                shopItem.setPrimaryView();
                shopItem.setSecondaryView(mainView);

                break;
            }
        }
    }

    @Override
    public void postAction(EActionType actionType, Object object) {

    }

    @Override
    public Context getContext() {
        return context;
    }
}
