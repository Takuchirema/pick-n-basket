package org.picknbasket.picknbasket.Views;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Abstracts.IGetPicture;
import org.picknbasket.picknbasket.Abstracts.IWebViewActivity;
import org.picknbasket.picknbasket.Data.SQLite;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.WebScraper;
import org.picknbasket.picknbasket.Managers.ShopsManager;
import org.picknbasket.picknbasket.Objects.Shop;
import org.picknbasket.picknbasket.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Dictionary;

public class ShopOnlineActivity extends AppCompatActivity implements IGetPicture, IWebViewActivity {

    private TextView mTextMessage;
    private Context context;
    private int shopId;
    private Shop shop;
    private ActionBar actionBar;
    private WebScraper webScraper;
    private WebView webView;
    private ShopsManager shopsManager;

    private FloatingActionMenu menuRed;
    private FloatingActionButton editShop;
    private FloatingActionButton shopItems;
    private FloatingActionButton refresh;
    private FloatingActionButton exit;

    private ProgressBar progressBar;
    private FrameLayout progressBarHolder;

    private boolean getItemsAndView;
    private boolean getItemsAndFinish;

    private String currentUrl;
    private String currentHtml;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_online);

        context = this;
        shopId = getIntent().getExtras().getInt("shopId");
        shop = SQLite.getShop(shopId);

        UISetup();
        AppSetup();
    }

    public void UISetup(){
        menuRed = findViewById(R.id.menu_red);
        menuRed.setClosedOnTouchOutside(true);
        menuRed.setIconAnimated(false);
        menuRed.open(true);

        editShop = findViewById(R.id.edit_shop);
        shopItems = findViewById(R.id.shop_items);
        refresh = findViewById(R.id.refresh);
        exit = findViewById(R.id.exit);

        editShop.setOnClickListener(clickListener);
        shopItems.setOnClickListener(clickListener);
        refresh.setOnClickListener(clickListener);
        exit.setOnClickListener(clickListener);

        webView = findViewById(R.id.web_view);
        mTextMessage = findViewById(R.id.message);

        progressBar = findViewById(R.id.progress_bar);
        progressBarHolder = findViewById(R.id.progress_bar_holder);
    }

    public void AppSetup(){
        shopsManager = new ShopsManager();
        if (!shop.hasWebsite()){
            shopsManager.getShop(shopId,this);
        }else{
            //getWebView();
            getView();
        }
    }

    @Override
    public void postGetPicture(Bitmap bmp) {
        if (bmp != null){
            shop.setBmp(bmp);
            Drawable drawable = new BitmapDrawable(getResources(), bmp);
            actionBar.setLogo(drawable);
        }
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            menuRed.close(true);
            Intent intent;
            switch (v.getId()) {
                case R.id.refresh:
                    webView.reload();
                    break;
                case R.id.edit_shop:
                    intent = new Intent(context, ShopActivity.class);
                    intent.putExtra("shopId", shopId);
                    startActivity(intent);
                    break;
                case R.id.shop_items:
                    getShoppingItems(true,true);
                    break;
                case R.id.exit:
                    finish();
                    break;
            }
        }
    };

    public WebView getWebView(){
        webView = findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient()
        {
            @Override
            public void onPageFinished(WebView view, String url)
            {
                final String injectedJs = "javascript:(function(){" + injectedJS() + "})()";
                webView.loadUrl(injectedJs);
            }
        });
        webView.addJavascriptInterface(
                new Object() {
                    @JavascriptInterface
                    public void onClick(String param) {
                        Toast.makeText(context, param, Toast.LENGTH_LONG).show();
                    }
                },
                "appHost"
        );
        webView.loadUrl(shop.getWebsite());
        return webView;
    }

    public void getView() {
        if (!shop.hasWebsite()){
            Toast.makeText(context, shop.getName()+" has no website", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(context, ShopActivity.class);
            intent.putExtra("shopId", shopId);
            startActivity(intent);
            finish();
            return;
        }
        showProgress(true);
        webScraper = new WebScraper(this,webView);
        webScraper.setLoadImages(true); //default: false
        webScraper.loadURL(shop.getWebsite());
        webScraper.setOnPageLoadedListener(new WebScraper.PageLoadedListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void loaded(String url) {
                currentUrl = url;
                menuRed.close(true);
                if (webScraper.html != null) {
                    System.out.println("loaded " + webScraper.htmlUrl + " " + webScraper.html.substring(0,10));
                }else {
                    System.out.println("loaded " + webScraper.htmlUrl + " " + webScraper.html);
                }
                getShoppingItems();
            }
        });

        webScraper.execute(new WebScraper.WebTaskListener() {
            @Override
            public void finished(Dictionary<String, String> result) {
                showProgress(false);
            }
        });
    }

    private void getShoppingItems(boolean getItemsAndView, boolean getItemsAndFinish){
        this.getItemsAndView = getItemsAndView;
        this.getItemsAndFinish = getItemsAndFinish;
        webScraper.getHtml(new WebScraper.WaitForHtml() {
            @Override
            public void gotHtml(String html) {
                getShoppingItems(webScraper.currentUrl, html);
            }
        });
    }

    private void getShoppingItems(){
        if (webScraper == null){
            return;
        }
        if (webScraper.html != null) {
            System.out.println("get shopping items " + webScraper.htmlUrl + " " + webScraper.html.substring(0,10));
        }
        getShoppingItems(webScraper.htmlUrl, webScraper.html);
    }

    private void getShoppingItems(String url, String html){
        if (url != null && html != null) {
            shopsManager.getShopItems(shopId, url, this, html);
        }
    }

    // Javascript code to inject on the Web page
    private String injectedJS() {
        BufferedReader stream = null;
        StringBuilder jsBuilder = new StringBuilder();
        try {
            stream = new BufferedReader(new InputStreamReader(getAssets().open("js.js")));
            String line;
            while ((line = stream.readLine()) != null) {
                jsBuilder.append(line.trim());
            }
            return jsBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webView.canGoBack()) {
                        getShoppingItems();
                        webView.goBack();
                    } else {
                        getShoppingItems(false,true);
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void populateMainView(Object object) {
        if (object instanceof Shop){
            this.shop=(Shop)object;
            getView();
        }
    }

    @Override
    public void postAction(EActionType actionType, Object object) {
        switch (actionType){
            case POST_REQUEST:
                if (getItemsAndView){
                    getItemsAndView=false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Bundle bundle = new Bundle();
                            bundle.putInt("shopId", shopId);
                            ShopItemsActivity fragment = new ShopItemsActivity();
                            fragment.setArguments(bundle);
                            AppVariables.mainActivity.loadFragment(fragment,R.id.navigation_items);
                            finish();
                        }
                    });
                }else if (getItemsAndFinish){
                    getItemsAndFinish = false;
                    finish();
                }
                break;
        }
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public ProgressBar getProgressBar() {
        return progressBar;
    }

    @Override
    public void showProgress(boolean show) {
        if (show){
            progressBarHolder.setVisibility(View.VISIBLE);
        }else{
            progressBarHolder.setVisibility(View.GONE);
        }
    }
}
