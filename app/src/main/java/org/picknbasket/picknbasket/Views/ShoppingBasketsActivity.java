package org.picknbasket.picknbasket.Views;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Abstracts.IActivity;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.ListAdapter;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Managers.BasketsManager;
import org.picknbasket.picknbasket.Objects.Basket;
import org.picknbasket.picknbasket.R;

import java.util.ArrayList;

public class ShoppingBasketsActivity extends Fragment implements IActivity {

    private Context context;
    private View mainView;
    private ListView listView;
    private BasketsManager basketsManager;
    private FloatingActionButton newBasket;
    private FloatingActionMenu menuGreen;

    private ListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        context = AppVariables.context;
        mainView = inflater.inflate(R.layout.activity_shopping_baskets, null);

        UISetup();
        AppSetup();

        return mainView;
    }

    private void UISetup() {
        listView = mainView.findViewById(R.id.baskets_list);

        menuGreen = mainView.findViewById(R.id.menu_green);
        menuGreen.setClosedOnTouchOutside(true);
        menuGreen.setIconAnimated(false);

        newBasket = mainView.findViewById(R.id.new_basket);
        newBasket.setOnClickListener(clickListener);
    }

    private void AppSetup(){
        basketsManager = new BasketsManager();
        basketsManager.getBaskets(this);
    }

    public void refresh(){
        basketsManager.getBaskets(this);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onClick(View v) {
            //menuOrange.close(true);
            Intent intent;
            switch (v.getId()) {
                case R.id.new_basket:
                    intent = new Intent(context,ShoppingBasketActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    };

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(SyncLinkedHashMap<Integer,Basket> baskets){
        ArrayList<Integer> shopIds = new ArrayList<>(baskets.keySet());

        adapter = new ListAdapter(getActivity(),ShoppingBasketActivity.class,shopIds,listView, R.layout.content_basket_list_item);
        adapter.setMapObjects(baskets);
        listView.setAdapter(adapter);

        adapter.notifyDataSetChanged();
    }

    @Override
    public void populateMainView(Object object) {
        SyncLinkedHashMap<Integer, Basket> baskets = (SyncLinkedHashMap)object;
        createListAdapter(baskets);
    }

    @Override
    public void postAction(EActionType actionType, Object object) {

    }
}
