package org.picknbasket.picknbasket.Views;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseSequence;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;

import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Abstracts.IActivity;
import org.picknbasket.picknbasket.Abstracts.IMainActivity;
import org.picknbasket.picknbasket.Abstracts.ISearchable;
import org.picknbasket.picknbasket.BuildConfig;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Managers.FileManager;
import org.picknbasket.picknbasket.Managers.PermissionsManager;
import org.picknbasket.picknbasket.Managers.ShopsManager;
import org.picknbasket.picknbasket.R;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements IMainActivity{

    private Context context;
    private ShopsManager shopsManager;
    private PermissionsManager permissionsManager;
    private SearchView searchView;
    private FrameLayout fragmentContainer;
    // Keeps track of the active fragment
    private Fragment fragment;

    private static BottomNavigationView navigation;
    private IActivity activity;
    private BubbleShowCaseSequence showCaseSequence = new BubbleShowCaseSequence();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        permissionsManager = new PermissionsManager(this);

        UISetup();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsManager.canAccessLocation()) {
                AppSetup();
            } else {
                permissionsManager.requestPermissions();
            }
        }else {
            AppSetup();
        }
    }

    private void AppSetup(){
        AppVariables.mainActivity =this;
        AppVariables.context = this;
        AppVariables.setLocationManager();
        AppVariables.setNotificationManager();
        setCrashListener();

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {

            }
        });

        shopsManager = new ShopsManager();
        shopsManager.startManager();

        loadFragment(new ShopsActivity(), 0);
    }

    private void UISetup() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setLogo(R.drawable.logo_white);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setTitle("");

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    /**
     * Only call when search view has been populated
     */
    public void TutorialSetup(){
        if (!showTutorial()){
            return;
        }

        View shopsNav = navigation.findViewById(R.id.navigation_shops);
        View itemsNav = navigation.findViewById(R.id.navigation_items);
        View basketsNav = navigation.findViewById(R.id.navigation_baskets);

        //no
        BubbleShowCaseBuilder b1 = new BubbleShowCaseBuilder(this)
                .title("Search")
                .description(
                            "1. Search for supermarkets\n"+
                            "2. Only supermarkets close to you are loaded")
                .backgroundColor(Color.parseColor(getString(0+R.color.blue)))
                .targetView(searchView);
        BubbleShowCaseBuilder b2 = new BubbleShowCaseBuilder(this)
                .title("Shops")
                .description(
                            "1. List of supermarkets found\n"+
                            "2. Select to browse the shops website\n"
                )
                .backgroundColor(Color.parseColor(getString(0+R.color.colorPrimary)))
                .targetView(shopsNav);
        BubbleShowCaseBuilder b3 = new BubbleShowCaseBuilder(this)
                .title("Shopping Items")
                .description(
                            "1. Shop items are obtained from browsed shop web pages\n"+
                            "2. Select items to add into a shopping list/basket\n"+
                            "3. Prices are updated once a week"
                )
                .backgroundColor(Color.parseColor(getString(0+R.color.red)))
                .targetView(itemsNav);
        BubbleShowCaseBuilder b4 = new BubbleShowCaseBuilder(this)
                .title("Shopping Baskets/Lists")
                .description(
                            "1. Create baskets and view shopping progress\n"+
                            "2. Long press basket item to mark as picked\n"+
                            "3. Use up and down arrows for multiple item selection"
                )
                .backgroundColor(Color.parseColor(getString(0+R.color.green)))
                .targetView(basketsNav);
        showCaseSequence
                .addShowCase(b1)
                .addShowCase(b2)
                .addShowCase(b3)
                .addShowCase(b4)
                .show();
    }

    public boolean showTutorial(){
        if (navigation == null || searchView == null){
            return false;
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean showTutorial = preferences.getBoolean(AppVariables.SHOW_TUTORIAL,true);
        if (showTutorial){
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(AppVariables.SHOW_TUTORIAL, false);
            editor.commit();
        }
        return showTutorial;
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){
        int initialRequest = permissionsManager.INITIAL_REQUEST;
        if (permsRequestCode == initialRequest && grantResults.length > 0) {
            boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
            if (locationAccepted){
                AppSetup();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.shopping_basket_menu, menu);

        menu.findItem(R.id.amount_item).setVisible(false);
        menu.findItem(R.id.quantity_item).setVisible(false);

        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @SuppressWarnings("unused")
            public boolean onMenuItemActionCollapse(MenuItem item) {
                if (fragment instanceof ISearchable){
                    ((ISearchable)fragment).filter(null);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String arg0) {
                String text = arg0.toLowerCase(Locale.getDefault()).trim();
                System.out.println("text change: "+text);
                if (fragment instanceof ISearchable){
                    ((ISearchable)fragment).filter(text);
                }
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String text) {
                if (fragment instanceof ISearchable){
                    ((ISearchable)fragment).filter(text);
                }
                return false;
            }
        });

        TutorialSetup();

        return true;
    }

    @Override
    public void onResume(){
        super.onResume();
        if (fragment != null && fragment instanceof ShoppingBasketsActivity){
            ((ShoppingBasketsActivity)fragment).refresh();
        }
    }

    /**
     * Only use for debugging! NOT in production code!
     */
    private void setCrashListener(){
        if (!BuildConfig.BUILD_TYPE.equalsIgnoreCase("debug")) {
            return;
        }

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler(){
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                //String timeStamp = new SimpleDateFormat(AppVariables.timeFormat).format(Calendar.getInstance().getTime());
                String stackTrace = Log.getStackTraceString(ex);
                System.out.println(stackTrace);
                FileManager fileManager = new FileManager(AppVariables.context);
                fileManager.writeToFile(AppVariables.errorLogName,stackTrace);
                System.exit(1);
            }
        });
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_shops:
                    loadFragment(new ShopsActivity(),0);
                    return true;
                case R.id.navigation_items:
                    loadFragment(new ShopItemsActivity(), 0);
                    return true;
                case R.id.navigation_baskets:
                    loadFragment(new ShoppingBasketsActivity(),0);
                    return true;
            }
            return false;
        }
    };

    @Override
    public boolean loadFragment(Fragment fragment, int navigationId) {
        if (navigationId > 0){
            navigation.setSelectedItemId(navigationId);
        }

        this.fragment = fragment;
        if (fragment != null) {
            //Toast.makeText(context, "fragment loaded", Toast.LENGTH_LONG).show();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commitAllowingStateLoss();
            return true;
        }
        return false;
    }

    @Override
    public void populateMainView(Object object) {

    }

    @Override
    public void postAction(EActionType actionType, Object object) {

    }

    @Override
    public void logout() {

    }

    @Override
    public void deprecatedVersion() {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void updateSettings(String key) {

    }

    @Override
    public void startBackgroundService() {

    }

    @Override
    public void setTutorial() {

    }

    @Override
    public ShopsManager getShopsManager() {
        return shopsManager;
    }

    @Override
    public Context getContext() {
        return context;
    }
}
