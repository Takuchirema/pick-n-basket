package org.picknbasket.picknbasket.Views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Abstracts.IActivity;
import org.picknbasket.picknbasket.Abstracts.ILocationListener;
import org.picknbasket.picknbasket.Abstracts.ISearchable;
import org.picknbasket.picknbasket.Abstracts.IWebViewActivity;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.GridListAdapter;
import org.picknbasket.picknbasket.Helpers.GridRecyclerAdapter;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Helpers.ViewHolder;
import org.picknbasket.picknbasket.Managers.ShopsManager;
import org.picknbasket.picknbasket.Objects.Shop;
import org.picknbasket.picknbasket.R;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class ShopsActivity extends Fragment implements IWebViewActivity, ISearchable, ILocationListener {

    private RecyclerView gridView;
    private SwipeRefreshLayout refreshLayout;
    private LayoutInflater inflater;
    private GridRecyclerAdapter adapter;
    private GridLayoutManager layoutManager;
    private WebView webView;
    private ProgressBar progressBar;
    private FrameLayout progressBarHolder;
    private Context context;
    private View mainView;
    private IActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        activity = this;
        context = AppVariables.context;
        mainView = inflater.inflate(R.layout.activity_shops, null);

        UISetup();
        AppSetup();

        return mainView;
    }

    private void UISetup(){
        refreshLayout = mainView.findViewById(R.id.swipe_refresh);

        gridView = mainView.findViewById(R.id.shops_grid);
        gridView.setLayoutManager(layoutManager = new GridLayoutManager(getContext(), 2){
            @Override
            public void onLayoutCompleted(final RecyclerView.State state) {
                super.onLayoutCompleted(state);
            }
        });

        webView = mainView.findViewById(R.id.webview_hidden);
        progressBar = mainView.findViewById(R.id.progress_bar);
        progressBarHolder = mainView.findViewById(R.id.progress_bar_holder);

        inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
    }

    @SuppressLint("MissingPermission")
    public void AppSetup(){
        AppVariables.registerLocationListener(R.layout.activity_shops, this);
        ShopsManager shopsManager = new ShopsManager();
        shopsManager.getShops(null,this);

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ShopsManager shopsManager = new ShopsManager();
                        shopsManager.getShops(null,activity);
                    }
                }
        );
    }

    @Override
    public void onLocationChanged(Location location) {
        ShopsManager shopsManager = new ShopsManager();
        shopsManager.getShops(location, this);
    }

    @Override
    public void populateMainView(Object object) {
        //Toast.makeText(AppVariables.context, "shops populate main view", Toast.LENGTH_LONG).show();
        refreshLayout.setRefreshing(false);
        SyncLinkedHashMap<Integer,Shop> shops = (SyncLinkedHashMap)object;
        createListAdapter(shops);
        //TutorialSetup();
    }

    @Override
    public void postAction(EActionType actionType, Object object) {
        Toast.makeText(AppVariables.context, "post get action ", Toast.LENGTH_LONG).show();
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(SyncLinkedHashMap<Integer,Shop> shops){
        ArrayList<Integer> shopIds = new ArrayList<>(shops.keySet());

        adapter = new GridRecyclerAdapter(getActivity(),ShopOnlineActivity.class,shopIds,gridView, R.layout.content_grid_item);
        adapter.setMapObjects(shops);
        adapter.setAdverts(layoutManager, 5);
        gridView.setAdapter(adapter);

        adapter.notifyDataSetChanged();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Intent intent;
            switch (item.getItemId()) {
                case R.id.navigation_shops:
                    return true;
                case R.id.navigation_items:
                    intent = new Intent(context, ShopItemsActivity.class);
                    startActivity(intent);
                    return true;
                case R.id.navigation_baskets:
                    intent = new Intent(context, ShoppingBasketsActivity.class);
                    startActivity(intent);
                    return true;
            }
            return false;
        }
    };

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public WebView getWebView() {
        return webView;
    }

    @Override
    public ProgressBar getProgressBar(){
        return progressBar;
    }

    @Override
    public void showProgress(boolean show) {
        if (show){
            progressBarHolder.setVisibility(View.VISIBLE);
        }else{
            progressBarHolder.setVisibility(View.GONE);
        }
    }

    @Override
    public void filter(String text) {
        if (adapter != null) {
            adapter.filter(text);
        }
    }
}
