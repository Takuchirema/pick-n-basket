package org.picknbasket.picknbasket.Views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;

import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Abstracts.IActivity;
import org.picknbasket.picknbasket.Abstracts.IGridActivity;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.GridListAdapter;
import org.picknbasket.picknbasket.Helpers.GridRecyclerAdapter;
import org.picknbasket.picknbasket.Helpers.ObjectCache;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Managers.BasketsManager;
import org.picknbasket.picknbasket.Objects.Basket;
import org.picknbasket.picknbasket.Objects.ShopItem;
import org.picknbasket.picknbasket.R;

import java.util.ArrayList;

public class ShoppingBasketActivity extends AppCompatActivity implements IGridActivity {

    private FloatingActionMenu menuBlue;
    private FloatingActionButton saveBasket;
    private FloatingActionButton deleteBasket;
    private FloatingActionButton addItems;
    private FloatingActionButton resetBasket;

    private TextView purchasedAmountTV;
    private TextView purchasedQuantityTV;
    private TextView nonPurchasedAmountTV;
    private TextView nonPurchasedQuantityTV;
    private TextView totalAmountTV;
    private TextView totalQuantityTV;
    private RecyclerView gridView;
    private GridRecyclerAdapter adapter;
    private EditText basketNameET;
    private TextView basketNameTV;

    private int basketId;
    private Basket basket;
    private BasketsManager basketsManager;
    private Context context;
    private IActivity activity;

    private boolean addItemsAfterSave;

    private GridLayoutManager layoutManager;

    private PieChart amountChart;
    private PieChart quantityChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_basket);

        context = this;
        activity = this;

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey("basketId")) {
            basketId = getIntent().getExtras().getInt("basketId");
        }

        UISetup();
        AppSetup();
    }

    private void AppSetup(){
        basketsManager = new BasketsManager();

        if (basketId != 0){
            basket = ObjectCache.getBasket(basketId);
            if (basket == null){
                basketsManager.getBasket(this, basketId);
            }else{
                //System.out.println("cache not null setup "+basket.getTotalCost()+" "+basket.getTotalItems());
                populateMainView(basket);
            }
        }
    }

    private void UISetup(){

        menuBlue = findViewById(R.id.menu_blue);
        menuBlue.setClosedOnTouchOutside(true);
        menuBlue.setIconAnimated(false);

        saveBasket = findViewById(R.id.save_basket);
        deleteBasket = findViewById(R.id.delete_basket);
        addItems = findViewById(R.id.add_items);
        resetBasket = findViewById(R.id.reset_basket);

        saveBasket.setOnClickListener(clickListener);
        deleteBasket.setOnClickListener(clickListener);
        addItems.setOnClickListener(clickListener);
        resetBasket.setOnClickListener(clickListener);

        basketNameET = findViewById(R.id.name_et);
        basketNameTV = findViewById(R.id.name_tv);

        if (basketId == 0){
            findViewById(R.id.name_tv).setVisibility(View.GONE);
            findViewById(R.id.shop_items_grid).setVisibility(View.GONE);
            findViewById(R.id.name_et).setVisibility(View.VISIBLE);
        }

        purchasedAmountTV = findViewById(R.id.purchased_amount);
        purchasedQuantityTV = findViewById(R.id.purchased_quantity);
        nonPurchasedAmountTV = findViewById(R.id.non_purchased_amount);
        nonPurchasedQuantityTV = findViewById(R.id.non_purchased_quantity);
        totalAmountTV = findViewById(R.id.total_amount);
        totalQuantityTV = findViewById(R.id.total_quantity);

        gridView = findViewById(R.id.shop_items_grid);
        gridView.setLayoutManager(layoutManager = new GridLayoutManager(getContext(), 2));
    }

    public void setCharts(PieChart chart){

        chart.setUsePercentValues(false);
        chart.getDescription().setEnabled(false);
        chart.setDragDecelerationFrictionCoef(0.95f);
        //chart.setCenterText(generateCenterSpannableText());
        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE);
        chart.setTransparentCircleColor(Color.WHITE);
        chart.setTransparentCircleAlpha(110);
        chart.setHoleRadius(20f);
        chart.setTransparentCircleRadius(25f);

        chart.setDrawCenterText(false);
        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        chart.animateY(1400, Easing.EaseInOutQuad);
        // entry label styling
        chart.setEntryLabelColor(Color.WHITE);
        chart.setEntryLabelTextSize(12f);
        chart.getLegend().setEnabled(false);
    }

    private void setData(PieChart chart, ArrayList<PieEntry> entries) {
        PieDataSet dataSet = new PieDataSet(entries, "");

        dataSet.setDrawIcons(false);
        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors
        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.JOYFUL_COLORS) {
            colors.add(c);
        }

        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);

        chart.setData(data);
        // undo all highlights
        chart.highlightValues(null);
        chart.invalidate();
    }

    private ArrayList<PieEntry> getAmountPieEntries(){
        ArrayList<PieEntry> entries = new ArrayList<>();

        if (basket != null){
            // NOTE: The order of the entries when being added to the entries array determines their position around the center of
            // the chart.
            entries.add(new PieEntry(basket.getSpentCost(),"Spent"));
            entries.add(new PieEntry(basket.getRemainingCost(),"Remainder"));
        }else{
            entries.add(new PieEntry(100,"Spent"));
        }

        return entries;
    }

    private ArrayList<PieEntry> getQuantityPieEntries(){
        ArrayList<PieEntry> entries = new ArrayList<>();

        if (basket != null){
            entries.add(new PieEntry(basket.getPurchasedItems(),"Purchased"));
            entries.add(new PieEntry(basket.getRemainingItems(),"Remainder"));
        }else{
            entries.add(new PieEntry(100,"Purchased"));
        }

        return entries;
    }

    private SpannableString generateCenterSpannableText() {
        SpannableString s = new SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
        return s;
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onClick(View v) {
            menuBlue.close(true);
            //Intent intent;
            switch (v.getId()) {
                case R.id.save_basket:
                    if (basket != null){
                        saveBasket();
                        return;
                    }
                    if (canCreate()){
                        createBasket();
                    }
                    break;
                case R.id.delete_basket:
                    basketsManager.deleteBasket(activity, basket);
                    break;
                case R.id.reset_basket:
                    basketsManager.resetBasket(activity, basket);
                    break;
                case R.id.add_items:
                    if (canCreate()){
                        addItemsAfterSave = true;
                        createBasket();
                        return;
                    }
                    addItems();
                    break;
            }
        }
    };

    /**
     * Opens up the fragment for shop items with a basket id specified so that items are added to it
     */
    private void addItems(){
        addItemsAfterSave=false;
        Bundle bundle = new Bundle();
        bundle.putInt("basketId", basket.getId());
        ShopItemsActivity fragment = new ShopItemsActivity();
        fragment.setArguments(bundle);
        AppVariables.mainActivity.loadFragment(fragment,R.id.navigation_items);
        finish();
    }

    public void createBasket(){
        Basket basket = new Basket(basketNameET.getText().toString());
        basketsManager.createBasket(activity,basket);
    }

    public void saveBasket() {
        basketsManager.saveBasket(this, basket);
    }

    private boolean canCreate(){
        if (basketNameET.getText().toString().trim().equalsIgnoreCase("")) {
            basketNameET.setError("This field can not be blank");
            return false;
        }
        return true;
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(SyncLinkedHashMap<Integer, ShopItem> shopsItems){
        shopsItems.sort();
        ArrayList<Integer> shopItemIds = new ArrayList<>(shopsItems.keySet());

        adapter = new GridRecyclerAdapter(this,ShopItemActivity.class,shopItemIds,gridView, R.layout.content_grid_item);
        adapter.setGridActivity(this);
        adapter.setAdverts(layoutManager,5);
        adapter.setMapObjects(shopsItems);
        adapter.selectAll(true);
        adapter.setShowNumberPicker(true);
        gridView.setAdapter(adapter);
    }

    @Override
    public void longClick(Object object) {
        if (object instanceof ShopItem){
            ShopItem shopItem = (ShopItem)object;
            if (basket != null){
                basket.setPicked(shopItem);
                refreshAmounts();
                shopItem.applyBackground();
            }
        }
    }

    @Override
    public void numberPicked(Object object, int value) {
        if (object instanceof ShopItem){
            ShopItem shopItem = (ShopItem)object;
            if (basket != null){
                basket.setCount(shopItem, value);
                refreshAmounts();
            }
        }
    }

    @Override
    public void gridItemSelected(Object object, boolean selected) {
        if (object instanceof ShopItem){
            if (basket != null){
                basket.calculate();
                refreshAmounts();
            }
        }
    }

    private void refreshAmounts(){
        basketNameTV.setText(basket.getName());
        purchasedAmountTV.setText(basket.getSpentCost()+"");
        purchasedQuantityTV.setText(basket.getPurchasedItems()+"");
        nonPurchasedAmountTV.setText(basket.getRemainingCost()+"");
        nonPurchasedQuantityTV.setText(basket.getRemainingItems()+"");
        totalAmountTV.setText(basket.getTotalCost()+"");
        totalQuantityTV.setText(basket.getTotalItems()+"");
    }

    @Override
    public void populateMainView(Object object) {
        if (object instanceof SyncLinkedHashMap){
            SyncLinkedHashMap<Integer, Basket> baskets = (SyncLinkedHashMap)object;
            for (Basket basket:baskets.values()) {
                populateMainView(basket);
                break;
            }
        }
    }

    private void populateMainView(Basket basket){
        createListAdapter(basket.getBasketItems());
        this.basket = copyBasket(basket);
        refreshAmounts();
    }

    private Basket copyBasket(Basket basket){
        Basket copyBasket = new Basket(basket.getName());
        copyBasket.setId(basket.getId());
        copyBasket.setBasketItems((SyncLinkedHashMap)adapter.getSelectedItems());
        return copyBasket;
    }

    @Override
    public void postAction(EActionType actionType, Object object) {
        //Toast.makeText(context, "post action: "+actionType.toString(), Toast.LENGTH_LONG).show();
        menuBlue.close(true);
        switch (actionType){
            case POST_UPDATE:
                populateMainView((Basket)object);
                break;
            case POST_ADD:
                populateMainView((Basket)object);
                if (addItemsAfterSave){
                    addItems();
                }

                finish();
                break;
            case POST_DELETE:
                finish();
                break;
        }
    }

    @Override
    public Context getContext() {
        return context;
    }
}
