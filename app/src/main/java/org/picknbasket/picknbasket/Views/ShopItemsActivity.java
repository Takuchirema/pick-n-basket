package org.picknbasket.picknbasket.Views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.SearchView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Abstracts.IGridActivity;
import org.picknbasket.picknbasket.Abstracts.ISearchable;
import org.picknbasket.picknbasket.Abstracts.IWebViewActivity;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Helpers.GridListAdapter;
import org.picknbasket.picknbasket.Helpers.GridRecyclerAdapter;
import org.picknbasket.picknbasket.Helpers.SyncLinkedHashMap;
import org.picknbasket.picknbasket.Managers.BasketsManager;
import org.picknbasket.picknbasket.Managers.ShopsManager;
import org.picknbasket.picknbasket.Objects.Basket;
import org.picknbasket.picknbasket.Objects.Shop;
import org.picknbasket.picknbasket.Objects.ShopItem;
import org.picknbasket.picknbasket.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class ShopItemsActivity extends Fragment implements IWebViewActivity, IGridActivity, ISearchable{

    private Context context;
    private int shopId;
    private int basketId;
    private GridRecyclerAdapter adapter;
    private GridLayoutManager layoutManager;
    private RecyclerView gridView;
    private WebView webView;
    private ProgressBar progressBar;
    private FrameLayout progressBarHolder;
    private View mainView;

    private BasketsManager basketsManager;
    private IWebViewActivity activity;
    private Menu menu;

    private SwipeRefreshLayout refreshLayout;
    private FloatingActionMenu menuOrange;
    private FloatingActionButton selectAllFB;
    private FloatingActionButton addToBasketFB;
    private FloatingActionButton addShopItemsFB;
    private FloatingActionButton editShopItemFB;
    private FloatingActionButton deleteItemsFB;

    private FloatingActionMenu saveBasketMenu;
    private LinearLayout basketsContainer;

    private boolean selectAll = true;
    private Basket basket;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        context = AppVariables.context;
        activity = this;
        mainView = inflater.inflate(R.layout.activity_shop_items, null);

        Bundle arguments = getArguments();
        if (arguments != null){
            if (arguments.containsKey("shopId")) {
                shopId = getArguments().getInt("shopId");
            }
            if (arguments.containsKey("basketId")) {
                basketId = getArguments().getInt("basketId");
            }
        }

        UISetup();
        AppSetup();

        return mainView;
    }

    private void UISetup(){
        refreshLayout = mainView.findViewById(R.id.swipe_refresh);

        menuOrange = mainView.findViewById(R.id.menu_orange);
        menuOrange.setClosedOnTouchOutside(true);
        menuOrange.setIconAnimated(false);

        saveBasketMenu = mainView.findViewById(R.id.save_basket);
        saveBasketMenu.setClosedOnTouchOutside(true);
        saveBasketMenu.setIconAnimated(false);
        saveBasketMenu.setVisibility(View.GONE);

        selectAllFB = mainView.findViewById(R.id.select_all);
        addToBasketFB = mainView.findViewById(R.id.add_to_basket);
        addShopItemsFB = mainView.findViewById(R.id.add_shop_items);
        editShopItemFB = mainView.findViewById(R.id.edit_shop_item);
        deleteItemsFB = mainView.findViewById(R.id.reset_items);

        selectAllFB.setOnClickListener(clickListener);
        addToBasketFB.setOnClickListener(clickListener);
        addShopItemsFB.setOnClickListener(clickListener);
        editShopItemFB.setOnClickListener(clickListener);
        deleteItemsFB.setOnClickListener(clickListener);

        saveBasketMenu.setOnMenuButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (saveBasketMenu.isOpened()){
                    saveBasketMenu.close(true);
                    saveBasket();
                }else {
                    saveBasketMenu.open(true);
                }
            }
        });

        gridView = mainView.findViewById(R.id.shop_items_grid);
        gridView.setLayoutManager(layoutManager = new GridLayoutManager(getContext(), 2));

        webView = mainView.findViewById(R.id.webview_hidden);
        progressBar = mainView.findViewById(R.id.progress_bar);
        progressBarHolder = mainView.findViewById(R.id.progress_bar_holder);
    }

    public void AppSetup(){
        ShopsManager shopsManager = new ShopsManager();
        shopsManager.getShopItems(shopId, this);

        basketsManager = new BasketsManager();
        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ShopsManager shopsManager = new ShopsManager();
                        shopsManager.getShopItems(shopId, activity);
                    }
                }
        );
    }

    @Override
    public void populateMainView(Object object) {
        if (object instanceof SyncLinkedHashMap) {
            SyncLinkedHashMap items = (SyncLinkedHashMap) object;

            if (items.isEmpty()){
                return;
            }

            Object item = items.values().toArray()[0];
            if (item instanceof ShopItem){
                refreshLayout.setRefreshing(false);
                System.out.println("Shop Items! " + items.size());
                createListAdapter(items);
            }else if (item instanceof Basket){
                System.out.println("Baskets! " + items.size());
                if (basketId == 0) {
                    selectBasket(items);
                }else{
                    basketSelected((Basket)item);
                }
            }else if (item instanceof Shop){
                selectShop(items);
            }
        }
    }

    @Override
    public void postAction(EActionType actionType, Object object) {
        switch (actionType){
            case POST_UPDATE:
                if (object instanceof Basket){
                    Basket basket = (Basket)object;
                    Toast.makeText(context, "Basket, "+basket.getName()+", updated", Toast.LENGTH_LONG).show();
                    removeBasket();
                }
                break;
            case POST_DELETE:
                ShopsManager shopsManager = new ShopsManager();
                shopsManager.getShopItems(0, this);
                break;
        }
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(SyncLinkedHashMap<Integer,ShopItem> shopsItems){
        ArrayList<Integer> shopItemIds = new ArrayList<>(shopsItems.keySet());

        adapter = new GridRecyclerAdapter(getActivity(),ShopItemActivity.class,shopItemIds,gridView, R.layout.content_grid_item);
        adapter.setGridActivity(this);
        adapter.setAdverts(layoutManager,7);
        adapter.setMapObjects(shopsItems);
        gridView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        // Get basket if we are supposed to fill
        if (basketId != 0){
            basketsManager.getBasket(this, basketId);
        }
    }

    private void selectShop(SyncLinkedHashMap<Integer,Shop> shops){
        final Dialog dialog = new Dialog(context,R.style.Dialog);
        dialog.setContentView(R.layout.content_select_shop);
        dialog.setTitle("Please Select A Shop");

        TextView titleView = dialog.findViewById(android.R.id.title);
        if (titleView != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            titleView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }

        RecyclerView gridView = dialog.findViewById(R.id.shops_grid);
        GridLayoutManager layoutManager;
        gridView.setLayoutManager(layoutManager = new GridLayoutManager(getContext(), 2){
            @Override
            public void onLayoutCompleted(final RecyclerView.State state) {
                super.onLayoutCompleted(state);
            }
        });

        ArrayList<Integer> shopIds = new ArrayList<>(shops.keySet());

        GridRecyclerAdapter adapter = new GridRecyclerAdapter(getActivity(),ShopOnlineActivity.class,shopIds,gridView, R.layout.content_grid_item);
        adapter.setMapObjects(shops);
        adapter.setAdverts(layoutManager, 5);
        gridView.setAdapter(adapter);

        adapter.notifyDataSetChanged();

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void selectBasket(SyncLinkedHashMap<Integer,Basket> baskets){
        final Dialog dialog = new Dialog(context,R.style.Dialog);
        dialog.setContentView(R.layout.content_select_basket);
        dialog.setTitle("Please Select A Basket");

        TextView titleView = dialog.findViewById(android.R.id.title);
        if (titleView != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            titleView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        basketsContainer= dialog.findViewById(R.id.baskets_container);
        for (Map.Entry<Integer,Basket> entry: baskets.entrySet()){
            final Basket basket = entry.getValue();

            View mainView = inflater.inflate(R.layout.content_basket_list_item, null);

            basket.setView(mainView);
            basket.setUpView();
            basket.setTextViews();
            basket.setPrimaryView();

            mainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    basketSelected(basket);
                    dialog.cancel();
                }
            });

            basketsContainer.addView(mainView);
        }

        System.out.println("basket container "+basketsContainer.getChildCount()+" "+baskets.size());
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        this.menu = menu;

        inflater.inflate(R.menu.shopping_basket_menu, menu);

        if (basketId == 0){
            menu.findItem(R.id.amount_item).setVisible(false);
            menu.findItem(R.id.quantity_item).setVisible(false);
        }else if (basket != null){
            refreshMenuAmounts();
        }

        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @SuppressWarnings("unused")
            public boolean onMenuItemActionCollapse(MenuItem item) {
                filter(null);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String arg0) {
                String text = arg0.toLowerCase(Locale.getDefault()).trim();
                System.out.println("text change: "+text);
                filter(text);
                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String text) {
                filter(text);
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void basketSelected(Basket basket) {
        saveBasketMenu.setMenuButtonLabelText("Save basket "+basket.getName());

        menu.findItem(R.id.amount_item).setVisible(true);
        menu.findItem(R.id.quantity_item).setVisible(true);

        saveBasketMenu.setVisibility(View.VISIBLE);

        adapter.getSelectedItems().putAll(basket.getBasketItems());
        adapter.notifyDataSetChanged();

        this.basket = copyBasket(basket);
        refreshMenuAmounts();
    }

    private Basket copyBasket(Basket basket){
        Basket copyBasket = new Basket(basket.getName());
        copyBasket.setId(basket.getId());
        copyBasket.setBasketItems((SyncLinkedHashMap) adapter.getSelectedItems());
        return copyBasket;
    }

    private void removeBasket(){
        basket = null;

        menu.findItem(R.id.amount_item).setVisible(false);
        menu.findItem(R.id.quantity_item).setVisible(false);

        saveBasketMenu.setVisibility(View.GONE);
    }

    public void saveBasket() {
        basketsManager.saveBasket(this, basket);
    }

    private void refreshMenuAmounts(){
        LinearLayout amountLayout = (LinearLayout) menu.findItem(R.id.amount_item).getActionView();
        LinearLayout quantityLayout = (LinearLayout) menu.findItem(R.id.quantity_item).getActionView();

        TextView amountTV = amountLayout.findViewById(R.id.amount_tv);
        TextView quantityTV = quantityLayout.findViewById(R.id.quantity_tv);

        if (basket != null) {
            System.out.println("baskets not null "+basket.getTotalCost()+" "+basket.getTotalItems());
            amountTV.setText(basket.getCurrency()+" "+basket.getTotalCost() + "");
            quantityTV.setText(basket.getTotalItems() + "");
        }else{
            System.out.println("baskets null");
            amountTV.setText(0 + "");
            quantityTV.setText(0 + "");
        }
    }

    @Override
    public void gridItemSelected(Object object, boolean selected) {
        if (object instanceof ShopItem){
            if (basket != null){
                basket.calculate();
                //Toast.makeText(context, "selected "+basket.getTotalItems()+" "+adapter.getSelectedItems().size()+" "+selected, Toast.LENGTH_LONG).show();
                refreshMenuAmounts();
            }
        }
    }

    @Override
    public void longClick(Object object) {

    }

    @Override
    public void numberPicked(Object object, int value) {

    }

    @Override
    public Context getContext() {
        return context;
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onClick(View v) {
            menuOrange.close(true);
            ShopsManager shopsManager;
            //Intent intent;
            switch (v.getId()) {
                case R.id.select_all:
                    if (selectAll) {
                        adapter.selectAll(true);
                        selectAllFB.setLabelText("Deselect All");
                        selectAll = false;
                    }else{
                        adapter.selectAll(false);
                        selectAllFB.setLabelText("Deselect All");
                        selectAll = true;
                    }
                    break;
                case R.id.add_to_basket:
                    basketsManager.getBaskets(activity);
                    break;
                case R.id.reset_items:
                    shopsManager = new ShopsManager();
                    shopsManager.resetShopItems(activity);
                    break;
                case R.id.add_shop_items:
                    shopsManager = new ShopsManager();
                    shopsManager.getShops(null,activity);
                    break;
                case R.id.edit_shop_item:
                    break;
            }
        }
    };

    public void getBaskets(){
        basketsManager.getBaskets(this);
    }

    @Override
    public WebView getWebView() {
        return webView;
    }

    @Override
    public ProgressBar getProgressBar() {
        return progressBar;
    }

    @Override
    public void showProgress(final boolean show) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (show){
                    progressBarHolder.setVisibility(View.VISIBLE);
                }else{
                    progressBarHolder.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void filter(String text) {
        if (adapter != null) {
            adapter.filter(text);
        }
    }
}
