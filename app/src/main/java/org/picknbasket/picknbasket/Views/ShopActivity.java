package org.picknbasket.picknbasket.Views;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.picknbasket.picknbasket.Abstracts.EActionType;
import org.picknbasket.picknbasket.Abstracts.IActivity;
import org.picknbasket.picknbasket.Helpers.AppVariables;
import org.picknbasket.picknbasket.Managers.ShopsManager;
import org.picknbasket.picknbasket.Managers.ViewManager;
import org.picknbasket.picknbasket.Objects.Shop;
import org.picknbasket.picknbasket.R;

public class ShopActivity extends AppCompatActivity implements IActivity{

    private int shopId;
    private Shop shop;
    private View view;
    private Context context;

    private ImageView primaryIV;
    private ImageView secondaryIV;
    private TextView primaryTV;
    private TextView locationTV;
    private TextView businessHoursTV;
    private TextView websiteTV;
    private TextView phoneNumberTV;
    private TextView logoTV;

    private EditText locationET;
    private EditText websiteET;
    private EditText phoneNumberET;
    private EditText logoET;

    private boolean editing;
    private ShopsManager shopsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        context = this;
        view = findViewById(R.id.shop_layout);
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            shopId = extras.getInt("shopId");
        }

        shopsManager = new ShopsManager();
        shopsManager.getShop(shopId, this);
    }

    @Override
    public void populateMainView(Object object) {
        shop = (Shop)object;

        //Sets the ratings stuff
        shop.setSecondaryView(view);

        primaryIV = findViewById(R.id.primary_iv);
        secondaryIV = findViewById(R.id.secondary_iv);
        primaryTV = view.findViewById(R.id.primary_tv);
        locationTV = view.findViewById(R.id.location_tv);
        businessHoursTV = view.findViewById(R.id.business_hours_tv);
        websiteTV = view.findViewById(R.id.website_tv);
        phoneNumberTV = view.findViewById(R.id.phoneNumber_tv);
        logoTV = view.findViewById(R.id.logo_tv);

        locationET = view.findViewById(R.id.location_et);
        websiteET = view.findViewById(R.id.website_et);
        phoneNumberET = view.findViewById(R.id.phoneNumber_et);
        logoET = view.findViewById(R.id.logo_et);

        setTextViews();

        if (shop.getBmp() != null) {
            Drawable drawable = new BitmapDrawable(AppVariables.context.getResources(), shop.getBmp());
            primaryIV.setImageDrawable(drawable);
        }else{
            ViewManager viewManager = new ViewManager(primaryIV, EActionType.DOWNLOAD_PICTURE,this);
            viewManager.setUrl(shop.getPictureUrl());
            viewManager.setObject(shop);
            viewManager.getPicture();
        }

        secondaryIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editShopDetails(secondaryIV);
            }
        });
    }

    private void setTextViews(){
        primaryTV.setText(shop.getName());
        locationTV.setText(shop.getVicinity());
        businessHoursTV.setText(shop.getBusinessHours());
        websiteTV.setText(shop.getWebsite());
        phoneNumberTV.setText(shop.getPhoneNumber());
        logoTV.setText(shop.getPictureUrl());
    }

    @Override
    public void postAction(EActionType actionType, Object object) {

    }

    public void editShopDetails(ImageView secondaryIV){
        if (!editing) {
            editing = true;
            secondaryIV.setImageResource(R.drawable.ic_done_all_white_24dp);
            locationET.setText(shop.getVicinity());
            websiteET.setText(shop.getWebsite());
            phoneNumberET.setText(shop.getPhoneNumber());
            logoET.setText(shop.getPictureUrl());

            locationET.setVisibility(View.VISIBLE);
            websiteET.setVisibility(View.VISIBLE);
            phoneNumberET.setVisibility(View.VISIBLE);
            logoET.setVisibility(View.VISIBLE);

            primaryTV.setVisibility(View.GONE);
            locationTV.setVisibility(View.GONE);
            businessHoursTV.setVisibility(View.GONE);
            websiteTV.setVisibility(View.GONE);
            phoneNumberTV.setVisibility(View.GONE);
            logoTV.setVisibility(View.GONE);
        }else{
            if (!canSave()){
                return;
            }

            editing = false;
            secondaryIV.setImageResource(R.drawable.ic_edit);
            locationET.setVisibility(View.GONE);
            websiteET.setVisibility(View.GONE);
            phoneNumberET.setVisibility(View.GONE);
            logoET.setVisibility(View.GONE);

            primaryTV.setVisibility(View.VISIBLE);
            locationTV.setVisibility(View.VISIBLE);
            businessHoursTV.setVisibility(View.VISIBLE);
            websiteTV.setVisibility(View.VISIBLE);
            phoneNumberTV.setVisibility(View.VISIBLE);
            logoTV.setVisibility(View.VISIBLE);

            setTextViews();

            saveShopDetails();
        }
    }

    private boolean canSave(){
        return true;
    }

    public void saveShopDetails(){
        String location = locationET.getText().toString();
        String phoneNumber = phoneNumberET.getText().toString();
        String website = websiteET.getText().toString();
        String logo = logoET.getText().toString();

        System.out.println("save shop "+location+" "+logo);
        if (location != null && !location.isEmpty()){
            shop.setVicinity(location);
        }
        if (phoneNumber != null && !phoneNumber.isEmpty()){
            shop.setPhoneNumber(phoneNumber);
        }
        if (website != null && !website.isEmpty()){
            shop.setWebsite(website);
        }
        if (logo != null && !logo.isEmpty()){
            shop.setPictureUrl(logo);
        }
        shopsManager.storeShop(shop);
    }

    @Override
    public Context getContext() {
        return context;
    }
}
