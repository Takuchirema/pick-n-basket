package org.picknbasket.picknbasket.Abstracts;

import org.json.JSONException;
import org.picknbasket.picknbasket.Data.CustomJSONObject;

/**
 * Created by Takunda on 26/05/2018.
 */

public interface IGetServerData {
    void postGetServerData(CustomJSONObject dataSet) throws JSONException;
}
