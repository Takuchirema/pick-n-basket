package org.picknbasket.picknbasket.Abstracts;

public interface IDataAdapterCall<V> {
    void postCreateData(V v);

    void postUpdateData(V v);

    void postDeleteData(boolean success);
}
