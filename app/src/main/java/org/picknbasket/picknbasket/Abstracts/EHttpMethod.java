package org.picknbasket.picknbasket.Abstracts;

/**
 * Created by Takunda on 24/05/2018.
 */

public enum EHttpMethod {
    GET,
    POST,
    UPDATE,
    PATCH,
    DELETE
}
