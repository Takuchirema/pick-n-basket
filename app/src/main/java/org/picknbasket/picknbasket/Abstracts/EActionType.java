package org.picknbasket.picknbasket.Abstracts;

/**
 * Created by Takunda on 23/06/2018.
 */

public enum EActionType {
    POST_ADD,
    POST_UPDATE,
    POST_DELETE,
    POST_REQUEST,
    DOWNLOAD_PICTURE,

}
