package org.picknbasket.picknbasket.Abstracts;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;

import org.picknbasket.picknbasket.Data.CustomJSONObject;
import org.picknbasket.picknbasket.Data.JSONParser;
import org.picknbasket.picknbasket.Data.SQLite;
import org.picknbasket.picknbasket.Helpers.AppVariables;

import org.json.JSONException;
import org.picknbasket.picknbasket.Objects.ShopWebPage;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Takunda on 26/05/2018.
 */

public abstract class DataSet implements IGetServerData, IGetWebData{
    // Used to show if the DataSet successfully go the data
    public boolean success;
    public String message;
    public String status;

    public IGetDataSet getDataset;
    public HashMap<String, ShopWebPage> webpages = new HashMap<>();

    public DataSet(IGetDataSet getDataset){
        this.getDataset=getDataset;
    }

    public void getDataSet(){}

    public void prepareDataSet(CustomJSONObject json) throws JSONException {}

    public void prepareDataSet(Object data) {}

    @Override
    public void postGetServerData(CustomJSONObject json) throws JSONException {

        if (getDataset == null){
            return;
        }

        // check your log for json response
        if (json != null){
            int successTag = json.getInt(JSONParser.TAG_SUCCESS);
            message = json.getString(JSONParser.TAG_MESSAGE);
            EStatusCodes status = getStatus(json.getString("status"));

            if (successTag == 1 || status == EStatusCodes.OK) {
                success = true;
                prepareDataSet(json);
            }else{
                success = false;
            }
            postGetDataSet(this);
        }else{
            success = false;
            message = "Could not connect to server. Please check your internet connection and try again.";
            postGetDataSetMessage();
        }
    }

    @Override
    public void postGetWebData(Object data, String logoUrl, ArrayList<String> webPages,int shopId){

        if (getDataset == null){
            return;
        }

        if (data != null){
            success = true;
            storeWepPages(webPages,shopId);
            prepareDataSet(data);
            postGetDataSet(this);
        }else{
            success = false;
            message = "Could not connect to server. Please check your internet connection and try again.";
            postGetDataSetMessage();
        }
    }

    public void storeWepPages(ArrayList<String> webPages, int shopId){
        System.out.println("web scraped size: "+webPages.size());
        for(String url:webPages){
            ShopWebPage webPage = SQLite.storeShopWebPages(url,shopId);
            webpages.put(webPage.getUrl(),webPage);
        }
    }

    private EStatusCodes getStatus(String status){
        if (status == null){
            return EStatusCodes.FAIL;
        }

        if (status.equalsIgnoreCase(EStatusCodes.OK.toString())){
            return EStatusCodes.OK;
        }

        if (status.equalsIgnoreCase(EStatusCodes.BAD_REQUEST.toString())){
            return EStatusCodes.BAD_REQUEST;
        }

        if (status.equalsIgnoreCase(EStatusCodes.FORBIDDEN.toString())){
            return EStatusCodes.FORBIDDEN;
        }

        if (status.equalsIgnoreCase(EStatusCodes.NOT_FOUND.toString())){
            return EStatusCodes.NOT_FOUND;
        }

        return EStatusCodes.FAIL;
    }

    public void postGetDataSet(final DataSet dataSet){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                getDataset.postGetDataSet(dataSet);
            }
        });
    }

    public void postGetDataSetMessage() {
        if (!isNetworkAvailable()) {
            getDataset.postGetDataSet(this);
            return;
        }

        new Thread(new Runnable() {
            DataSet dataSet;

            public Runnable init(DataSet dataSet) {
                this.dataSet = dataSet;
                return this;
            }

            public void run() {
                try {
                    HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    urlc.setConnectTimeout(1500);
                    urlc.connect();

                    if(urlc.getResponseCode() == 200){
                        message = "It's not you it's us. We are fixing the issue, please try again later. Thank you!";
                    }
                } catch (Exception e) {}
                postGetDataSet(dataSet);
            }
        }.init(this)).start();
    }

    private boolean isNetworkAvailable() {
        Context context = AppVariables.context;
        if (AppVariables.mainActivity != null){
            context = AppVariables.mainActivity.getContext();
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public HashMap<String, ShopWebPage> getWebpages() {
        return webpages;
    }
}
