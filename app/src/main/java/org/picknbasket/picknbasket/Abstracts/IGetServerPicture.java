package org.picknbasket.picknbasket.Abstracts;

import android.graphics.Bitmap;

/**
 * Created by Takunda on 26/05/2018.
 */

public interface IGetServerPicture {
    void postGetServerPicture(Bitmap bmp);
}
