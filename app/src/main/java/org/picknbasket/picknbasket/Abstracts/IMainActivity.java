package org.picknbasket.picknbasket.Abstracts;

import android.content.Context;
import androidx.fragment.app.Fragment;

import org.picknbasket.picknbasket.Managers.ShopsManager;

/**
 * Created by Takunda on 7/28/2018.
 */

public interface IMainActivity extends IActivity{
    void logout();

    void deprecatedVersion();

    void showMessage(String message);

    void updateSettings(String key);

    void startBackgroundService();

    void setTutorial();

    ShopsManager getShopsManager();

    boolean loadFragment(Fragment fragment, int navigationId);

    Context getContext();
}
