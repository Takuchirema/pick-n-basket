package org.picknbasket.picknbasket.Abstracts;

/**
 * Created by Takunda on 2/14/2020.
 */

public interface ISearchable {
    void filter(String text);
}
