package org.picknbasket.picknbasket.Abstracts;

import android.webkit.WebView;
import android.widget.ProgressBar;

/**
 * Created by Takunda on 1/22/2020.
 */

public interface IWebViewActivity extends IActivity {
    WebView getWebView();
    ProgressBar getProgressBar();
    void showProgress(boolean show);
}
