package org.picknbasket.picknbasket.Abstracts;

import android.content.Context;

/**
 * Created by Takunda on 1/20/2020.
 */

public interface IActivity {
    void populateMainView(Object object);
    void postAction(EActionType actionType, Object object);
    Context getContext();
}
