package org.picknbasket.picknbasket.Abstracts;

/**
 * Created by Takunda on 07/07/2018.
 */

public enum EObjectType {
    SHOP {
        public String toString() {
            return "shop";
        }
    },
    SHOP_ITEM {
        public String toString() {
            return "shop item";
        }
    },
    BASKET {
        public String toString() {
            return "basket";
        }
    }
}
