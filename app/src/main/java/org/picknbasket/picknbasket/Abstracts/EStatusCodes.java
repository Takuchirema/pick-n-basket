package org.picknbasket.picknbasket.Abstracts;

/**
 * Created by Takunda on 1/19/2020.
 */

public enum EStatusCodes {
    OK {
        public String toString() {
            return "ok";
        }
    },
    FAIL {
        public String toString() {
            return "failed request";
        }
    },
    BAD_REQUEST {
        public String toString() {
            return "bad request";
        }
    },
    UNAUTHORIZED {
        public String toString() {
            return "unauthorized";
        }
    },
    FORBIDDEN {
        public String toString() {
            return "forbidden";
        }
    },
    NOT_FOUND {
        public String toString() {
            return "not found";
        }
    }
}
