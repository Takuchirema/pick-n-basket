package org.picknbasket.picknbasket.Abstracts;

public abstract class DataAdapter<V> {

    private String message;
    private int success;
    protected IDataAdapterCall dataAdapterCall;

    public DataAdapter(IDataAdapterCall dataAdapterCall){
        this.dataAdapterCall = dataAdapterCall;
    }

    public abstract void createData(V v);

    public abstract void updateData(V v);

    public abstract void deleteData(V v);

    public abstract void postCreateData(V v);

    public abstract void postUpdateData(V v);

    public abstract void postDeleteData(boolean success);
}
