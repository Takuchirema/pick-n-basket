package org.picknbasket.picknbasket.Abstracts;

public interface INumberPickerListener {
    void onNumberPicked(int value);
}
