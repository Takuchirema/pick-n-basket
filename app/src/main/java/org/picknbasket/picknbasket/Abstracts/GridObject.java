package org.picknbasket.picknbasket.Abstracts;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.picknbasket.picknbasket.Helpers.GridListAdapter;
import org.picknbasket.picknbasket.Helpers.GridRecyclerAdapter;
import org.picknbasket.picknbasket.Managers.ViewManager;
import org.picknbasket.picknbasket.Objects.Shop;
import org.picknbasket.picknbasket.R;
import org.picknbasket.picknbasket.Views.ImageViewerActivity;

/**
 * Created by Takunda on 23/06/2018.
 */

public abstract class GridObject implements Comparable<GridObject> {

    protected int id;
    protected int position;
    protected View view;
    protected ImageView primaryIV;
    protected TextView primaryTV;
    protected TextView secondaryTV;
    protected TextView infoTV;
    protected CheckBox selectItem;
    protected TextView infoHighlightTV;
    public RelativeLayout gridViewContainer;
    public EObjectType listObjectType;
    protected GridRecyclerAdapter adapter;
    public Bitmap bmp;
    public ArrayList<String> pictureUrls = new ArrayList<>();
    public String fileUrl;
    protected String mainText="";
    protected String secondaryText="";
    // Activity the list object is being displayed in
    public Activity listActivity;
    public Class listItemClassActivity;

    //Main object values
    protected double rating;
    protected int userRatingsTotal;
    protected String pictureUrl;
    protected GridObject object;

    public GridObject(){
        object=this;
        listObjectType = getListObjectType();
    }

    public void setUpView(){
        primaryIV = view.findViewById(R.id.primary_iv);
        secondaryTV = view.findViewById(R.id.secondary_tv);
        gridViewContainer = view.findViewById(R.id.grid_view_container);
        primaryTV = view.findViewById(R.id.primary_tv);
        secondaryTV =  view.findViewById(R.id.secondary_tv);
        selectItem = view.findViewById(R.id.select_item);
        infoHighlightTV = view.findViewById(R.id.info_highlight);
        infoTV = view.findViewById(R.id.info_tv);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void populateData(){
        setTextViews();
        setPrimaryView();
        setSecondaryView(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public View updateView(){
        populateData();
        return view;
    }

    public void setTextViews(){

        if (getMainText().length() == 0){
            primaryTV.setVisibility(View.GONE);
        }else{
            primaryTV.setVisibility(View.VISIBLE);
            primaryTV.setSelected(true);
            primaryTV.setText(getMainText());
        }

        if (getSecondaryText().length() == 0){
            secondaryTV.setVisibility(View.GONE);
        }else{
            secondaryTV.setVisibility(View.VISIBLE);
            secondaryTV.setSelected(true);
            secondaryTV.setText(getSecondaryText());
        }
    }

    public void setListeners(){
        setSecondaryViewListener();
        setPrimaryViewListener();
    }

    public void setSecondaryView(View view){
        RatingBar ratingBar = view.findViewById(R.id.rating);
        if (ratingBar == null) {
            return;
        }

        DecimalFormat df1 = new DecimalFormat("#.#");
        TextView ratingTV = view.findViewById(R.id.rating_tv);
        TextView ratingTotalTV = view.findViewById(R.id.rating_total_tv);

        ratingBar.setRating((float) getRating());
        ratingTV.setText(df1.format(getRating()));
        ratingTotalTV.setText("("+Integer.toString(getUserRatingsTotal())+")");
    }

    public void setSecondaryViewListener(){

        selectItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                adapter.selected(object, b);
            }
        });

        if (listItemClassActivity != null){
            doSecondaryViewActivity();
        }else{
            doSecondaryViewAction();
        }
    }

    public void doSecondaryViewActivity(){
        secondaryTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void doSecondaryViewAction(){

    }

    public void setPrimaryViewListener(){
        if (listItemClassActivity != null){
            doPrimaryViewActivity();
        }else{
            doPrimaryViewAction();
        }
    }

    public void doPrimaryViewActivity(){
        primaryIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(listActivity, listItemClassActivity);
                listActivity.startActivity(intent);
            }
        });
    }

    public void doPrimaryViewAction(){
    }

    public boolean hasPicture(){
        if (pictureUrl == null){
            //System.out.println("has pic? false "+pictureUrl);
            return false;
        }

        int i = pictureUrl.lastIndexOf('.');

        if (i != pictureUrl.length()-1 && i > 0) {
            //System.out.println("has pic? true "+pictureUrl);
            return true;
        }else{
            //System.out.println("has pic? false "+pictureUrl);
            return false;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setPrimaryView() {
        //Log.i("grid object","list object list set picture");

        ViewManager viewManager = new ViewManager(view.findViewById(R.id.primary_iv), EActionType.DOWNLOAD_PICTURE, listActivity);
        //viewManager.setAdapter(adapter);
        viewManager.setObject(this);

        if (pictureUrl == null || pictureUrl.isEmpty()){
            viewManager.setPicture(getDefaultPicture());
        }else if (getBmp() == null) {
            //until we get the right picture put a reserve
            viewManager.setPicture(getDefaultPicture());
            viewManager.setUrl(getPictureUrl());
            viewManager.setObject(this);
            viewManager.getPicture();
        }else{
            viewManager.setPicture(getBmp());
        }
        applyBackground();
    }

    public void applyBackground(){}

    public Drawable getDefaultPicture(){
        return null;
    }

    public void viewImage(String pictureUrl){
        Intent intent = new Intent(listActivity, ImageViewerActivity.class);
        intent.putExtra("pictureUrl", pictureUrl);

        switch(listObjectType){
            case SHOP:
                Shop shop = (Shop)this;
                intent.putExtra("title", shop.getName());
                break;
        }
        listActivity.startActivity(intent);
    }

    public int compareTo(GridObject object) {
        EObjectType objectType = object.getListObjectType();
        if (objectType == listObjectType){
            return compareToSameObjectType(object);
        }
        return getSortOrder() - object.getSortOrder();
    }

    public int getSortOrder(){
        switch (listObjectType){
            case SHOP:
                return 1;
        }
        return 0;
    }

    public int compareToSameObjectType(GridObject object){
        return 0;
    }

    public String getSearchString(){
        String searchString = getMainText() + " " + getSecondaryText() +" "+getBrandText();
        return searchString;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public void setAdapter(GridRecyclerAdapter adapter){
        this.adapter=adapter;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Bitmap getBmp() {
        return bmp;
    }

    public void setBmp(Bitmap bmp) {
        this.bmp = bmp;
    }

    public String getPictureUrl() {
        return pictureUrl == null ? "" : pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        if (pictureUrl == null || pictureUrl.isEmpty()){
            return;
        }
        pictureUrl = pictureUrl.replace("'","");
        this.pictureUrl = pictureUrl;
    }

    public abstract CharSequence getMainText();

    public abstract EObjectType getListObjectType();

    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    public abstract CharSequence getSecondaryText();

    public CharSequence getBrandText() {
        return "";
    }

    public void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
    }

    public Activity getListActivity() {
        return listActivity;
    }

    public void setListActivity(Activity listActivity) {
        this.listActivity = listActivity;
    }

    public Class getListItemClassActivity() {
        return listItemClassActivity;
    }

    public void setListItemClassActivity(Class listItemClassActivity) {
        this.listItemClassActivity = listItemClassActivity;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getUserRatingsTotal() {
        return userRatingsTotal;
    }

    public void setUserRatingsTotal(int userRatingsTotal) {
        this.userRatingsTotal = userRatingsTotal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
