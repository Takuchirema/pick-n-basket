package org.picknbasket.picknbasket.Abstracts;

public interface IGridActivity extends IActivity{
    void gridItemSelected(Object object, boolean selected);
    void longClick(Object object);
    void numberPicked(Object object, int value);
}
