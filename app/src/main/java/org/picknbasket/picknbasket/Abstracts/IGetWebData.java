package org.picknbasket.picknbasket.Abstracts;

import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Takunda on 1/20/2020.
 */

public interface IGetWebData {
    void postGetWebData(Object data,String logoUrl,ArrayList<String> scrapedUrls,int shopId);
}
