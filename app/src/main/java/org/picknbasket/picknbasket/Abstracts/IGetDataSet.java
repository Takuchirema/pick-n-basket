package org.picknbasket.picknbasket.Abstracts;

/**
 * Created by Takunda on 24/05/2018.
 */

public interface IGetDataSet {
    void postGetDataSet(DataSet dataSet);
}
