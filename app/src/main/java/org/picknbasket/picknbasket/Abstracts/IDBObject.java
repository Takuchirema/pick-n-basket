package org.picknbasket.picknbasket.Abstracts;

import android.content.ContentValues;

/**
 * Created by Takunda on 1/17/2020.
 */

public interface IDBObject {
    ContentValues getContentValues();
}
