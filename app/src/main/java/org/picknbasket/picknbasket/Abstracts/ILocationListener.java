package org.picknbasket.picknbasket.Abstracts;

public interface ILocationListener {
    void onLocationChanged(android.location.Location location);
}
